<?php
require_once '../library/Thinkopen/Pagecacher.php';
require_once '../library/Thinkopen/Logger.php';


/**
 * Description of Cacher
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Thinkopen_Cacher {

    /**
     * @var \Thinkopen_Cacher
     */
    private static $instance = NULL;
    private $_cache = NULL;

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $_namespace
     * @param int $_lifetime
     */
    protected function __construct($_namespace, $_lifetime) {
        $frontendOptions = array(
            "lifetime" => $_lifetime,
            "default_options" => array(
                "cache" => true,
                'cache_with_get_variables' => true,
                'cache_with_post_variables' => true,
                'cache_with_session_variables' => true,
                'cache_with_files_variables' => true,
                'cache_with_cookie_variables' => true,
                'make_id_with_get_variables' => true,
                'make_id_with_post_variables' => true,
                'make_id_with_session_variables' => true,
                'make_id_with_files_variables' => true,
                'make_id_with_cookie_variables' => true,
            ),
            'automatic_serialization' => true
        );
        $backendOptions = array(
            'cache_dir' => "../var/cache/{$_namespace}/"
        );
        if (!is_dir("../var/cache/{$_namespace}/")) {
            mkdir("../var/cache/{$_namespace}/", 0777);
        }
        if ($this->_cache === NULL) {
            $this->_cache = new stdClass();
            $this->_cache->namespaces = array();
        }
        $this->_cache->{$_namespace
                } = Zend_Cache::factory(
                        "Core", "File", $frontendOptions, $backendOptions
        );
        $this->_cache->namespaces[] = $_namespace;
        return $this;
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $_namespaceToClean
     */
    private static function _formatNamespace(&$_namespaceToClean) {
        $_namespace = preg_replace("/[^a-zA-Z0-9]+/", "", $_namespaceToClean);
        $_namespaceToClean = $_namespace;
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $_namespace
     * @param int $_lifetime
     * @return \Zend_Cache_Core
     * @throws Exception
     */
    public static function getInstance($_namespace = NULL, $_lifetime = 86400) {
        self::_formatNamespace($_namespace);
        if (self::$instance === NULL || !in_array($_namespace, self::$instance->_cache->namespaces)) {
            if ($_namespace === NULL || $_namespace == "") {
                throw new Exception("Please, specify cache namespace", 500);
            }
            self::$instance = new Thinkopen_Cacher($_namespace, $_lifetime);
        }
        return self::$instance->_cache->{$_namespace};
    }

    /**
     * Metodo statico pubblico per la pulizia di tutte le cache (database
     * e twitter esclusi, a meno di impostare i parametri in input a true)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param boolean $_database Flag per la pulizia dei metadati in cache
     * del database (default false)
     * @param boolean $_twitter Flag per la pulizia dei dati in cache
     * di Twitter (default false)
     */
    public static function cleanAll($_database = false, $_twitter = false) {
        $logger = new Thinkopen_Logger();
        $_directory = dir(APPLICATION_PATH . "/../var/cache");
        $_arrayNotToRemove = array("fullpage");
        if (!$_database) {
            array_push($_arrayNotToRemove, "database");
        }
        if(!$_twitter){
            array_push($_arrayNotToRemove, "twitter");
        }
        try {
            while ($_content = $_directory->read()) {
                if (stripos($_content, ".") === FALSE && $_content != "" && !in_array($_content, $_arrayNotToRemove)) {
                    $cache = Thinkopen_Cacher::getInstance($_content, 100);
                    $cache->clean();
                    $logger->log("CACHE", Zend_Log::INFO, $_content. " Cache cleaned");
                } elseif ($_content == "fullpage") {
                    $cache = Thinkopen_Pagecacher::getInstance();
                    $cache->clean();
                    $logger->log("FPCACHE", Zend_Log::INFO, "FPCache cleaned");
                }
            }
        } catch (Exception $e) {
            $logger->log("CACHE", Zend_Log::ERR, "Error cleaning cache:: " . $e->getMessage());
        }
    }

}
