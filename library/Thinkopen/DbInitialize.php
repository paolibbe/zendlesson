<?php

require_once '../library/Thinkopen/Cacher.php';

/**
 * Classe per l'inizializzazione del database ed il relativo settaggio
 * della cache per i metadati di tabella
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @version 2.1
 */
class Thinkopen_DbInitialize {

    protected static $instance;
    protected static $db;
    public static $iniConfig;

    protected function __construct() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        self::$iniConfig = $config;
        $db = Zend_Db::factory('PDO_MYSQL', array(
                    'host' => '127.0.0.1',
                    'username' => $config->database->username,
                    'password' => $config->database->password,
                    'dbname' => $config->database->schema
        ));
        $db->query("SET NAMES 'utf8'");
        $cache = Thinkopen_Cacher::getInstance("database", 86400);
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
        self::$db = $db;
        Zend_Db_Table::setDefaultAdapter($db);
    }

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new Thinkopen_DbInitialize();
        }
        return self::$instance;
    }

    public static function getDbInstance(){
        if (is_null(self::$db)) {
            self::$instance = new Thinkopen_DbInitialize();
        }
        return self::$db;
    }

}
