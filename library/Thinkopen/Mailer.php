<?php

/**
 * Classe per l'invio delle email tramite autenticazione ad un servizio
 * SMTP
 * @author Luigi Tironese <luigi.tironese@thinkopen.it>
 */
class Thinkopen_Mailer {

    const REGISTER = 1;
    const CHANGEPWD = 2;
    const RECOVERPWD = 3;

    private $_content;
    private $_subject;

    /**
     * @var \Zend_View_Helper_Translate 
     */
    private $_translator;

    /**
     * Costruttore di classe, inizializzazione dei parametri per il sending
     * dell'email
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     */
    public function __construct() {
        $_configs = Thinkopen_DbInitialize::getInstance();
        $this->_translator = new Zend_View_Helper_Translate();
        Zend_Mail::setDefaultFrom($_configs::$iniConfig->mailer->addressFrom, $_configs::$iniConfig->mailer->nameFrom);
        $_config = array('auth' => 'login',
            'username' => $_configs::$iniConfig->mailer->authentication->email,
            'password' => $_configs::$iniConfig->mailer->authentication->password,
            'port' => $_configs::$iniConfig->mailer->authentication->port
        );
        if ($_configs::$iniConfig->mailer->authentication->protocol != "None") {
            $_config['ssl'] = $_configs::$iniConfig->mailer->authentication->protocol;
        }
        $transport = new Zend_Mail_Transport_Smtp($_configs::$iniConfig->mailer->endpoint, $_config);
        Zend_Mail::setDefaultTransport($transport);
    }

    /**
     * Metodo per l'invio della mail ad un indirizzo
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param \stdClass|\Default_Model_Mapper_User $_userData
     * @param string $_mailType
     * @return boolean
     */
    public function sendMail($_userData, $_mailType, $pass) {
        try {
            if (is_a($_userData, "Default_Model_Mapper_User")) {
                $_userData = $this->_getDataFromUser($_userData);
            }
            switch ($_mailType) {
                case self::REGISTER:
                default:
                    $this->_createRegisterEmailMessage($_userData->first_name, $_userData->last_name);
                    break;
                case self::CHANGEPWD:
                    $this->_createChangePwdEmailMessage($_userData->first_name, $_userData->last_name);
                    break;
                case self::RECOVERPWD:
                    $this->_createRecoverPwdEmailMessage($_userData->first_name, $_userData->last_name, $pass);
                    break;
            }
            $mail = new Zend_Mail();
            $mail->setBodyHtml($this->_content);
            $mail->addTo($_userData->email, $_userData->first_name . " " . $_userData->last_name);
            $mail->setSubject($this->_subject);
            $mail->send();
            return true;
        } catch (Exception $exc) {
            $_logger = Thinkopen_Logger::getInstance();
            if (is_a($exc, "Zend_Mail_Protocol_Exception")) {
                $_message = $exc->getMessage() . ", " . $_userData->email;
            } else {
                $_message = $exc->getMessage();
            }
            $_logger->log("MAILER ({$_mailType})", Zend_Log::ERR, $_message);
            return false;
        }
    }

    /**
     * Metodo privato per la memorizzazione delle informazioni riguardanti la modifica dei dati
     * dell'utente nella classe stdClass
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param \Default_Model_Mapper_User $_user
     * @return \stdClass
     */
    private function _getDataFromUser($_user) {
        $_toReturn = new stdClass();
        $_toReturn->first_name = $_user->getFirstname();
        $_toReturn->last_name = $_user->getLastname();
        $_toReturn->email = $_user->getEmail();
        return $_toReturn;
    }

    /**
     * Metodo privato per la creazione del messaggio HTML da inserire come corpo
     * dell'email da inviare all'utente
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     */
    private function _createRegisterEmailMessage($_firstname, $_lastname) {
        $this->_subject = $this->_translator->translate("Conferma registrazione community Teatro di San Carlo");
        $_messageString = "";
        switch (Top::getCurrentLanguage()) {
            case "it":
            default:
                $_messageString .= $this->_getRegisterEmailContentIta($_firstname, $_lastname);
                break;
            case "en":
                $_messageString .= $this->_getRegisterEmailContentEng($_firstname, $_lastname);
                break;
        }
        $this->_content = $_messageString;
    }

    /**
     * Metodo privato per la creazione del messaggio HTML da inserire come corpo
     * dell'email da inviare all'utente
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     */
    private function _createChangePwdEmailMessage($_firstname, $_lastname) {
        $this->_subject = $this->_translator->translate("Cambio password community Teatro di San Carlo");
        $_messageString = "";
        switch (Top::getCurrentLanguage()) {
            case "it":
            default:
                $_messageString .= $this->_getChangePwdEmailContentIta($_firstname, $_lastname);
                break;
            case "en":
                $_messageString .= $this->_getChangePwdEmailContentEng($_firstname, $_lastname);
                break;
        }
        $this->_content = $_messageString;
    }
    /**
     * Metodo privato per la creazione del messaggio HTML da inserire come corpo
     * dell'email da inviare all'utente per il recupero password
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @param string $_password Nuova password dell'utente
     */
    private function _createRecoverPwdEmailMessage($_firstname, $_lastname, $pass) {
        $this->_subject = $this->_translator->translate("Recupero password community Teatro di San Carlo");
        $_messageString = "";
        switch (Top::getCurrentLanguage()) {
            case "it":
            default:
                $_messageString .= $this->_getRecoverPwdEmailContentIta($_firstname, $_lastname, $pass);
                break;
            case "en":
                $_messageString .= $this->_getRecoverPwdEmailContentEng($_firstname, $_lastname, $pass);
                break;
        }
        $this->_content = $_messageString;
    }

    /**
     * Metodo che restituisce il corpo dell'email in lingua italiana
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @return string
     */
    private function _getRegisterEmailContentIta($_firstname, $_lastname) {
        return "<div style='text-align:center'>"
                . '<a href="http://www.teatrosancarlo.it" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/logo.jpg" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<div style='margin:20px'>Benvenuto {$_firstname} {$_lastname},<br />"
                . "<br />"
                . "con questa e-mail ti confermiamo che sei entrato a far parte della Community del Teatro di San Carlo."
                . "<br />"
                . "Ecco i vantaggi che, con questa registrazione, potrai avere:"
                . "<br />"
                . "&nbsp;&nbsp;1.   Ricevere le nostre newsletter informative per essere sempre aggiornato su tutto ci&ograve;  che accade al San Carlo"
                . "<br />"
                . "&nbsp;&nbsp;2.   Essere informato sulle iniziative speciali e gli eventi dedicati agli iscritti"
                . "<br />"
                . "&nbsp;&nbsp;3.   Acquistare i biglietti per i nostri spettacoli direttamente online "
                . "<br />"
                . "&nbsp;&nbsp;4.   Ricevere, quando disponibili, le nostre promozioni con cui poter acquistare comodamente da casa i biglietti per gli spettacoli "
                . "<br />"
                . "Per commentare e condividere con noi contenuti, immagini e notizie sulla vita e le attivit&agrave;  del Teatro San Carlo ti ricordiamo che puoi continuare"
                . " a seguirci sul nostro sito, www.teatrosancarlo.it oppure  sui nostri profili Facebook, Twitter ed Instagram."
                . "<br />"
                . "<br />"
                . "Ti aspettiamo a Teatro!"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<b>Teatro di San Carlo</b>"
                . "<br />"
                . "<b>98\f, via San Carlo - 80132 Napoli</b>"
                . "<br />"
                . "<b>ITALIA</b>"
                . "<br />"
                . "<b>T+39 081 7972111</b>"
                . "<br />"
                . "<b><a style='text-decoration: none;color:black!important'>www.teatrosancarlo.it</a></b>"
                . "<br />"
                . "<b>#&egrave;inscena</b>"
                . "<br />"
                . "<b><hr></b>"
                . "<br />"
                . "<div style='text-align:center;'>"
                . '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/fbemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . '<a href="https://twitter.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tweetemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://instagram.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/instagramemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tripemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/youtubeemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<br />"
                . "</div>";
    }

    /**
     * Metodo che restituisce il corpo dell'email in lingua inglese
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @return string
     */
    private function _getRegisterEmailContentEng($_firstname, $_lastname) {
        return "<div style='text-align:center'>"
                . '<a href="http://www.teatrosancarlo.it" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/logo.jpg" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<div style='margin:20px'>Welcome {$_firstname} {$_lastname},<br />"
                . "<br />"
                . "With this email we would just like to confirm you that you have been correctly registered to  San Carlo Theatre Web Community."
                . "<br />"
                . "Here  following  the main advantages connected to this registration:"
                . "<br />"
                . "&nbsp;&nbsp;1.   A periodical newsletter  to be regularly informed about what&rsquo;s on at San Carlo Theater"
                . "<br />"
                . "&nbsp;&nbsp;2.   To be informed about special events organized for all the members"
                . "<br />"
                . "&nbsp;&nbsp;3.   Buy online your tickets"
                . "<br />"
                . "&nbsp;&nbsp;4.   Receive upcoming promotions for our shows (reduced tickets, extras...) "
                . "<br />"
                . "To be always connected with us, you can also visit our website, www.teatrosancarlo.it or follow us on our official social network accounts."
                . "<br />"
                . "<br />"
                . "You wait a Theater!"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<b>Teatro di San Carlo</b>"
                . "<br />"
                . "<b>98\f, via San Carlo - 80132 Napoli</b>"
                . "<br />"
                . "<b>ITALIA</b>"
                . "<br />"
                . "<b>T+39 081 7972111</b>"
                . "<br />"
                . "<b><a style='text-decoration: none;color:black!important'>www.teatrosancarlo.it</a></b>"
                . "<br />"
                . "<b>#&egrave;inscena</b>"
                . "<br />"
                . "<b><hr></b>"
                . "<br />"
                . "<div style='text-align:center;'>"
                . '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/fbemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . '<a href="https://twitter.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tweetemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://instagram.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/instagramemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tripemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/youtubeemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . "</div>"
                . "<br />"
                . "</div>";
    }

    /**
     * Metodo che restituisce il corpo dell'email relativo 
     * al cambio password in lingua italiana
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @param string $_password Nuova password dell'utente
     * @return string
     */
    private function _getChangePwdEmailContentIta($_firstname, $_lastname) {
        return "<div style='text-align:center'>"
                . '<a href="http://www.teatrosancarlo.it" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/logo.jpg" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<div style='margin:20px'>Gentile {$_firstname} {$_lastname},<br />"
                . "<br />"
                . "Con questa email ti confermiamo che hai correttamente modificato la tua password per accedere a www.teatrosancarlo.it."
                . "<br />"
                . "Ti ricordiamo che potrai nuovamente modificare la tua password in qualsiasi momento accedendo al tuo profilo dall&rsquo;area riservata dall&rsquo;homepage del nostro sito www.teatrosancarlo.it. In questa sezione potrai anche avere un riepilogo dei tuoi acquisti effettuati online sino ad oggi."
                . "<br />"
                . "La modifica della password che hai appena effettuato NON &egrave; valida per la biglietteria elettronica GeTicket. Se desideri cambiare anche le credenziali di accesso a questo servizio potrai farlo dall&rsquo;apposita area riservata sul portale www.geticket.it."
                . "<br />"
                . "<br />"
                . "Ti aspettiamo a Teatro!"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<b>Teatro di San Carlo</b>"
                . "<br />"
                . "<b>98\f, via San Carlo - 80132 Napoli</b>"
                . "<br />"
                . "<b>ITALIA</b>"
                . "<br />"
                . "<b>T+39 081 7972111</b>"
                . "<br />"
                . "<b><a style='text-decoration: none;color:black!important'>www.teatrosancarlo.it</span></a>"
                . "<br />"
                . "<b>#&egrave;inscena</b>"
                . "<br />"
                . "<b><hr></b>"
                . "<br />"
                . "<div style='text-align:center;'>"
                . '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/fbemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . '<a href="https://twitter.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tweetemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://instagram.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/instagramemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tripemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/youtubeemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<br />"
                . "</div>";
    }

    /**
     * Metodo che restituisce il corpo dell'email relativo al cambio password
     *  in lingua inglese
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @param string $_password Nuova password dell'utente
     * @return string
     */
    private function _getChangePwdEmailContentEng($_firstname, $_lastname) {
        return "<div style='text-align:center'>"
                . '<a href="http://www.teatrosancarlo.it" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/logo.jpg" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<div style='margin:20px'>Dear {$_firstname} {$_lastname},<br />"
                . "<br />"
                . "This message is to confirm you that your password to log in www.teatrosancarlo.it  has been successfully modified."
                . "<br />"
                . "We remind you that, if you want, you can change your password again from the reserved area of our website where you will also find a resume of all your online purchases."
                . "<br />"
                . "We inform you that your new password is NOT valid for GeTicket services. If you want to modify  the details of your GeTicket profile, please login in www.geticket.it and follow the instructions available in the reserved area."
                . "<br />"
                . "Thank you."
                . "<br />"
                . "Ti aspettiamo a Teatro!"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<b>Teatro di San Carlo</b>"
                . "<br />"
                . "<b>98\f, via San Carlo - 80132 Napoli</b>"
                . "<br />"
                . "<b>ITALIA</b>"
                . "<br />"
                . "<b>T+39 081 7972111</b>"
                . "<br />"
                . "<b><a style='text-decoration: none;color:black!important'>www.teatrosancarlo.it</a></b>"
                . "<br />"
                . "<b>#&egrave;inscena</b>"
                . "<br />"
                . "<b><hr></b>"
                . "<br />"
                . "<div style='text-align:center;'>"
                . '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/fbemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . '<a href="https://twitter.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tweetemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://instagram.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/instagramemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tripemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/youtubeemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<br />"
                . "</div>";
    }
    /**
     * Metodo che restituisce il corpo dell'email relativo 
     * al recuper password in lingua italiana
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @param string $_password Nuova password dell'utente
     * @return string
     */
    private function _getRecoverPwdEmailContentIta($_firstname, $_lastname, $pass) {
        return "<div style='text-align:center'>"
                . '<a href="http://www.teatrosancarlo.it" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/logo.jpg" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<div style='margin:20px'>Gentile {$_firstname} {$_lastname} ,<br />"
                . "<br />"
                . "Ecco la tua nuova password per accedere a www.teatrosancarlo.it: {$pass}"
                . "<br />"
                . "Ti ricordiamo che potrai  modificare la tua password in qualsiasi momento accedendo al tuo profilo dall&rsquo;area riservata dall&rsquo;homepage del nostro sito www.teatrosancarlo.it. In questa sezione potrai anche avere un riepilogo dei tuoi acquisti effettuati online sino ad oggi."
                . "<br />"
                . "La modifica della password che hai appena effettuato NON &egrave; valida per la biglietteria elettronica GeTicket. Se desideri cambiare anche le credenziali di accesso a questo servizio potrai farlo dall&rsquo;apposita area riservata sul portale www.geticket.it."
                . "<br />"
                . "<br />"
                . "Ti aspettiamo a Teatro!"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<b>Teatro di San Carlo</b>"
                . "<br />"
                . "<b>98\f, via San Carlo - 80132 Napoli</b>"
                . "<br />"
                . "<b>ITALIA</b>"
                . "<br />"
                . "<b>T+39 081 7972111</b>"
                . "<br />"
                . "<b><a style='text-decoration: none;color:black!important'>www.teatrosancarlo.it</span></a>"
                . "<br />"
                . "<b>#&egrave;inscena</b>"
                . "<br />"
                . "<b><hr></b>"
                . "<br />"
                . "<div style='text-align:center;'>"
                . '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/fbemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . '<a href="https://twitter.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tweetemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://instagram.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/instagramemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tripemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/youtubeemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<br />"
                . "</div>";
    }

    /**
     * Metodo che restituisce il corpo dell'email relativo al recuper password
     *  in lingua inglese
     * @author Luigi Tironese <luigi.tironese@thinkopen.it>
     * @param string $_firstname Nome dell'utente
     * @param string $_lastname Cognome dell'utente
     * @param string $_password Nuova password dell'utente
     * @return string
     */
    private function _getRecoverPwdEmailContentEng($_firstname, $_lastname, $pass) {
        return "<div style='text-align:center'>"
                . '<a href="http://www.teatrosancarlo.it" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/logo.jpg" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<div style='margin:20px'>Dear {$_firstname} {$_lastname},<br />"
                . "<br />"
                . "Here's your new password to access www.teatrosancarlo.it: {$pass}"
                . "<br />"
                . "We remind you that, if you want, you can change your password from the reserved area of our website where you will also find a resume of all your online purchases."
                . "<br />"
                . "We inform you that your new password is NOT valid for GeTicket services. If you want to modify  the details of your GeTicket profile, please login in www.geticket.it and follow the instructions available in the reserved area."
                . "<br />"
                . "Thank you."
                . "<br />"
                . "Ti aspettiamo a Teatro!"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<br />"
                . "<b>Teatro di San Carlo</b>"
                . "<br />"
                . "<b>98\f, via San Carlo - 80132 Napoli</b>"
                . "<br />"
                . "<b>ITALIA</b>"
                . "<br />"
                . "<b>T+39 081 7972111</b>"
                . "<br />"
                . "<b><a style='text-decoration: none;color:black!important'>www.teatrosancarlo.it</a></b>"
                . "<br />"
                . "<b>#&egrave;inscena</b>"
                . "<br />"
                . "<b><hr></b>"
                . "<br />"
                . "<div style='text-align:center;'>"
                . '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/fbemail.png" style="margin-left:5px;margin-right:5px;" >'
                . '</a>'
                . '<a href="https://twitter.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tweetemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://instagram.com/teatrosancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/instagramemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/tripemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >'
                . '<img src="' . BASE_URL . 'img/youtubeemail.png" style="margin-left:5px;margin-right:5px;">'
                . '</a>'
                . "</div>"
                . "<br />"
                . "</div>";
    }

}
