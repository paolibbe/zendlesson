<?php

require_once '../library/Thinkopen/DbInitialize.php';

/**
 * Description of Pagecacher
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Thinkopen_Pagecacher extends Zend_Cache {

    protected static $_instance = NULL;

    public static function getInstance() {
        if (self::$_instance === NULL) {
            $_iniConfig = Thinkopen_DbInitialize::getInstance();
            $_regexps = array();
            $_arrayConfigsCachedPages = array(
                "cache" => true,
                'cache_with_get_variables' => true,
                'cache_with_post_variables' => true,
                'cache_with_session_variables' => true,
                'cache_with_files_variables' => true,
                'cache_with_cookie_variables' => true,
                'make_id_with_get_variables' => true,
                'make_id_with_post_variables' => true,
                'make_id_with_session_variables' => false,
                'make_id_with_files_variables' => false,
                'make_id_with_cookie_variables' => false,
            );
            foreach ($_iniConfig::$iniConfig->website->languages->toArray() as $_language) {
                $_regexps["^/(" . $_language . '){0,2}[\/]{0,1}$'] = $_arrayConfigsCachedPages;
            }
            $_regexps['^/.{0,2}/pages'] = $_arrayConfigsCachedPages;
            $_regexps['^/.{0,2}/news'] = $_arrayConfigsCachedPages;
            $_regexps['^/.{0,2}/spettacoli'] = $_arrayConfigsCachedPages;
            $_regexps['^/.{0,2}/season'] = $_arrayConfigsCachedPages;
            $frontendOptions = array(
                "lifetime" => 21600,
                "default_options" => array(
                    "cache" => false,
                    'cache_with_get_variables' => true,
                    'cache_with_post_variables' => true,
                    'cache_with_session_variables' => true,
                    'cache_with_files_variables' => true,
                    'cache_with_cookie_variables' => true,
                    'make_id_with_get_variables' => true,
                    'make_id_with_post_variables' => true,
                    'make_id_with_session_variables' => false,
                    'make_id_with_files_variables' => false,
                    'make_id_with_cookie_variables' => false,
                ),
                "regexps" => $_regexps
            );
            $backendOptions = array(
                'cache_dir' => '../var/cache/fullpage/'
            );
            if (!is_dir('../var/cache/fullpage/')) {
                mkdir('../var/cache/fullpage/', 0777);
            }
            $cache = Zend_Cache::factory(
                            "Page", "File", $frontendOptions, $backendOptions
            );
            self::$_instance = $cache;
        }
        return self::$_instance;
    }

}
