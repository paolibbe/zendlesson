<?php

/**
 * Layer di interfaccia con il webservice in Java creato per la gestione
 * dell'indice di ricerca tramite Lucene
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @version 1.0
 */
class Thinkopen_Searcher {

    protected $client;
    protected $modelShow;
    protected $modelNews;
    protected $modelPage;
    protected $cache;
    protected $arrayLanguages;
    private $_searchForPaginated = false;
    protected static $_sortKey = NULL;
    protected static $_sortOrder = NULL;

    const SHOWS = 1;
    const PAGES = 2;
    const NEWS = 3;

    /**
     * Override del costruttore, inizializza le variabili di
     * classe per la fruibilita` all'interno dei metodi
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function __construct() {
        $options = array(
            'soap_version' => SOAP_1_1,
            'cache_wsdl' => false
        );
        $_configs = Thinkopen_DbInitialize::getInstance();
        $_wsAddress = $_configs::$iniConfig->wssearch->address;
        $this->client = new Zend_Soap_Client($_wsAddress, $options);
        $this->modelShow = Top::getModel("mapper/show");
        $this->modelNews = Top::getModel("mapper/news");
        $this->modelPage = Top::getModel("mapper/page");
        $this->arrayLanguages = Top::getLanguages(true);
        $this->_initCacheParams();
    }

    /**
     * Aggiunta di uno show all'interno dell'indice per la ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_showId
     */
    public function addShowToIndex($_showId, $_language = "it") {
        $this->client->writeIntoIndex($this->_createParamsFromShowForAdding($_showId, "false", $_language, $this->arrayLanguages[$_language]));
        $this->optimizeIndex();
    }

    /**
     * Aggiunta di una season all'interno dell'indice per la ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_newsId
     */
    public function addNewsToIndex($_newsId) {
        $this->client->writeIntoIndex($this->_createParamsFromNewsForAdding($_newsId));
        $this->optimizeIndex();
    }

    /**
     * Aggiunta di una season all'interno dell'indice per la ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_pageId
     */
    public function addPageToIndex($_pageId) {
        $this->client->writeIntoIndex($this->_createParamsFromPageForAdding($_pageId));
        $this->optimizeIndex();
    }

    /**
     * Ottimizzazione dell'indice della ricerca di Lucene,
     * da richiamare dopo gli inserimenti per tenere ben formattato
     * il FS dei documenti
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function optimizeIndex() {
        $_result = $this->client->optimizeIndex(array("environment" => APPLICATION_ENV));
        $this->cache->clean();
        return $_result;
    }

    /**
     * Recupero delle informazioni dello show per l'inserimento all'interno
     * dell'indice per la ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_showId
     * @return array
     */
    private function _createParamsFromShowForAdding($_showId, $_reindex = "false", $_shortLang = "it", $_language = "Italiano") {
        $show = $this->modelShow->load($_showId, $_shortLang);
        $_dataDal = explode("/", $show->getData_da());
        $_dataAl = explode("/", $show->getData_a());
        return array(
            "environment" => APPLICATION_ENV,
            "id_elemento" => $_showId,
            "titolo" => $show->getTitolo(),
            "immagine" => $show->getImg_wide(),
            "linkGeticket" => $show->getLink_geticket(),
            "location" => $show->getLocation(),
            "infoAcquisto" => $show->getInfo_acquisto(),
            "identifier" => $show->getIdentifier(),
            "content" => $show->getDescription(),
            "data_dal" => $_dataDal[2] . "-" . $_dataDal[1] . "-" . $_dataDal[0],
            "data_al" => $_dataAl[2] . "-" . $_dataAl[1] . "-" . $_dataAl[0],
            "categoryId" => 1,
            "language" => $_shortLang,
            "tabellaPrezzi" => $show->getTabella_prezzi(),
            "documentType" => 1,
            "reindex" => $_reindex,
            "long_language" => $_language
        );
    }

    /**
     * Recupero delle informazioni della season per l'inserimento all'interno
     * dell'indice della ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_pageId
     * @return array
     */
    private function _createParamsFromPageForAdding($_pageId, $_reindex = "false", $_language = NULL, $_longLanguage = NULL) {
        $page = $this->modelPage->load($_pageId, true);
        if (!$page) {
            throw new Exception("Impossibile caricare la pagina (pageId: {$_pageId})", 501);
        }
        return array(
            "environment" => APPLICATION_ENV,
            "id_elemento" => $_pageId,
            "titolo" => $page->getTitle(),
            "immagine" => $page->getImg_wide(),
            "linkGeticket" => "",
            "location" => "",
            "infoAcquisto" => "",
            "identifier" => $page->getIdentifier(),
            "content" => $page->getContent(),
            "data_dal" => date("Y-m-d"),
            "data_al" => date("Y-m-d"),
            "categoryId" => 1,
            "language" => ((is_null($_language)) ? $page->getLanguage() : $_language),
            "tabellaPrezzi" => "",
            "documentType" => 2,
            "reindex" => $_reindex,
            "long_language" => ((is_null($_longLanguage)) ? $this->arrayLanguages[$page->getLanguage()] : $_longLanguage)
        );
    }

    /**
     * Recupero delle informazioni della season per l'inserimento all'interno
     * dell'indice della ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_newsId
     * @return array
     */
    private function _createParamsFromNewsForAdding($_newsId, $_reindex = "false", $_language = NULL, $_longLanguage = NULL) {
        $news = $this->modelNews->load($_newsId, false, true);
        if (!$news) {
            throw new Exception("Impossibile caricare la news (pageId: {$_newsId})", 501);
        }
        $_parametriChiamata = array(
            "environment" => APPLICATION_ENV,
            "id_elemento" => $_newsId,
            "titolo" => $news->getTitle(),
            "immagine" => $news->getImg_wide(),
            "linkGeticket" => "",
            "location" => "",
            "infoAcquisto" => "",
            "identifier" => $news->getIdentifier(),
            "content" => $news->getContent(),
            "data_dal" => date("Y-m-d"),
            "data_al" => date("Y-m-d"),
            "categoryId" => 1,
            "language" => ((is_null($_language)) ? $news->getLanguage() : $_language),
            "tabellaPrezzi" => "",
            "documentType" => 3,
            "reindex" => $_reindex,
            "long_language" => ((is_null($_longLanguage)) ? $this->arrayLanguages[$news->getLanguage()] : $_longLanguage)
        );
        return $_parametriChiamata;
    }

    /**
     * Aggiunta di tutti gli show all'indice della ricerca, per
     * effettuare una reindicizzazione completa degli elementi
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function addAllShowsToIndex($_reindexer = true) {
        $collection = $this->modelShow->getCollection();
        $_index = 0;
        foreach ($collection as $_showElement) {
            if (!is_bool($_showElement)) {
                $_showId = $_showElement->getShow_id();
                if ($_reindexer) {
                    $_reindex = ($_index == 0) ? "true" : "false";
                    $_index++;
                } else {
                    $_reindex = "false";
                }
                foreach ($this->arrayLanguages as $_shortLang => $_language) {
                    $this->client->writeIntoIndex($this->_createParamsFromShowForAdding($_showId, $_reindex, $_shortLang, $_language));
                }
            }
        }
    }

    /**
     * Aggiunta di tutte le pagine all'indice della ricerca, per
     * effettuare una reindicizzazione completa degli elementi
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function addAllPagesToIndex($_reindexer = true) {
        foreach ($this->arrayLanguages as $_shortLang => $_longLanguage) {
            $collection = $this->modelPage->getCollection(array("language" => array("=", $_shortLang)));
            $_index = 0;
            foreach ($collection as $_pageElement) {
                if (!is_bool($_pageElement)) {
                    $_pageId = $_pageElement->getPage_id();
                    if ($_reindexer) {
                        $_reindex = ($_index == 0) ? "true" : "false";
                        $_index++;
                    } else {
                        $_reindex = "false";
                    }
                    $this->client->writeIntoIndex($this->_createParamsFromPageForAdding($_pageId, $_reindex, $_shortLang, $_longLanguage));
                }
            }
        }
    }

    /**
     * Aggiunta di tutte le season all'indice della ricerca, per
     * effettuare una reindicizzazione completa degli elementi
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function addAllNewsToIndex($_reindexer = true) {
        foreach ($this->arrayLanguages as $_shortLanguage => $_language) {
            $collection = $this->modelNews->getCollection(array("language" => array("=", $_shortLanguage)));
            $_index = 0;
            foreach ($collection as $_newsElement) {
                if (!is_bool($_newsElement)) {
                    $_newsId = $_newsElement->getNews_id();
                    if ($_reindexer) {
                        $_reindex = ($_index == 0) ? "true" : "false";
                        $_index++;
                    } else {
                        $_reindex = "false";
                    }
                    $this->client->writeIntoIndex($this->_createParamsFromNewsForAdding($_newsId, $_reindex, $_shortLanguage, $_language));
                }
            }
        }
    }

    /**
     * Realizza una completa reindicizzazione dell'indice di ricerca,
     * aggiungendo tutti gli shows, le pagine e le news
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return boolean
     */
    public function performCompleteReindex() {
        $this->addAllShowsToIndex(true);
        $this->addAllPagesToIndex(false);
        $this->addAllNewsToIndex(false);
        $this->optimizeIndex();
        return true;
    }

    /**
     * Encode in base64 e rimozione dei caratteri =, per l'inserimento
     * come indice nella cache
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $_input
     * @return string
     */
    private function _base64_encoding($_input) {
        return str_replace("=", "_", base64_encode($_input));
    }
    
     /**
     * Questo metodo va a cancellare dall'indice di Lucene un elemento.
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_documentId
     * @param int $_documentType
     * @param string $_language
     * @return bool
     */
    public function delete($_documentId, $_documentType, $_language) {
        $_parametri = array(
            "idElemento" => $_documentId,
            "documentType" => $_documentType,
            "language" => $_language,
            "environment" => APPLICATION_ENV
        );
        $_result = $this->client->deleteDocument($_parametri);
        return $_result;
    }

    /**
     * Effettuazione della ricerca attraverso interrogazione del
     * web-service sull'indice di Lucene creato, differenziato per lingua
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $queryString
     * @return array
     */
    public function search($queryString) {
        $_parametri = array(
            'text' => $this->_regexpQuerystring($queryString),
            'maxres' => 10000,
            "environment" => APPLICATION_ENV,
            "language" => ucfirst($this->arrayLanguages[Top::getCurrentLanguage()])
        );
        $_resultSetCached = $this->cache->load($this->_base64_encoding($_parametri['text'] . "_" . $this->arrayLanguages[Top::getCurrentLanguage()]));
        if (!$_resultSetCached) {
            $_resultSet = $this->client->searchForDocumentWithLanguage($_parametri);
            $this->cache->save(json_decode($_resultSet->return), $this->_base64_encoding($_parametri['text'] . "_" . $this->arrayLanguages[Top::getCurrentLanguage()]));
            $_resultSetCached = json_decode($_resultSet->return);
        }
        if ($this->_searchForPaginated) {
            return $_resultSetCached;
        } else {
            $_arrayDaTornare['resultSet'] = $_resultSetCached;
            $_arrayDaTornare['totalHits'] = count($_arrayDaTornare['resultSet']);
            $_arrayDaTornare['queryString'] = $queryString;
            return $_arrayDaTornare;
        }
    }

    /**
     * Effettuazione della ricerca attraverso interrogazione del
     * web-service sull'indice di Lucene creato, differenziato per lingua,
     * secondo la categoria di oggetti da ricercare (shows, pages, news)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $queryString
     * @param int $documentType Tipologia di documento all'interno della quale 
     * cercare (1 => SHOWS, 2 => PAGES, 3 => NEWS)
     * @return array
     */
    public function searchIntoDocType($queryString, $documentType) {
        $_parametri = array(
            'text' => $this->_regexpQuerystring($queryString),
            'document_type' => $documentType,
            'maxres' => 10000,
            "environment" => APPLICATION_ENV,
            "language" => ucfirst($this->arrayLanguages[Top::getCurrentLanguage()])
        );
        $_resultSetCached = $this->cache->load($this->_base64_encoding($_parametri['text'] . "_" . $this->arrayLanguages[Top::getCurrentLanguage()] . "_" . $documentType));
        if (!$_resultSetCached) {
            $_resultSet = $this->client->searchForDocumentAdvanced($_parametri);
            $_arrayDaTornare['resultSet'] = json_decode($_resultSet->return);
            $_arrayDaTornare['totalHits'] = count($_arrayDaTornare['resultSet']);
            $_arrayDaTornare['queryString'] = $queryString;
            $this->cache->save($_arrayDaTornare, $this->_base64_encoding($_parametri['text'] . "_" . $this->arrayLanguages[Top::getCurrentLanguage()] . "_" . $documentType));
        } else {
            $_arrayDaTornare = $_resultSetCached;
        }
        return $_arrayDaTornare;
    }

    /**
     * Effettuazione della ricerca avanzata attraverso interrogazione
     * del web-service sull'indice di Lucene creato
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @deprecated since version 0.9
     * @param string $_queryString
     * @param string $_location
     * @param string $_categoria
     * @param string|boolean $_dal
     * @param string|boolean $_al
     * @return string
     */
    public function advancedSearch($_queryString, $_location, $_categoria, $_dal = false, $_al = false) {
        $_queryString = $this->_constructQueryForAdvancedSearch($_queryString, $_location, $_dal, $_al);
        $_parametri = array(
            'text' => $_queryString,
            'maxres' => 10000
        );
        $_resultSetCached = $this->cache->load($this->_base64_encoding($_queryString . $_categoria));
        if (!$_resultSetCached) {
            if ($_categoria != "All") {
                $_parametri['categoria'] = $_categoria;
                $_resultSet = $this->client->searchForDocumentAdvanced($_parametri);
            } else {
                $_resultSet = $this->client->searchForDocument($_parametri);
            }
            $_arrayDaTornare['resultSet'] = json_decode($_resultSet->return);
            $_arrayDaTornare['totalHits'] = count($_arrayDaTornare['resultSet']);
            $_arrayDaTornare['queryString'] = $_queryString;
            $this->groupBySeason($_arrayDaTornare);
            $this->cache->save($_arrayDaTornare, $this->_base64_encoding($_queryString));
        } else {
            $_arrayDaTornare = $_resultSetCached;
        }
        return $_arrayDaTornare;
    }

    /**
     * Effettuazione della ricerca avanzata attraverso interrogazione
     * del web-service sull'indice di Lucene creato
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @deprecated since version 0.9
     * @param string $_queryString
     * @param string $_location
     * @param string $_categoria
     * @param string $_categoriaMax
     * @param string|boolean $_dal
     * @param string|boolean $_al
     * @return string
     */
    public function advancedSearchCategory($_queryString, $_location, $_categoria, $_categoriaMax, $_dal = false, $_al = false) {
        $_queryString = $this->_constructQueryForAdvancedSearch($_queryString, $_location, $_dal, $_al);
        $_parametri = array(
            'text' => $_queryString,
            'maxres' => 10000
        );
        $_resultSetCached = $this->cache->load($this->_base64_encoding($_queryString . $_categoria));
        if (!$_resultSetCached) {
            if ($_categoria != "All") {
                $_parametri['categoria'] = $_categoria;
                $_parametri['categoriaMax'] = $_categoriaMax;
                $_resultSet = $this->client->searchForDocumentAdvancedCategory($_parametri);
            } else {
                $_resultSet = $this->client->searchForDocument($_parametri);
            }
            $_arrayDaTornare['resultSet'] = json_decode($_resultSet->return);
            $_arrayDaTornare['totalHits'] = count($_arrayDaTornare['resultSet']);
            $_arrayDaTornare['queryString'] = $_queryString;
            $this->groupBySeason($_arrayDaTornare);
            $this->cache->save($_arrayDaTornare, $this->_base64_encoding($_queryString));
        } else {
            $_arrayDaTornare = $_resultSetCached;
        }
        return $_arrayDaTornare;
    }

    /**
     * Reg exp per togliere i caratteri speciali da una stringa
     * prima di effettuare la query di ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $str_orginal
     * @return string
     */
    private function _regexpQuerystring($str_orginal) {
        $str_orginal = str_replace("'", "*", $str_orginal);
        $str = preg_replace("/[^a-zA-Z0-9\ *]/", '', $str_orginal);
        $str = strtolower(trim($str));
        $str = quotemeta($str);
        $str = str_replace("-", "\-", $str);
        return $str;
    }

    /**
     * Restituzione della stringa compilata per l'effettuazione
     * della ricerca avanzata
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @deprecated since version 0.9
     * @param string $_queryString
     * @param string $_location
     * @param int $_categoria
     * @param string|boolean $_dal
     * @param string|boolean $_al
     * @return string
     */
    private function _constructQueryForAdvancedSearch($_queryString, $_location, $_dal, $_al) {
        $_searchString = array();
        if ($_queryString != "" && $_queryString != " ") {
            $_searchString[] = $this->_regexpQuerystring($_queryString);
        }
        if ($_location != "" && $_location != " ") {
            $_searchString[] = " (location: " . $_location . " OR citta: " . $_location . ")";
        }
        if ($_dal !== false && $_al !== false) {
            $_searchString[] = " +al:[" . str_replace("-", "", $_dal) . " TO " . str_replace("-", "", $_al) . "]";
        } elseif ($_dal !== false) {
            $_searchString[] = " +al:[" . str_replace("-", "", $_dal) . " TO 30000101]";
        } elseif ($_al !== false) {
            $_searchString[] = " +al:[19700101 TO " . str_replace("-", "", $_al) . "]";
        } else {
            $_searchString[] = " +al:[" . date("Ymd") . " TO 30000101]";
        }
        $_searchString[] = " -tipo_prodotto: \"prodotto\"";
        return implode(" AND", $_searchString);
    }

    /**
     * Inizializzazione dei parametri per la cache della ricerca
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    private function _initCacheParams() {
        $this->cache = Thinkopen_Cacher::getInstance("searcher", 3600);
    }

    /**
     * Funzione per il trim del testo dopo $max_char caratteri
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $stringa
     * @param int $max_char
     * @return string
     */
    public static function cutstring($stringa, $max_char) {
        if (strlen($stringa) > $max_char) {
            $stringa_tagliata = substr($stringa, 0, $max_char);
            $last_space = strrpos($stringa_tagliata, " ");
            $stringa_ok = substr($stringa_tagliata, 0, $last_space);
            return $stringa_ok . "...";
        } else {
            return $stringa;
        }
    }

    /**
     * Metodo per la restituzione di un paginatore avente al suo interno
     * i risultati della ricerca effettuata, suddivisi per pagine
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $_queryString
     * @param int $_pageNumber
     * @param int $_itemsPerPage
     * @return \Zend_Paginator
     */
    public function paginatedSearch($_queryString, $_pageNumber = 1, $_itemsPerPage = 5, $_sortKey = NULL, $_sortOrder = NULL) {
        try {
            $this->_searchForPaginated = true;
            $_resultArray = $this->search($_queryString);
            $this->_sortResults($_resultArray, $_sortKey, $_sortOrder);
            $_adapter = new Zend_Paginator_Adapter_Array($_resultArray);
            $_paginator = new Zend_Paginator($_adapter);
            return $_paginator->setItemCountPerPage($_itemsPerPage)->setCurrentPageNumber($_pageNumber);
        } catch (Exception $e) {
            $logger = Thinkopen_Logger::getInstance();
            $logger->log("SEARCHER", Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * Metodo statico privato per l'ordinamento degli elementi della ricerca
     * all'interno della paginatedSearch
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $a
     * @param array $b
     * @return int
     */
    private static function _sorter($a, $b) {
        if (self::$_sortOrder === "ASC") {
            if ($a->{self::$_sortKey} < $b->{self::$_sortKey}) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if ($a->{self::$_sortKey} < $b->{self::$_sortKey}) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    /**
     * Metodo privato per l'ordinamento della ricerca effettuata
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $_resultArray
     * @param string $_sortKey
     * @param string $_sortOrder
     */
    private function _sortResults(&$_resultArray, $_sortKey, $_sortOrder = "DESC") {
        if ($_sortKey !== NULL) {
            self::$_sortKey = $_sortKey;
            self::$_sortOrder = $_sortOrder;
            usort($_resultArray, array("Thinkopen_Searcher", "_sorter"));
        }
    }

}
