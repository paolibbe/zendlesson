<?php

/**
 * Description of PaginatorAdapter
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Thinkopen_PaginatorAdapter extends Zend_Paginator_Adapter_DbSelect {

    private $_class = null;

    public function __construct(\Zend_Db_Select $select, $class) {
        $this->_class = $class;
        parent::__construct($select);
    }

    public function getItems($offset, $itemCountPerPage) {
        $class = $this->_class;
        $this->_select->limit($itemCountPerPage, $offset);
        $rowset = $this->_select->getTable()->fetchAll($this->_select);
        $_resultSet = array();
//        $_statoModel = new Ordini_Stato();
        foreach ($rowset as $row) {
            $objectModel = new $class();
            $_object = $objectModel->load($row['id']);
//            if(in_array("stato", array_keys(get_object_vars($_object)))){
//                $_object->stato = $_statoModel->load($_object->stato)->descrizione;
//            }
            $_resultSet[] = $_object;
        }
        return $_resultSet;
    }

}
