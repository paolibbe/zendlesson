<?php

require_once '../library/Thinkopen/DbInitialize.php';
require_once '../library/Thinkopen/Cacher.php';

/**
 * Classe per la gestione delle connessioni verso Twitter ed il conseguente
 * recupero della lista di tweets e utenti per il widget di Twitter
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @version 1.1
 */
class Thinkopen_Twitter {

    protected $_session;
    protected static $_instance = NULL;
    private $_oauth_token = NULL;
    private $_oauth_token_secret = NULL;
    private $_username = NULL;
    private $_consumerKey = NULL;
    private $_consumerSecret = NULL;

    /**
     * @var \Zend_Service_Twitter
     */
    private $twitter = NULL;

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    protected function __construct() {
        $_iniConfig = Thinkopen_DbInitialize::getInstance();
        $this->_oauth_token = $_iniConfig::$iniConfig->twitter->oauth_token;
        $this->_oauth_token_secret = $_iniConfig::$iniConfig->twitter->oauth_token_secret;
        $this->_consumerKey = $_iniConfig::$iniConfig->twitter->consumerKey;
        $this->_consumerSecret = $_iniConfig::$iniConfig->twitter->consumerSecret;
        $this->_username = $_iniConfig::$iniConfig->twitter->username;
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return \Thinkopen_Twitter
     */
    public static function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new Thinkopen_Twitter();
        }
        return self::$_instance;
    }

    /**
     * Metodo per la connessione verso Twitter
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @throws Exception
     */
    private function _connect() {
        $twitter = new Zend_Service_Twitter(array(
            'username' => $this->_username,
            'accessToken' => array(
                'token' => $this->_oauth_token,
                'secret' => $this->_oauth_token_secret,
            ),
            'oauthOptions' => array(
                'consumerKey' => $this->_consumerKey,
                'consumerSecret' => $this->_consumerSecret,
            ),
            'http_client_options' => array(
                'adapter' => 'Zend_Http_Client_Adapter_Curl',
            ),
        ));
        $response = $twitter->accountVerifyCredentials();
        if ($response->isSuccess()) {
            $this->twitter = $twitter;
        } else {
            $logger = Thinkopen_Logger::getInstance();
            $logger->log("Twitter", Thinkopen_Logger::ERR, $response->getErrors());
            throw new Exception("Impossibile connettersi a Twitter", 503);
        }
    }

    /**
     * Metodo per il picking random degli utenti dalla lista degli amici
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $_userList
     * @param int $_count
     * @return array
     */
    private function _randomChooseFriends($_userList, $_count) {
        $_randomKeys = array_rand($_userList, $_count);
        $_randomArray = array_intersect_key($_userList, array_flip($_randomKeys));
        return $_randomArray;
    }

    /**
     * Metodo per il recupero da Twitter della lista degli amici
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_count
     * @param boolean $_random
     * @return array|boolean
     */
    private function _getFriendsList($_count, $_random = true) {
        try {
            $this->_connect();
            $_users = $this->twitter->get('friends/list', array('count' => (($_random) ? (5 * $_count) : ($_count))));
            $_response = new Zend_Service_Twitter_Response($_users);
            if ($_response->isSuccess()) {
                $_userList = $_response->toValue();
                return ($_random) ? $this->_randomChooseFriends($_userList->users, $_count) : $_userList->users;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Metoto per recuperare le immagini degli ultimi tweets effettuati
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param int $_count
     * @return boolean|array
     */
    private function _getTweetsMediaImage($_count) {
        try {
            $this->_connect();
            $_users = $this->twitter->get('search/tweets', array('count' => $_count, 'q' => 'from:teatrosancarlo filter:images', 'include_entities' => 1, "result_type" => "recent"));
            $_response = new Zend_Service_Twitter_Response($_users);
            if ($_response->isSuccess()) {
                $_return = array();
                $_userList = json_decode($_response->getRawResponse());
                foreach ($_userList as $obj) {
                    foreach ($obj as $ob) {
                        $entities = $ob->entities;
                        $media = $entities->media;
                        if ($media[0]->media_url) {
                            array_push($_return, array("image"=>$media[0]->media_url,"url"=>$media[0]->url));
                        }
                    }
                }
                return $_return;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo per il recupero da Twitter della lista degli ultimi tweets
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_count
     * @return array|boolean
     */
    private function _getLastTweets($_count) {
        try {
            $this->_connect();
            $_response = $this->twitter->statusesUserTimeline(array("count" => $_count));
            if ($_response->isSuccess()) {
                return $_response->toValue();
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Metodo pubblico per il recupero degli ultimi N Tweets postati sulla Timeline
     * dell'utente corrente. Verifica l'esistenza in cache del risultato (cache 30minuti)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_count Numero di elementi estratti (default 10)
     * @return array
     */
    public function getLastTweets($_count = 10) {
        $cache = Thinkopen_Cacher::getInstance("twitter", 1800);
        if (!$_response = $cache->load("lastweets")) {
            $tweets = $this->_getLastTweets($_count);
            $_response = array();
            if ($tweets !== false) {
                foreach ($tweets as $_tweet) {
                    $_singleTweet = new stdClass();
                    $_singleTweet->text = $_tweet->text;
                    $_singleTweet->date = $_tweet->created_at;
                    $_singleTweet->userName = $_tweet->user->name;
                    $_singleTweet->userLink = "https://twitter.com/" . $_tweet->user->screen_name;
                    $_singleTweet->userAtName = "@" . $_tweet->user->screen_name;
                    $_singleTweet->userUrl = $_tweet->user->url;
                    array_push($_response, $_singleTweet);
                }
                $cache->save($_response, "lastweets");
            }
        }
        return $_response;
    }

    /**
     * Metodo pubblico per il recupero di N utenti "amici" dell'utente corrente.
     * Verifica l'esistenza in cache del risultato (cache 30minuti)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_count Numero di elementi estratti (default 9)
     * @return array
     */
    public function getFriends($_count = 9) {
        $cache = Thinkopen_Cacher::getInstance("twitter", 1800);
        if (!$_response = $cache->load("friendslist")) {
            $friends = $this->_getFriendsList($_count);
            $_response = array();
            if ($friends !== false) {
                foreach ($friends as $_friend) {
                    $_singleFriend = new stdClass();
                    $_singleFriend->userName = $_friend->name;
                    $_singleFriend->userLink = "https://twitter.com/" . $_friend->screen_name;
                    $_singleFriend->userAtName = "@" . $_friend->screen_name;
                    $_singleFriend->userUrl = $_friend->url;
                    $_singleFriend->userThumb = $_friend->profile_image_url;
                    array_push($_response, $_singleFriend);
                }
            }
            $cache->save($_response, "friendslist");
        }
        return $_response;
    }

    public function getTweetsImage($_count = 9) {
        $cache = Thinkopen_Cacher::getInstance("twitter", 1800);
        if (!$_response = $cache->load("medialist")) {
            $medias = $this->_getTweetsMediaImage($_count);
            $_response = array();
            if (count($medias) > 0) {
                $_response = $medias;
            }
            $cache->save($_response, "medialist");
        }
        return $_response;
    }

}
