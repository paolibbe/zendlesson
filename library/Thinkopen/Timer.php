<?php

/**
 * Description of Cacher
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
final class Thinkopen_Timer {

    private $_startTiming = 0.0;
    private $_memoryUsage = 0.0;
    private $logger;

    /**
     * @var Thinkopen_Timer
     */
    protected static $_timer;

    public function setTimer() {
        $microtime = microtime();
        $microtimeExploded = explode(' ', $microtime);
        $time = $microtimeExploded[1] + $microtimeExploded[0];
        $this->_startTiming = $time;
        $this->_memoryUsage = memory_get_usage();
    }

    private function convert($size) {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    /**
     * Funzione di timing 
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param String $_namingProcedures
     * @return String
     */
    public function timing($_namingProcedures = "Procedures", $_printing = true) {
        $microtime = microtime();
        $microtimeExploded = explode(' ', $microtime);
        $time = $microtimeExploded[1] + $microtimeExploded[0];
        $finish = $time;
        $total_time = round(($finish - $this->_startTiming), 4);
        $_memoryUsage = memory_get_usage() - $this->_memoryUsage;
        if ($_printing) {
            echo ">>> " . $_namingProcedures . ' generated in ' . $total_time . ' seconds. >> Memory Usage:: ' . $this->convert($_memoryUsage) . '<br />';
            flush();
            ob_flush();
        } else{
            $this->logger->log($_namingProcedures, Zend_Log::ALERT, ">>> " . $_namingProcedures . ' generated in ' . $total_time . ' seconds. >> Memory Usage:: ' . $this->convert($_memoryUsage));
        }
        return $_namingProcedures . ' generated in ' . $total_time . ' seconds. >> Memory Usage:: ' . $_memoryUsage;
    }

    /**
     * Singleton
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param boolean $_init
     * @return Thinkopen_Timer
     */
    public static function getInstance($_init = false) {
        if (Thinkopen_Timer::$_timer === null) {
            Thinkopen_Timer::$_timer = new Thinkopen_Timer();
        }
        if ($_init !== false) {
            Thinkopen_Timer::$_timer->setTimer();
        }
        return Thinkopen_Timer::$_timer;
    }

    private function __construct() {
        $this->_startTiming = 0.0;
        $this->logger = Thinkopen_Logger::getInstance();
    }

}
