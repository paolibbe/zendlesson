<?php

/**
 * Description of Verticalabstractmodel
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
abstract class Thinkopen_Verticalabstractmodel extends Zend_Db_Table_Abstract {

    protected $_primaryKey;
    protected static $_instance = array();
    protected $_tableName;
    protected $_mapperName;
    protected $_eavTablesName;
    protected $_eavAttributeTableName;
    protected $_staticAttributes;
    protected $_attributes = array();
    protected $_cannotSave = false;
    protected $_realAttributesForLanguage = array();
    private $__order = NULL;
    public $totalResultPaginatedCollection = 0;

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function __construct($_data = array()) {
        $this->_primaryKey = ucfirst($this->retrievePrimaryKey());
        $this->_tableName = $this->retrieveTableName();
        $this->_mapperName = $this->retrieveMapperName();
        $config = array('name' => $this->_tableName);
        parent::__construct($config);
        if (count($_data) != 0) {
            $this->_setData($_data);
        } else {
            $this->_setData(array_flip($this->_getAttributes(true)), true);
        }
        self::$_instance[get_called_class()] = $this;
        return $this;
    }

    private function _getAttributes($_typo = false) {
        $_array = $this->getAdapter()
                ->query("SELECT SQL_CACHE attributes.`attribute_name`, attributes.`attribute_id`, attributes.`attribute_type` FROM `" . $this->_eavAttributeTableName . "` attributes")
                ->fetchAll(PDO::FETCH_CLASS);
        $_arrayNormal = array();
        if ($_typo) {
            $_arrayComplete = array();
            for ($i = 0; $i < count($_array); $i++) {
                $attribute = new stdClass();
                $attribute->attribute_name = $_array[$i]->attribute_name;
                $attribute->attribute_type = $_array[$i]->attribute_type;
                $_arrayComplete[$_array[$i]->attribute_id] = $attribute;
                array_push($_arrayNormal, $_array[$i]->attribute_name);
            }
            $this->_staticAttributes = $_arrayComplete;
        } else {
            for ($i = 0; $i < count($_array); $i++) {
                array_push($_arrayNormal, $_array[$i]->attribute_name);
            }
        }
        return $_arrayNormal;
    }

    public function setData($_data) {
        if (count($_data) != 0) {
            $this->_setData($_data);
        } else {
            return false;
        }
        return $this;
    }

    public function getAttributes($_entity = false) {
        return ($_entity) ? $this->_getCols() : $this->_getAttributes();
    }

    public function countRows() {
        $_count = $this->getAdapter()->fetchOne('SELECT COUNT(*) AS count FROM ' . $this->_tableName);
        return $_count;
    }

    public function getData() {
        foreach ($this->_attributes as $_key => $_attributes) {
            if (is_a($_attributes, "Zend_Db_Table_Row_Abstract")) {
                $_toReturn[$_key] = $_attributes->value;
            } elseif (!is_object($_attributes)) {
                $_toReturn[$_key] = $_attributes;
            } else {
                $_toReturn[$_key] = $_attributes->getValue();
            }
        }
        return $_toReturn;
    }

    protected function provideInformationsForRelatedTables() {
        $_referencedClass = $this->retrieveTableReference();
        $_referencedColumns = $this->retrieveColsForReference();
        if ($_referencedClass != "") {
            $this->addReference($_referencedClass, array($_referencedColumns[0]), $_referencedClass, array($_referencedColumns[1]));
        }
    }

    protected function _setData($_data, $_init = false) {
        if (!$_init) {
            foreach ($_data as $_key => $_value) {
                $this->_attributes[$_key] = ($_init) ? NULL : $_value;
            }
        } else {
            $_object = array();
            foreach (array_flip($this->_getCols()) as $_key => $_column) {
                $_object[$_key] = NULL;
            }
            $this->_fillWithDbTableRowsData($_object, NULL);
            $this->_setData($_object);
        }
    }

    public static function getInstance() {
        if (self::$_instance[get_called_class()] === NULL) {
            $_className = get_called_class();
            self::$_instance[get_called_class()] = new $_className();
        }
        return self::$_instance[get_called_class()];
    }

    private function parseDocComment($str, $tag = '') {
        if (empty($tag)) {
            return $str;
        }
        $matches = array();
        preg_match("/" . $tag . ":(.*)(\\r\\n|\\r|\\n)/U", $str, $matches);
        if (isset($matches[1])) {
            return trim($matches[1]);
        }

        return '';
    }

    private function retrievePrimaryKey() {
        $annotations = new ReflectionClass($this);
        return $this->parseDocComment($annotations->getDocComment(), "@PrimaryKey");
    }

    private function retrieveTableReference() {
        $annotations = new ReflectionClass($this);
        return $this->parseDocComment($annotations->getDocComment(), "@ClassReferenced");
    }

    private function retrieveMapperName() {
        $_classExplodedName = explode("_", get_called_class());
        return end($_classExplodedName);
    }

    private function retrieveColsForReference() {
        $annotations = new ReflectionClass($this);
        $_columnsReferenced = $this->parseDocComment($annotations->getDocComment(), "@ColsReferenced");
        return explode("->", $_columnsReferenced);
    }

    private function _retrieveEavTablesName($_baseTable, $_tableSuffix) {
        $this->_eavAttributeTableName = str_replace("_entity", "_attributes", $_baseTable) . $_tableSuffix;
        return array(
            "date" => str_replace("_entity", "_date", $_baseTable) . $_tableSuffix,
            "int" => str_replace("_entity", "_int", $_baseTable) . $_tableSuffix,
            "text" => str_replace("_entity", "_text", $_baseTable) . $_tableSuffix,
            "varchar" => str_replace("_entity", "_varchar", $_baseTable) . $_tableSuffix
        );
    }

    private function retrieveTableName() {
        $annotations = new ReflectionClass($this);
        $configs = Thinkopen_DbInitialize::getInstance();
        $_tableSuffix = $configs::$iniConfig->database->table->suffix;
        $_tableAnnotatedName = $this->parseDocComment($annotations->getDocComment(), "@Table");
        $_baseTable = str_replace($_tableSuffix, "", $_tableAnnotatedName);
        $this->_eavTablesName = $this->_retrieveEavTablesName($_baseTable, $_tableSuffix);
        return $_baseTable . $_tableSuffix;
    }

    private function _rotateDate($originalDate, $_american = false) {
        $_americanDate = date("Y-m-d", strtotime($date = str_replace('/', '-', $originalDate)));
        return ($_american) ? $_americanDate : date("d/m/Y", strtotime($originalDate));
    }

    private function _isDate($attributeName) {
        foreach ($this->_staticAttributes as $_attribute) {
            if ($_attribute->attribute_name == $attributeName) {
                return ($_attribute->attribute_type == "date") ? true : false;
            }
        }
        return false;
    }

    public function __call($methodName, $params = null) {
        $methodPrefix = substr($methodName, 0, 3);
        $key = strtolower(substr($methodName, 3));
        $_isUppercase = ctype_upper(substr($methodName, 3, 1));
        if ($methodPrefix == 'set' && count($params) == 1 && $_isUppercase) {
            if (!in_array($key, array_keys($this->_attributes))) {
                return $this;
            } else {
                $value = $params[0];
                if ($value != "" && $value !== NULL) {
                    if (in_array($key, $this->getAttributes())) {
                        if (is_a($this->_attributes[$key], "Zend_Db_Table_Row_Abstract")) {
                            $this->_attributes[$key]->value = ($this->_isDate($key)) ? $this->_rotateDate($value, true) : $value;
                        } else {
                            $this->_attributes[$key]->setValue(($this->_isDate($key)) ? $this->_rotateDate($value, true) : $value);
                        }
                    } else {
                        $this->_attributes[$key] = $value;
                    }
                }
                return $this;
            }
        } elseif ($methodPrefix == 'get') {
            if ($key == "reallangattributes") {
                return $this->_realAttributesForLanguage;
            }
            if (in_array($key, array_keys($this->_attributes))) {
                if (in_array($key, $this->getAttributes())) {
                    if (isset($this->_attributes[$key]->value)) {
                        return ($this->_isDate($key)) ? $this->_rotateDate($this->_attributes[$key]->value) : $this->_attributes[$key]->value;
                    } else {
                        return "";
                    }
                } else {
                    return $this->_attributes[$key];
                }
            } else {
                return;
            }
        } elseif (!in_array($methodName, array_flip(get_class_methods($this)))) {
            throw new Exception("Method \"" . $methodName . "\" doesn't exist in " . get_called_class(), 500);
        }
    }

    protected function _mapAttributeIdToName($_attributeId) {
        return $this->_staticAttributes[$_attributeId]->attribute_name;
    }

    protected function _constructDependentQueries($_tableName, $_language) {
        return $this->select()->from($_tableName)->where("`language` = ?", $_language);
    }

    protected function _appendDependentRowValues($_object, $_blocks, $_attributeIds = array()) {
        for ($i = 0; $i < $_blocks->count(); $i++) {
            if (!in_array($_blocks->current()->attribute_id, $_attributeIds)) {
                $_object[$this->_mapAttributeIdToName($_blocks->current()->attribute_id)] = $_blocks->current();
            }
            $_blocks->next();
        }
        return $_object;
    }

    public function fillWithDbTableRowsData(&$_object, $_showId, $_language = "it") {
        $this->_fillWithDbTableRowsData($_object, $_showId, $_language);
    }

    protected function _fillWithDbTableRowsData(&$_object, $_showId, $_language = "it") {
        foreach ($this->_staticAttributes as $_attribute_id => $_attribute) {
            if (!isset($_object[$_attribute->attribute_name]) || $_object[$_attribute->attribute_name] === NULL) {
                $_referenceModel = Top::getModel("bo_dbTable/" . strtolower($this->_mapperName . $_attribute->attribute_type));
                $_referenceModel->setLanguage($_language)->setAttribute_id($_attribute_id);
                $_reference = $_referenceModel->retrieveColsForReference();
                $_referenceCols = "set" . ucFirst($_reference[0]);
                $_referenceModel->{$_referenceCols}($_showId);
                $_referenceModel->setValue(NULL);
                $_object[$_attribute->attribute_name] = $_referenceModel;
            }
        }
    }

    public function fillWithFallback($_objectData, &$_object) {
        $this->_fillWithFallback($_objectData, $_object);
    }

    protected function _fillWithFallback($_objectData, &$_object) {
        $_attributeIdsNotFallbackable = array();
        foreach ($this->_staticAttributes as $_attribute_id => $_attribute) {
            if (isset($_object[$_attribute->attribute_name]) && $_object[$_attribute->attribute_name] !== NULL) {
                array_push($_attributeIdsNotFallbackable, $_attribute_id);
                array_push($this->_realAttributesForLanguage, $this->_mapAttributeIdToName($_attribute_id));
            }
        }
        foreach ($this->_eavTablesName as $_type => $_tableName) {
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_" . $this->_mapperName . $_type, NULL, $this->_constructDependentQueries($_tableName, "it"));
            if (count($_blocks) != 0) {
                $_object = $this->_appendDependentRowValues($_object, $_blocks, $_attributeIdsNotFallbackable);
            }
        }
    }

    public function load($_id, $_language = NULL, $_fallback = true) {
        try {
            if ($_language === NULL) {
                $_language = Top::getCurrentLanguage();
            }
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_object = $_objectData->toArray();
            foreach ($this->_eavTablesName as $_type => $_tableName) {
                $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_" . $this->_mapperName . $_type, NULL, $this->_constructDependentQueries($_tableName, $_language));
                if (count($_blocks) != 0) {
                    $_object = $this->_appendDependentRowValues($_object, $_blocks);
                }
            }
            if ($_fallback === true) {
                $this->_fillWithFallback($_objectData, $_object);
            }
            $this->_fillWithDbTableRowsData($_object, $_id, $_language);
            $this->_setData($_object);
            return $this;
        } catch (Exception $e) {
            $_logger = Thinkopen_Logger::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage() . " (PK:: {$_id})");
            return false;
        }
    }

    public function loadByAttribute($_attributeName, $_value = NULL) {
        if (!in_array($_attributeName, $this->_getCols()) && !is_array($_attributeName)) {
            throw new Exception("Attribute \"" . $_attributeName . "\" is not part of object's attribute set", 500, NULL);
        } else {
            foreach (array_keys($_attributeName) as $_attributeSearched) {
                if (!in_array($_attributeSearched, $this->_getAttributes()) && !in_array($_attributeSearched, $this->_getCols())) {
                    throw new Exception("Attribute \"" . $_attributeSearched . "\" is not part of object's attribute set (" . json_encode($this->_getAttributes()) . ")", 500, NULL);
                }
            }
        }
        try {
            if (!is_array($_attributeName)) {
                $_attributes = array($_attributeName => $_value);
            } else {
                $_attributes = $_attributeName;
            }
            $_whereClause = "";
            foreach ($_attributes as $_key => $_value) {
                $_whereClause .= (($_key == 0) ? ($_key . " = '" . $_value . "'") : (" && " . $_key . " = '" . $_value . "'"));
            }
            $_objectData = $this->fetchRow($_whereClause);
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_object = $_objectData->toArray();
            foreach ($this->_eavTablesName as $_type => $_tableName) {
                $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_" . $this->_mapperName . $_type, NULL, $this->_constructDependentQueries($_tableName, Top::getCurrentLanguage()));
                if (count($_blocks) != 0) {
                    $_object = $this->_appendDependentRowValues($_object, $_blocks);
                }
            }
            $this->_fillWithFallback($_objectData, $_object);
            $_pkId = strtolower($this->_primaryKey);
            $this->_fillWithDbTableRowsData($_object, $_objectData->{$_pkId}, Top::getCurrentLanguage());
            $this->_setData($_object);
            $this->_cannotSave = true;
            return $this;
        } catch (Exception $e) {
//            echo $e->getTraceAsString();
//            echo $e->getMessage();
            return false;
        }
    }

    /**
     * 
     * @param \Zend_Db_Table_Select $select
     * @param string $_attributeName
     * @param string $_operand
     * @param mixed $_matchingValue
     * @return NULL
     */
    private function _addJoinFromEntity($select, $_attributeName, $_operand, $_matchingValue) {
        if (!in_array($_attributeName, $this->_getCols())) {
            return;
        } else {
            $select->where("`en`.`{$_attributeName}` {$_operand} ?", $_matchingValue);
        }
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param \Zend_Db_Table_Select $select
     * @param string $_attributeType
     * @param array $_joinsUsed
     * @return string
     */
    private function _addJoinsFromAttribute(&$select, $_attributeType, &$_joinsUsed) {
        $_appendAlias = "";
        if (in_array($_attributeType, $_joinsUsed) || $_attributeType === NULL) {
            $_appendAlias = "_" . count(array_keys($_joinsUsed, $_attributeType));
        }
        foreach ($this->_eavTablesName as $_type => $_tableName) {
            if ($_attributeType == $_type) {
                $select->join(array("t_" . $_type . $_appendAlias => $_tableName), "`en`.`" . strtolower($this->_primaryKey) . "` = `t_" . $_type . $_appendAlias . "`.`" . strtolower($this->_primaryKey) . "`", array());
                array_push($_joinsUsed, $_type);
                break;
            }
        }
        return "t_" . $_type . $_appendAlias;
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param \Zend_Db_Table_Select $select
     * @param string $_attributeType
     * @param string $_operand
     * @param mixed $_matchingValue
     */
    private function _addWhereClauseFromAttribute(&$select, $_attributeKey, $_attributeType, $_operand, $_matchingValue, $_language, $_tableName, $_orWhere) {
        if (!$_orWhere) {
            switch (strtoupper($_operand)) {
                case "BETWEEN":
                case "IN":
                    if ($_language !== NULL) {
                        $select->where("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `t_" . $_attributeType . "`.`language` = '{$_language}' AND `{$_tableName}`.`value` {$_operand} $_matchingValue");
                    } else {
                        $select->where("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `{$_tableName}`.`value` {$_operand} $_matchingValue");
                    }
                    break;
                default:
                    if ($_language !== NULL) {
                        $select->where("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `t_" . $_attributeType . "`.`language` = '{$_language}' AND `{$_tableName}`.`value` {$_operand} ?", $_matchingValue);
                    } else {
                        $select->where("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `{$_tableName}`.`value` {$_operand} ?", $_matchingValue);
                    }
                    break;
            }
        } else {
            switch (strtoupper($_operand)) {
                case "BETWEEN":
                case "IN":
                    if ($_language !== NULL) {
                        $select->orWhere("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `t_" . $_attributeType . "`.`language` = '{$_language}' AND `{$_tableName}`.`value` {$_operand} $_matchingValue");
                    } else {
                        $select->orWhere("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `{$_tableName}`.`value` {$_operand} $_matchingValue");
                    }
                    break;
                default:
                    if ($_language !== NULL) {
                        $select->orWhere("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `t_" . $_attributeType . "`.`language` = '{$_language}' AND `{$_tableName}`.`value` {$_operand} ?", $_matchingValue);
                    } else {
                        $select->orWhere("`{$_tableName}`.`attribute_id` = {$_attributeKey} AND `{$_tableName}`.`value` {$_operand} ?", $_matchingValue);
                    }
                    break;
            }
        }
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $_filters Es. array("nome" => array("like", "%drago%"))
     * @param string $_language Lingua all'interno della quale effettuare il matching degli elementi (default NOT SET)
     * @return array
     * @throws Exception
     */
    public function getCollection($_filters, $_language = NULL, $_limit = NULL, $_order = NULL, $_orWhere = false) {
        foreach (array_keys($_filters) as $_attributeSearched) {
            if (!in_array($_attributeSearched, $this->_getAttributes()) && !in_array($_attributeSearched, $this->_getCols())) {
                throw new Exception("Attribute \"" . $_attributeSearched . "\" is not part of object's attribute set", 500, NULL);
            }
        }
        try {
            $_attributesWithTypo = $this->_staticAttributes;
            $_joinsUsed = array();
            $select = $this->select()->distinct()->from(array("en" => $this->_tableName));
            foreach ($_filters as $_attributeName => $_value) {
                $_attributeType = NULL;
                $_attributeKey = NULL;
                $_operand = $_value[0];
                $_matchingValue = $_value[1];
                foreach ($_attributesWithTypo as $_attributesKey => $_attributesInfos) {
                    if ($_attributesInfos->attribute_name === $_attributeName) {
                        $_attributeType = $_attributesInfos->attribute_type;
                        $_attributeKey = $_attributesKey;
                        break;
                    }
                }
                if ($_attributeType === NULL) {
                    $this->_addJoinFromEntity($select, $_attributeName, $_operand, $_matchingValue);
                } else {
                    $_tableWhereName = $this->_addJoinsFromAttribute($select, $_attributeType, $_joinsUsed);
                    $this->_addWhereClauseFromAttribute($select, $_attributeKey, $_attributeType, $_operand, $_matchingValue, $_language, $_tableWhereName, $_orWhere);
                }
            }
            if (!is_null($_limit)) {
                $select->limit($_limit);
            }
            if (!is_null($_order)) {
                $select->order($_order);
            }
            $_objectData = $this->fetchAll($select);
            $_objects = $_objectData->toArray();
            $_retrievedObjects = array();
            $_pkId = strtolower($this->_primaryKey);
            $_className = get_called_class();
            if (count($_objects) == 0) {
                throw new Exception("No matching", 501);
            }
            foreach ($_objectData as $_object) {
                $_arrayObject = $_object->toArray();
                foreach ($this->_eavTablesName as $_type => $_tableName) {
                    $_blocks = $_object->findDependentRowset("Bo_Model_DbTable_" . $this->_mapperName . $_type, NULL, $this->_constructDependentQueries($_tableName, Top::getCurrentLanguage()));
                    if (count($_blocks) != 0) {
                        $_arrayObject = $this->_appendDependentRowValues($_arrayObject, $_blocks);
                    }
                }
                $_realObject = new $_className();
                $_realObject->fillWithFallback($_object, $_arrayObject);
                $_realObject->fillWithDbTableRowsData($_arrayObject, $_object->{$_pkId}, Top::getCurrentLanguage());
                $_realObject->setData($_arrayObject);
                array_push($_retrievedObjects, $_realObject);
            }
            $this->_cannotSave = true;
            return $_retrievedObjects;
        } catch (Exception $e) {
            return false;
        }
    }

    private function _checkAttributesForJoinsAndOrder($_attributeName, &$_attributeType, &$_attributeKey) {
        foreach ($this->_staticAttributes as $_attributesKey => $_attributesInfos) {
            if ($_attributesInfos->attribute_name === $_attributeName) {
                $_attributeType = $_attributesInfos->attribute_type;
                $_attributeKey = $_attributesKey;
                break;
            }
        }
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $_filters
     * @param int $_pageNumber (default 1)
     * @param string $_language
     * @param int $_itemsPerPage (default 10)
     * @return boolean|\Zend_Paginator
     * @throws Exception
     */
    public function getPaginatedCollection($_filters, $_pageNumber, $_language = NULL, $_itemsPerPage = 10, $_order = NULL, $_orWhere = false) {
        foreach (array_keys($_filters) as $_attributeSearched) {
            if (!in_array($_attributeSearched, $this->_getAttributes()) && !in_array($_attributeSearched, $this->_getCols())) {
                throw new Exception("Attribute \"" . $_attributeSearched . "\" is not part of object's attribute set", 500, NULL);
            }
        }
        try {
            $_attributesWithTypo = $this->_staticAttributes;
            $_joinsUsed = array();
            $select = $this->select()->distinct()->from(array("en" => $this->_tableName));
            foreach ($_filters as $_attributeName => $_value) {
                $_attributeType = NULL;
                $_attributeKey = NULL;
                $_operand = $_value[0];
                $_matchingValue = $_value[1];
                $this->_checkAttributesForJoinsAndOrder($_attributeName, $_attributeType, $_attributeKey);
                if ($_attributeType === NULL) {
                    $this->_addJoinFromEntity($select, $_attributeName, $_operand, $_matchingValue);
                } else {
                    $_tableWhereName = $this->_addJoinsFromAttribute($select, $_attributeType, $_joinsUsed);
                    $this->_addWhereClauseFromAttribute($select, $_attributeKey, $_attributeType, $_operand, $_matchingValue, $_language, $_tableWhereName, $_orWhere);
                }
            }
            $_objectData = $this->fetchAll($select);
            $_objects = $_objectData->toArray();
            $_retrievedObjects = array();
            $_pkId = strtolower($this->_primaryKey);
            $_className = get_called_class();
            if (count($_objects) == 0) {
                throw new Exception("No matching", 501);
            }
            foreach ($_objectData as $_object) {
                $_arrayObject = $_object->toArray();
                foreach ($this->_eavTablesName as $_type => $_tableName) {
                    $_blocks = $_object->findDependentRowset("Bo_Model_DbTable_" . $this->_mapperName . $_type, NULL, $this->_constructDependentQueries($_tableName, Top::getCurrentLanguage()));
                    if (count($_blocks) != 0) {
                        $_arrayObject = $this->_appendDependentRowValues($_arrayObject, $_blocks);
                    }
                }
                $_realObject = new $_className();
                $_realObject->fillWithFallback($_object, $_arrayObject);
                $_realObject->fillWithDbTableRowsData($_arrayObject, $_object->{$_pkId}, Top::getCurrentLanguage());
                $_realObject->setData($_arrayObject);
                array_push($_retrievedObjects, $_realObject);
            }
            $this->_cannotSave = true;
        } catch (Exception $e) {
            $_retrievedObjects = array();
        }
        if ($_order !== NULL && is_array($_order)) {
            $this->__order = $_order;
            usort($_retrievedObjects, array("Thinkopen_Verticalabstractmodel", "_sortArrayForOrder"));
        }
        $_adapter = new Zend_Paginator_Adapter_Array($_retrievedObjects);
        $_paginator = new Zend_Paginator($_adapter);
        $this->totalResultPaginatedCollection = count($_retrievedObjects);
        return $_paginator->setItemCountPerPage($_itemsPerPage)->setCurrentPageNumber($_pageNumber);
    }

    private function _cleanNotNullableAttributes($_attributes) {
        $_metadata = $this->info("metadata");
        foreach ($_attributes as $_key => $_value) {
            if ($_metadata[$_key]['NULLABLE'] === false && $_value === NULL) {
                unset($_attributes[$_key]);
            }
        }
        return $_attributes;
    }

    private function _sortArrayForOrder($a, $b) {
        $_sortKey = "get" . ucfirst($this->__order[0]);
        $_attributeA = $a->{$_sortKey}();
        $_attributeB = $b->{$_sortKey}();
        $_attributeType = NULL;
        $_attributeKey = NULL;
        $this->_checkAttributesForJoinsAndOrder($this->__order[0], $_attributeType, $_attributeKey);
        if ($_attributeType == "date" || stripos($this->__order[0], "data") !== FALSE) {
            $_dateAttributeA = explode("/", $_attributeA);
            $_dateAttributeB = explode("/", $_attributeB);
            $_attributeA = $_dateAttributeA[2] . "-" . $_dateAttributeA[1] . "-" . $_dateAttributeA[0];
            $_attributeB = $_dateAttributeB[2] . "-" . $_dateAttributeB[1] . "-" . $_dateAttributeB[0];
        }
        if ($_attributeA > $_attributeB) {
            return ($this->__order[1] == "DESC") ? -1 : 1;
        } else {
            return ($this->__order[1] == "DESC") ? 1 : -1;
        }
    }

    private function _convertAbstractDbTablesToReal($_pk) {
        foreach ($this->_staticAttributes as $_attribute) {
            $_reference = $this->_attributes[$_attribute->attribute_name]->retrieveColsForReference();
            $_referenceCols = "set" . ucFirst($_reference[0]);
            $this->_attributes[$_attribute->attribute_name]->{$_referenceCols}($_pk);
        }
    }

    /**
     * Inserisce l'entita` all'interno del database
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function save() {
        if (!$this->_cannotSave) {
            $_dataIntoEntity = $this->_cleanNotNullableAttributes(array_diff_key($this->_attributes, array_flip($this->_getAttributes())));
            $_primaryKey = "get" . $this->_primaryKey;
            $_primaryKeySet = "set" . $this->_primaryKey;
            if ($this->{$_primaryKey}() === NULL || $this->{$_primaryKey}() == 0) {
                $_pkInserted = $this->insert($_dataIntoEntity);
                $this->{$_primaryKeySet}($_pkInserted);
                $this->_convertAbstractDbTablesToReal($_pkInserted);
            } else {
                $this->update($_dataIntoEntity, strtolower($this->_primaryKey) . " = " . $this->{$_primaryKey}());
            }
            foreach (array_diff_key($this->_attributes, $_dataIntoEntity) as $_blockToSave) {
                if (is_a($_blockToSave, "Zend_Db_Table_Row_Abstract") || (is_a($_blockToSave, "Thinkopen_Abstractmodel") && $_blockToSave->getValue() !== NULL)) {
                    $_blockToSave->save();
                }
            }
            $this->_setData($this->_attributes);
            return $this;
        } else {
            throw new Exception("Non puoi salvare avendo caricato l'oggetto con il metodo loadByAttribute()", 500);
        }
    }

    public function delete($_where = NULL) {
        if (!$this->_cannotSave) {
            try {
                $_primaryKey = "get" . $this->_primaryKey;
                parent::delete(strtolower($this->_primaryKey) . " = " . $this->{$_primaryKey}());
                return true;
            } catch (Exception $e) {
                return false;
            }
        } else {
            throw new Exception("Non puoi salvare avendo caricato l'oggetto con il metodo loadByAttribute()", 500);
        }
    }

}
