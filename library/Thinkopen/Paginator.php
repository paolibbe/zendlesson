<?php
/**
 * Description of Paginator
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Thinkopen_Paginator extends Zend_Paginator {

    public $objectType = null;
    
    public function __construct($adapter, $_class) {
        parent::__construct($adapter);
        $this->objectType = $_class;
    }
    
}
