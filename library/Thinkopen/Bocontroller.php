<?php
require_once '../library/Thinkopen/Cacher.php';
require_once '../library/Thinkopen/Pagecacher.php';
define("BO_ADMINISTRATOR", 1);
define("BO_ADMINLESS", 2);
define("BO_USER", 3);
define("NONE", 256);

/**
 * Description of ControllerAction
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Thinkopen_Bocontroller extends Zend_Controller_Action {

    protected $_openAction = array();
    private $_cleanActions = array("SAVEFROMFE", "SAVE", "SAVEBLOCK", "SAVEBLOCKSHOULDER", "DELETE", "ARCHIVE");
    protected $_modulesControllerAction = array();

    private function parseDocComment($str, $tag = '') {
        if (empty($tag)) {
            return false;
        }
        $matches = array();
        preg_match("/" . $tag . ":(.*)(\\r\\n|\\r|\\n)/U", $str, $matches);
        if (isset($matches[1])) {
            return trim($matches[1]);
        }

        return false;
    }

    private function _parseSingleMethodForAnnotations($_methods, &$acl, $controller) {
        if (strpos($_methods->name, "Action") !== FALSE) {
            $reflectionMethods = new Zend_Reflection_Method($_methods->class, $_methods->name);
            $_docComments = $this->parseDocComment($reflectionMethods->getDocComment(), "@acl");
            if ($_docComments) {
                $acl[$controller][str_replace("Action", "", $_methods->name)] = ((is_null(constant($_docComments))) ? $acl[$controller]['acl_controller'] : constant($_docComments));
            } else {
                $acl[$controller][str_replace("Action", "", $_methods->name)] = $acl[$controller]['acl_controller'];
            }
        }
    }

    private function _parseSingleClassForAnnotations(&$acl, &$_passedClasses) {
        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, 'Thinkopen_Bocontroller') && !in_array($class, $_passedClasses)) {
                $annotations = new ReflectionClass($class);
                $controller = strtolower(substr($class, 0, strpos($class, "Controller")));
                $_controllerAcl = $this->parseDocComment($annotations->getDocComment(), "@acl");
                $acl[$controller]['acl_controller'] = ($_controllerAcl) ? (is_null(constant($_controllerAcl))) ? BO_USER : constant($_controllerAcl) : BO_USER;
                foreach ($annotations->getMethods() as $_methods) {
                    $this->_parseSingleMethodForAnnotations($_methods, $acl, $controller);
                }
                array_push($_passedClasses, $class);
            }
        }
    }

    private function _getAclFromControllers() {
        $front = $this->getFrontController();
        $acl = array();
        $_passedClasses = array();
        foreach ($front->getControllerDirectory() as $module => $path) {
            if ($module == "bo") {
                foreach (scandir($path) as $file) {
                    if (strstr($file, "Controller.php") !== false) {
                        include_once $path . DIRECTORY_SEPARATOR . $file;
                        $this->_parseSingleClassForAnnotations($acl, $_passedClasses);
                    }
                }
            }
        }
        $this->_modulesControllerAction = $acl;
    }

    public function __construct(\Zend_Controller_Request_Abstract $request, \Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {
        parent::__construct($request, $response, $invokeArgs);
        $this->_getAclFromControllers();
        $this->_openAction = array(
            "login/login",
            "login/index",
            "login/logout",
            "login/dologin",
            "index/cleancache",
            "user/register"
        );
    }

    private function _getAclForUser($_userId) {
        $_userBoModel = Top::getSingleton("bo_mapper/user");
        if ($_userId === false) {
            return NONE;
        } else {
            $_userBo = $_userBoModel->load($_userId);
            return $_userBo->getRole();
        }
    }

    private function _checkAclForUser($_userRole) {
        $_controllerName = $this->getRequest()->getControllerName();
        $_actionName = $this->getRequest()->getActionName();
        if (in_array(strtolower($_controllerName . "/" . $_actionName), $this->_openAction)) {
            return true;
        } elseif ($this->_modulesControllerAction["bo_" . $_controllerName][$_actionName] >= $_userRole) {
            return true;
        } else {
            if ($_userRole === NONE) {
                return -1;
            } else {
                return false;
            }
        }
    }

    public function preDispatch() {
        $_userBoModel = Top::getSingleton("bo_mapper/user");
        $_userRole = $this->_getAclForUser($_userBoModel::getUserId());
        $_checkAcl = $this->_checkAclForUser($_userRole);
        if ($_checkAcl === true) {
            parent::preDispatch();
        } elseif ($_checkAcl === -1) {
            $this->redirect(Top::getBaseUrl(true) . "login/index", array("code" => 303));
        } elseif ($_checkAcl === FALSE) {
            $this->redirect(Top::getBaseUrl(true) . "index/index", array("code" => 303));
        }
    }

    public function dispatch($action) {
        if (in_array(strtoupper($this->getFrontController()->getRequest()->getActionName()), $this->_cleanActions)) {
            Thinkopen_Cacher::cleanAll(FALSE, FALSE);
        }
        parent::dispatch($action);
    }

}
