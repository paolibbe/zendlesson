<?php

/**
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @date: 20/03/13
 */
class Thinkopen_Resizeimage {

    private static $instance = NULL;
    private $image;
    private $height;
    private $width;
    private $image_type;
    private $pathImages;
    private $pathThumbs;
    private $imageIsBroken = false;
    private $_cacher = NULL;
    private $_filename = NULL;
    private $_savePath = NULL;

    /**
     * Return true if image is broken or unreachable
     * @return boolean
     */
    function getImageStatus() {
        return $this->imageIsBroken;
    }

    /**
     * Instantiate class object
     */
    protected function __construct() {
        $this->pathImages = BASE_URL;
        $this->pathThumbs = BASE_URL . "thumbnails/";
        $this->_cacher = Thinkopen_Cacher::getInstance("thumbnails", 86400);
        $this->_savePath = APPLICATION_PATH . "/../public/thumbnails/";
    }

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new Thinkopen_Resizeimage();
        }
        return self::$instance;
    }

    /**
     * Load the requested image in the object
     * @param string $filename
     */
    function load($filename) {
        $this->_filename = $filename;
        $filename = $this->pathImages . $filename;
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if ($this->image_type == IMAGETYPE_JPEG) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->image_type == IMAGETYPE_GIF) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->image_type == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
        } else {
            $this->imageIsBroken = true;
            return false;
        }
    }

    private function _getWeH($width = NULL, $height = NULL, $forcesize = false) {
        if ($height === NULL) {
            $height = ($width / (int) $this->_getWidth()) * $this->_getHeight();
            $height = (int) $height;
        }
        /* optional. if file is smaller, do not resize. */
        if ($forcesize === false) {
            if ($width > $this->_getWidth() && $height > $this->_getHeight()) {
                $width = $this->_getWidth();
                $height = $this->_getHeight();
            }
        }
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Save the resized image into the server
     * @param string $filename
     * @param string $image_type
     * @param int $compression
     * @param string $permissions
     */
    public function saveImage($_width = NULL, $_height = NULL, $_forceweh = false) {
        $image_type = $this->image_type;
        $this->_getWeH($_width, $_height, $_forceweh);
        $_baseImageName = preg_replace("/[^a-zA-Z0-9_]+/", "", str_replace("files/", "", $this->_filename)) . "_" . $this->width . "x" . $this->height;
        if ($_width !== NULL) {
            $this->_resize();
        }
        switch ($image_type) {
            case IMAGETYPE_JPEG:
                if (!file_exists($this->_savePath . $_baseImageName . ".jpg")) {
                    imagejpeg($this->image, $this->_savePath . $_baseImageName . ".jpg");
                }
                return $this->pathThumbs . $_baseImageName . ".jpg";
            case IMAGETYPE_GIF:
                if (!file_exists($this->_savePath . $_baseImageName . ".gif")) {
                    imagegif($this->image, $this->_savePath . $_baseImageName . ".gif");
                }
                return $this->pathThumbs . $_baseImageName . ".gif";
            case IMAGETYPE_PNG:
                if (!file_exists($this->_savePath . $_baseImageName . ".png")) {
                    imagealphablending($this->image, false);
                    imagesavealpha($this->image, true);
                    imagepng($this->image, $this->_savePath . $_baseImageName . ".png");
                }
                return $this->pathThumbs . $_baseImageName . ".png";
            default:
                break;
        }
    }

    /**
     * Output image to browser
     * @param int $_width
     * @param int $_height
     * @param boolean $_forceWeH
     */
    public function output($_width = NULL, $_height = NULL, $_forceWeH = false) {
        $this->_getWeH($_width, $_height, $_forceWeH);
        if (!$base64Image = $this->_cacher->load(preg_replace("/[^a-zA-Z0-9_]+/", "", str_replace("files/", "", $this->_filename)) . "_" . $this->width . "x" . $this->height)) {
            $image_type = $this->image_type;
            if ($_width !== NULL) {
                $this->_resize();
            }
            ob_start();
            switch ($image_type) {
                case IMAGETYPE_JPEG:
                    imagejpeg($this->image, "php://output");
                    break;
                case IMAGETYPE_GIF:
                    imagegif($this->image, "php://output");
                    break;
                case IMAGETYPE_PNG:
                    imagealphablending($this->image, false);
                    imagesavealpha($this->image, true);
                    imagepng($this->image, "php://output");
                    break;
                default:
                    break;
            }
            $base64Image = ob_get_contents();
            ob_end_clean();
            $this->_cacher->save($base64Image, preg_replace("/[^a-zA-Z0-9_]+/", "", str_replace("files/", "", $this->_filename)) . "_" . $this->width . "x" . $this->height);
        }
        return base64_encode($base64Image);
    }

    /**
     * Get current image's width
     * @return int
     */
    function _getWidth() {
        return imagesx($this->image);
    }

    /**
     * Get current image's height
     * @return int
     */
    private function _getHeight() {
        return imagesy($this->image);
    }

    /**
     * Resize the current image, scaling on a fixed height's value
     * @param int $height
     */
    private function _resizeToHeight($height) {

        $ratio = $height / $this->_getHeight();
        $width = $this->_getWidth() * $ratio;
        $this->resize($width, $height);
    }

    /**
     * Resize the current image, scaling on a fixed width's value
     * @param int $width
     */
    private function _resizeToWidth($width) {
        $ratio = $width / $this->_getWidth();
        $height = $this->_getHeight() * $ratio;
        $this->resize($width, $height);
    }

    /**
     * Resize the current image, scaling on the parameter
     * @param int $scale
     */
    private function _scale($scale) {
        $width = $this->_getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->_resize($width, $height);
    }

    /**
     * Resize the current image, to the width and heigth passed
     * @param int $width
     * @param int $height
     */
    /* function resize($width, $height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->_getWidth(), $this->_getHeight());
      $this->image = $new_image;
      } */

    private function _resize() {
        if ($this->image_type == IMAGETYPE_GIF) {
            $new_image = imagecreate($this->width, $this->height);
        } else {
            $new_image = imagecreatetruecolor($this->width, $this->height);
        }
        /* Check if this image is PNG or GIF, then set if Transparent */
        if (($this->image_type == IMAGETYPE_GIF) || ($this->image_type == IMAGETYPE_PNG)) {
            imagealphablending($new_image, false);
            imagesavealpha($new_image, true);
            $transparent = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
            imagefilledrectangle($new_image, 0, 0, $this->width, $this->height, $transparent);
        }
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $this->width, $this->height, $this->_getWidth(), $this->_getHeight());
        $this->image = $new_image;
    }

}
