<?php

/**
 * Description of Abstractmodel
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
abstract class Thinkopen_Abstractmodel extends Zend_Db_Table_Abstract {

    protected $_primaryKey;
    protected static $_instance;
    protected $_tableName;
    protected $calledClass;
    protected $_attributes = array();

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function __construct($_data = array()) {
        $this->_primaryKey = ucfirst($this->retrievePrimaryKey());
        $this->_tableName = $this->retrieveTableName();
        $this->provideInformationsForRelatedTables();
        $config = array('name' => $this->_tableName);
        $this->calledClass = get_called_class();
        parent::__construct($config);
        if (count($_data) != 0) {
            $this->_setData($_data);
        } else {
            $this->_setData(array_flip($this->_getCols()), true);
        }
        self::$_instance = $this;
        return $this;
    }

    public function setData($_data) {
        if (count($_data) != 0) {
            $this->_setData($_data);
        } else {
            return false;
        }
        return $this;
    }

    public function getAttributes() {
        return $this->_getCols();
    }

    public function countRows() {
        $_count = $this->getAdapter()->fetchOne('SELECT COUNT(*) AS count FROM ' . $this->_tableName);
        return $_count;
    }

    protected function provideInformationsForRelatedTables() {
        $_referencedClass = $this->retrieveTableReference();
        $_referencedColumns = $this->retrieveColsForReference();
        if ($_referencedClass != "") {
            $this->addReference($_referencedClass, array($_referencedColumns[0]), $_referencedClass, array($_referencedColumns[1]));
        }
    }

    protected function _setData($_data, $_init = false) {
        foreach ($_data as $_key => $_value) {
            $this->_attributes[$_key] = ($_init) ? NULL : $_value;
        }
    }

    public static function getInstance() {
        if (self::$_instance === NULL || self::$_instance->calledClass !== get_called_class()) {
            $_className = get_called_class();
            self::$_instance = new $_className();
        }
        return self::$_instance;
    }

    private function parseDocComment($str, $tag = '') {
        if (empty($tag)) {
            return $str;
        }
        $matches = array();
        preg_match("/" . $tag . ":(.*)(\\r\\n|\\r|\\n)/U", $str, $matches);
        if (isset($matches[1])) {
            return trim($matches[1]);
        }

        return '';
    }

    private function retrievePrimaryKey() {
        $annotations = new ReflectionClass($this);
        return $this->parseDocComment($annotations->getDocComment(), "@PrimaryKey");
    }

    private function retrieveTableReference() {
        $annotations = new ReflectionClass($this);
        return $this->parseDocComment($annotations->getDocComment(), "@ClassReferenced");
    }

    public function retrieveColsForReference() {
        $annotations = new ReflectionClass($this);
        $_columnsReferenced = $this->parseDocComment($annotations->getDocComment(), "@ColsReferenced");
        return explode("->", $_columnsReferenced);
    }

    private function retrieveTableName() {
        $annotations = new ReflectionClass($this);
        $configs = Thinkopen_DbInitialize::getInstance();
        $_tableSuffix = $configs::$iniConfig->database->table->suffix;
        $_tablePrefix = $configs::$iniConfig->database->table->prefix;
        $_tableAnnotatedName = $this->parseDocComment($annotations->getDocComment(), "@Table");
        return (stristr($_tableAnnotatedName, $_tableSuffix)) ? $_tableAnnotatedName : $_tablePrefix . $_tableAnnotatedName . $_tableSuffix;
    }

    public function __call($methodName, $params = null) {
        $methodPrefix = substr($methodName, 0, 3);
        $key = strtolower(substr($methodName, 3));
        $_isUppercase = ctype_upper(substr($methodName, 3, 1));
        if ($methodPrefix == 'set' && count($params) == 1 && $_isUppercase) {
            if (!in_array($key, $this->_getCols())) {
                return $this;
            } else {
                $value = $params[0];
                $this->_attributes[$key] = $value;
                return $this;
            }
        } elseif ($methodPrefix == 'get') {
            if (in_array($key, array_keys($this->_attributes))) {
                return $this->_attributes[$key];
            } else {
                return;
            }
        } elseif (!in_array($methodName, array_flip(get_class_methods($this)))) {
            throw new Exception("Method \"" . $methodName . "\" doesn't exist in " . get_called_class(), 500);
        }
    }

    public function load($_id) {
        try {
            $_objectData = $this->select()->where(strtolower($this->_primaryKey) . " = ?", $_id)->query()->fetch();
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            } else {
                $object = get_called_class();
                return new $object($_objectData);
            }
        } catch (Exception $e) {
            $_logger = Thinkopen_Logger::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage()." (PK:: {$_id})");
            return false;
        }
    }

    public function getCollection($_filters) {
        foreach (array_keys($_filters) as $_attributeSearched) {
            if (!in_array($_attributeSearched, $this->_getAttributes()) && !in_array($_attributeSearched, $this->_getCols())) {
                throw new Exception("Attribute \"" . $_attributeSearched . "\" is not part of object's attribute set", 500, NULL);
            }
        }
        try {
            $_primaryKey = $this->getPrimaryKey();
            $_objectData = $this->select()->from($this->_tableName)->columns($_primaryKey);
            foreach ($_filters as $_attributeName => $_value) {
                $_operand = $_value[0];
                $_matchingValue = $_value[1];
                $_objectData->where($_attributeName . " {$_operand} ?", $_matchingValue);
            }
            $_objectData = $_objectData->query()->fetchAll();
            if (!$_objectData && count($_objectData) == 0) {
                throw new Exception("Object not found", 501);
            } else {
                $_toReturn = array();
                $objectName = get_called_class();
                foreach ($_objectData as $_singleObject) {
                    $_idPk = $_singleObject[$_primaryKey];
                    $object = new $objectName();
                    $element = $object->loadByAttribute(strtolower($_primaryKey), $_idPk);
                    array_push($_toReturn, $element);
                }
                return $_toReturn;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getData() {
        return $this->_attributes;
    }

    public function loadByAttribute($_attributeName, $_value = NULL) {
        if (!in_array($_attributeName, $this->_getCols()) && !is_array($_attributeName)) {
            throw new Exception("Attribute \"" . $_attributeName . "\" is not part of object's attribute set", 500, NULL);
        }
        try {
            if (!is_array($_attributeName)) {
                $_attributes = array($_attributeName => $_value);
            } else {
                $_attributes = $_attributeName;
            }
            $_objectData = $this->select();
            foreach ($_attributes as $_key => $_value) {
                $_objectData->where($_key . " = ?", $_value);
            }
            $_objectData = $_objectData->query()->fetch();
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            } else {
                $this->_setData($_objectData);
                return $this;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private function _cleanNotNullableAttributes($_attributes) {
        $_metadata = $this->info("metadata");
        foreach ($_attributes as $_key => $_value) {
            if ($_metadata[$_key]['NULLABLE'] === false && $_value === NULL) {
                unset($_attributes[$_key]);
            }
        }
        return $_attributes;
    }

    public function getPrimaryKey() {
        return $this->_primaryKey;
    }

    /**
     * Inserisce l'entita` all'interno del database
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function save() {
        $_data = $this->_cleanNotNullableAttributes($this->_attributes);
        $_primaryKey = "get" . $this->_primaryKey;
        $_primaryKeySet = "set" . $this->_primaryKey;
        if ($this->{$_primaryKey}() === NULL || $this->{$_primaryKey}() == 0) {
            $this->{$_primaryKeySet}($this->insert($_data));
        } else {
            $this->update($_data, strtolower($this->_primaryKey) . " = " . $this->{$_primaryKey}());
        }
        $this->_setData($this->_attributes);
        return $this;
    }

    public function delete($where = NULL) {
        try {
            $_primaryKey = "get" . $this->_primaryKey;
            parent::delete(strtolower($this->_primaryKey) . " = " . $this->{$_primaryKey}());
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

}
