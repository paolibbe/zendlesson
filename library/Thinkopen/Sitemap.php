<?php

/**
 * Description of Sitemap creator
 *
 * @author Walter Fato <walter.fato@thinkopen.it>
 */
class Thinkopen_Sitemap {
    
    protected  $sitemapPath;

    public function __construct() {
        $this->sitemapPath = APPLICATION_PATH . "/../public/sitemap.xml";
    }
    
    public function getLastUpdate(){
        return date("d-m-Y",filemtime($this->sitemapPath));
    }
    
    public function getSitemapPath(){
        return $this->sitemapPath;
    }
    
    public function getSitemapXml(){
        return file_get_contents($this->sitemapPath);
    }

    public function createSitemap() {
        $arrayCompletoPages = Top::getModel("bo_mapper/page")->getPagesForSitemap("it");
        $arrayCompletoNews = Top::getModel("bo_mapper/news")->getNewsForSitemap("it");
        $arrayCompletoShows = Top::getModel("mapper/show")->getCollection();
        $arrayCompletoSeason = Top::getModel("bo_mapper/programm")->getSeasonsForSitemap();

        $_out = fopen($this->sitemapPath, "w");
        fwrite($_out, '<?xml version="1.0" encoding="UTF-8"?>
');
        fwrite($_out, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
');
        $this->printHomeXml($_out);
        $this->printSeasonXml($_out, $arrayCompletoSeason);
        $this->printShowXml($_out, $arrayCompletoShows);
        $this->printPNXml($_out, $arrayCompletoPages, "pages");
        $this->printPNXml($_out, $arrayCompletoNews, "news");
        fwrite($_out, '</urlset>
');
    }

    private function randPagePrioprity() {
        $rand = rand(1, 99);
        return "0." . $rand;
    }

    private function printPNXml($_out, $arraycompletoFull, $modulo) {
        foreach ($arraycompletoFull as $p) {
            foreach ($p as $miniarray) {
                fwrite($_out, '     <url>
');
                $i = 0;
                foreach ($miniarray as $obj) {
                    if ($i == 0) {
                        fwrite($_out, '         <loc>' . BASE_URL . $obj->language . '/' . $modulo . '/' . $obj->url . '.html</loc>
');
                    fwrite($_out, '         <lastmod>' . date("Y-m-d", strtotime($obj->update)) . '</lastmod>
');
                    fwrite($_out, '         <changefreq>' . (($modulo == "pages") ? 'weekly' : 'daily' ) . '</changefreq>
');
                    fwrite($_out, '         <priority>' . $this->randPagePrioprity() . '</priority>
');
                    }
                    fwrite($_out, '         <xhtml:link 
');
                    fwrite($_out, '                     rel="alternate" 
');
                    fwrite($_out, '                     hreflang="' . $obj->language . '" 
');
                    fwrite($_out, '                     href="' . BASE_URL . $obj->language . '/' . $modulo . '/' . $obj->url . '.html" 
');
                    fwrite($_out, '                     />
');
                    $i++;
                }
                fwrite($_out, '     </url>
');
            }
        }
    }

    private function printShowXml($_out, $arraycompletoFull) {
        foreach ($arraycompletoFull as $p) {
            foreach (Top::getLanguages() as $lingua) {
                fwrite($_out, '     <url>
');
                $i = 0;
                foreach (Top::getLanguages() as $linguaDentro) {
                    if ($i == 0) {
                        fwrite($_out, '         <loc>' . BASE_URL . $lingua . '/spettacoli/' . $p->getIdentifier() . '.html</loc>
');
                    fwrite($_out, '         <lastmod>' . date("Y-m-d", strtotime($p->getUpdated_at())) . '</lastmod>
');
                    fwrite($_out, '         <changefreq>weekly</changefreq>
');
                    fwrite($_out, '         <priority>' . $this->randPagePrioprity() . '</priority>
');
                    }
                    fwrite($_out, '         <xhtml:link 
');
                    fwrite($_out, '                     rel="alternate" 
');
                    fwrite($_out, '                     hreflang="' . $linguaDentro . '" 
');
                    fwrite($_out, '                     href="' . BASE_URL . $linguaDentro . '/spettacoli/' . $p->getIdentifier() . '.html" 
');
                    fwrite($_out, '                     />
');
                    $i++;
                }
                fwrite($_out, '     </url>
');
            }
        }
    }

    private function printSeasonXml($_out, $arraycompletoFull) {
        foreach ($arraycompletoFull as $p) {
            foreach (Top::getLanguages() as $lingua) {
                fwrite($_out, '     <url>
');
                $i = 0;
                foreach (Top::getLanguages() as $linguaDentro) {
                    if ($i == 0) {
                        fwrite($_out, '         <loc>' . BASE_URL . $lingua . '/season/' . $p['identifier'] . '.html</loc>
');
                    fwrite($_out, '         <lastmod>' . date("Y-m-d", strtotime($p['updated_at'])) . '</lastmod>
');
                    fwrite($_out, '         <changefreq>weekly</changefreq>
');
                    fwrite($_out, '         <priority>' . $this->randPagePrioprity() . '</priority>
');
                    }
                    fwrite($_out, '         <xhtml:link 
');
                    fwrite($_out, '                     rel="alternate" 
');
                    fwrite($_out, '                     hreflang="' . $linguaDentro . '" 
');
                    fwrite($_out, '                     href="' . BASE_URL . $linguaDentro . '/season/' . $p['identifier'] . '.html" 
');
                    fwrite($_out, '                     />
');
                    $i++;
                }
                fwrite($_out, '     </url>
');
            }
        }
    }

    private function printHomeXml($_out) {

        $lingue = array();
        foreach (Top::getLanguages() as $language) {
            $obj = new stdClass();
            $obj->url = BASE_URL;
            $obj->language = $language;
            $obj->update = date("Y-m-d");
            array_push($lingue, $obj);
        }
        $array = array($lingue);
        for ($i = 0; $i < (count($lingue) - 1); $i++) {
            $out = array_shift($lingue);
            array_push($lingue, $out);
            array_push($array, $lingue);
        }

        foreach ($array as $miniarray) {
            fwrite($_out, '     <url>
');
            $i = 0;
            foreach ($miniarray as $obj) {
                if ($i == 0) {
                    fwrite($_out, '         <loc>' . $obj->url . $obj->language . '/</loc>
');
                fwrite($_out, '         <lastmod>' . date("Y-m-d", strtotime($obj->update)) . '</lastmod>
');
                fwrite($_out, '         <changefreq>weekly</changefreq>
');
                fwrite($_out, '         <priority>1</priority>
');
                }
                fwrite($_out, '         <xhtml:link 
');
                fwrite($_out, '                     rel="alternate" 
');
                fwrite($_out, '                     hreflang="' . $obj->language . '" 
');
                fwrite($_out, '                     href="' . $obj->url . $obj->language . '/" 
');
                fwrite($_out, '                     />
');
                $i++;
            }
            fwrite($_out, '     </url>
');
        }
    }

}
