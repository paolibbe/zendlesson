<?php

set_error_handler('eludeStrictErrors', E_STRICT);

function eludeStrictErrors($errno, $errstr) {
    
}

/**
 * Description of Logger
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Thinkopen_Logger extends Zend_Log {

    /**
     * @var \Zend_Log 
     */
    private static $instance;
    private static $date;
    private $_logMinLevel = NULL;

    public function __construct() {
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . "/../var/log/" . date("Y-m-d") . ".log");
        parent::__construct($writer);
        $_configs = Thinkopen_DbInitialize::getInstance();
        $this->_logMinLevel = $_configs::$iniConfig->logger->minimumlevel;
        return $this;
    }

    /**
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return \Thinkopen_Logger
     */
    public static function getInstance() {
        if (self::$instance === NULL || self::$date != date("Y-m-d")) {
            self::$instance = new Thinkopen_Logger();
        }
        return self::$instance;
    }

    public function log($_namespace, $_level, $_message) {
        if (is_array($_message) || is_object($_message)) {
            $_message = json_encode($_message);
        }
        if ($_level <= $this->_logMinLevel) {
            parent::log(strtoupper($_namespace) . "\t" . $_message, $_level);
        }
    }

}
