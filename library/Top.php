<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Top
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Top {

    public static function getModel($_model) {
        $_className = self::_getClassName($_model);
        return new $_className();
    }

    private static function _getClassName($_model) {
        $_modelExploded = explode("/", $_model);
        if (count($_modelExploded) == 1) {
            $_className = "Default_Model_" . ucfirst($_modelExploded[0]);
        } elseif (count($_modelExploded) == 2) {
            $_hasModule = explode("_", $_modelExploded[0]);
            if (count($_hasModule) == 1) {
                $_className = "Default_Model_" . ucfirst($_modelExploded[0]) . "_" . ucfirst($_modelExploded[1]);
            } else {
                $_className = ucfirst($_hasModule[0]) . "_Model_" . ucfirst($_hasModule[1]) . "_" . ucfirst($_modelExploded[1]);
            }
        } elseif (count($_modelExploded) == 3) {
            $_hasModule = explode("_", $_modelExploded[0]);
            if (count($_hasModule) == 1) {
                $_className = "Default_Model_" . ucfirst($_modelExploded[0]) . "_" . ucfirst($_modelExploded[1]) . "_" . ucfirst($_modelExploded[2]);
            } else {
                $_className = ucfirst($_hasModule[0]) . "_Model_" . ucfirst($_hasModule[1]) . "_" . ucfirst($_modelExploded[1]) . "_" . ucfirst($_modelExploded[2]);
            }
        }
        return $_className;
    }

    public static function getSingleton($_model) {
        $_className = self::_getClassName($_model);
        return $_className::getInstance();
    }

    public static function getBaseUrl($_admin = false) {
        $configs = Thinkopen_DbInitialize::getInstance();
        $_websiteAddress = $configs::$iniConfig->website->address;
        return ($_admin) ? $_websiteAddress->admin : $_websiteAddress->frontend;
    }

    public static function getLanguages($_description = false) {
        $configs = Thinkopen_DbInitialize::getInstance();
        return (!$_description) ? $configs::$iniConfig->website->languages->toArray() : array_combine($configs::$iniConfig->website->languages->toArray(), $configs::$iniConfig->website->languagesLabel->toArray());
    }

    public static function getCurrentLanguage() {
        $_frontController = Zend_Controller_Front::getInstance();
        $_lang = $_frontController->getRequest()->getParam("lang", "it");
        if (!in_array($_lang, self::getLanguages())) {
            $_lang = "it";
        }
        return $_lang;
    }

}
