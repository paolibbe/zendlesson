<?php

require_once '../library/Thinkopen/DbInitialize.php';
require_once '../library/Thinkopen/Pagecacher.php';

/**
 * Description of Bootstrap
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    private $_iniConfig;

    private function _translatorInit() {
        $cache = Thinkopen_Cacher::getInstance("traduzioni", 864000);
        $adapter = new Zend_Translate(
                array(
            'adapter' => 'csv',
            'locale' => 'it'
                )
        );
        foreach (Top::getLanguages() as $_language) {
            if ($_language !== "it") {
                if (file_exists("../languages/{$_language}.csv")) {
                    $adapter->addTranslation(array(
                        'content' => "../languages/{$_language}.csv",
                        'locale' => "{$_language}"
                    ));
                }
            }
        }
        $adapter->setCache($cache);
        Zend_Registry::set('Zend_Translate', $adapter);
    }

    public function _bootstrap($resource = null) {
        parent::_bootstrap($resource);
        $iniConfig = Thinkopen_DbInitialize::getInstance();
        $this->_iniConfig = $iniConfig::$iniConfig;
        $_errorLevel = $this->_iniConfig->php->error_reporting;
        error_reporting(eval("return $_errorLevel;"));
        Zend_Controller_Front::getInstance()->setBaseUrl($this->_iniConfig->website->address->frontend, $this->_iniConfig->website->address->admin);
        defined('BASE_URL') || define('BASE_URL', $this->_iniConfig->website->address->frontend);
        $this->_translatorInit();
//        $frontController->setParam("logged", $this->_getLoggedUser());
    }

    protected function _initAutoload() {
        $_iniConfig = Thinkopen_DbInitialize::getInstance();
        $this->_iniConfig = $_iniConfig::$iniConfig;
        if ($_iniConfig::$iniConfig->website->fullpagecache == "ENABLED") {
            $this->usePageCache($_iniConfig);
        }
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);
        $autoloader->suppressNotFoundWarnings(false);
        return $autoloader;
    }

    protected function _initView() {
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');
        // setting the controller and action name as title segments:
        $view->headTitle('Thinkopen Zend Lessons');
        $view->headTitle()->setSeparator(' - ');
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
                        'ViewRenderer'
        );
        $viewRenderer->setView($view);
        return $view;
    }

    public function _initRoutes() {
        $this->bootstrap('FrontController');
        $this->_frontController = $this->getResource('FrontController');
        $router = $this->_frontController->getRouter();
        $langRoute = new Zend_Controller_Router_Route(
                ':lang/', array(
            'lang' => 'it'
                )
        );
        $contactRoute = new Zend_Controller_Router_Route_Static(
                'contact', array('controller' => 'index', 'action' => 'index')
        );
        $defaultRoute = new Zend_Controller_Router_Route(
                ':controller/:action/*', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'index'
                )
        );
        $adminRoute = new Zend_Controller_Router_Route(
                'admin/:controller/:action/*', array(
            'module' => 'bo',
            'controller' => 'index',
            'action' => 'index'
                )
        );

        $pageRoute = new Zend_Controller_Router_Route_Regex(
            'productdetail/([a-zA-Z-_0-9]+).html', array(
            'action' => 'detail',
            'controller' => 'product',
            'module' => 'default'
        ), array(
            1 => 'product_url'
        ), 'productdetail/%s.html');
        $newsRoute = new Zend_Controller_Router_Route_Regex(
                'news/([a-zA-Z-_0-9]+).html', array(
            'action' => 'index',
            'controller' => 'news',
            'module' => 'default'
                ), array(
            1 => 'newsurl'
                ), 'news/%s.html');
        $spettacoliRoute = new Zend_Controller_Router_Route_Regex(
                'spettacoli/([a-zA-Z-_0-9]+).html', array(
            'action' => 'index',
            'controller' => 'spettacoli',
            'module' => 'default'
                ), array(
            1 => 'pageurl'
                ), 'spettacoli/%s.html');
        $seasonRoute = new Zend_Controller_Router_Route_Regex(
                'season/([a-zA-Z-_0-9]+).html', array(
            'action' => 'index',
            'controller' => 'season',
            'module' => 'default'
                ), array(
            1 => 'pageurl'
                ), 'season/%s.html');
        $pageRoute = $langRoute->chain($pageRoute);
        $newsRoute = $langRoute->chain($newsRoute);
        $spettacoliRoute = $langRoute->chain($spettacoliRoute);
        $seasonRoute = $langRoute->chain($seasonRoute);
        $contactRoute = $langRoute->chain($contactRoute);
        $defaultRoute = $langRoute->chain($defaultRoute);
        $router->addRoute('langRoute', $langRoute);
        $router->addRoute('defaultRoute', $defaultRoute);
        $router->addRoute('spettacoliRoute', $spettacoliRoute);
        $router->addRoute('seasonRoute', $seasonRoute);
        $router->addRoute('pagesRoute', $pageRoute);
        $router->addRoute('newsRoute', $newsRoute);
        $router->addRoute('contactRoute', $contactRoute);
        $router->addRoute('adminRoute', $adminRoute);
    }

    public function run() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout("layout/layout");
        parent::run();
    }

    public function usePageCache() {
        try {
            $cache = Thinkopen_Pagecacher::getInstance();
            $cache->start(false, false);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
