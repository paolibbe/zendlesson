<?php

/**
 * Description of User
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @PrimaryKey: user_id
 * @Table: t_user_entity
 */
class Default_Model_Mapper_User extends Thinkopen_Verticalabstractmodel {

    private $_headers = NULL;

    public function registerAndLoginStandard($_userDataJsonized) {
        $_userData = json_decode($_userDataJsonized);
        $_userAlreadyRegistered = $this->getCollection(array("email" => array("=", $_userData->email)));
        if (count($_userAlreadyRegistered) > 1) {
            throw new Exception("Esistono piu` utenti con la stessa email", 500, NULL);
        }
        if (!$_userAlreadyRegistered) {
            $this->setFirstname($_userData->first_name);
            $this->setLastname($_userData->last_name);
            $this->setEmail($_userData->email);
            $this->setGender(1);
            $this->setDob($_userData->birthdate);
            $this->setCountry($_userData->country->state);
            $this->setRegion($_userData->country->region);
            $_hasBytes = mb_convert_encoding($_userData->password, "UTF-16LE", mb_detect_encoding($_userData->password));
            $_psswdCrypted = sha1($_hasBytes, true);
            $_password = base64_encode($_psswdCrypted);
            $this->setPassword($_password);
            if ($_userData->newsletter) {
                $this->setNewsletter(($_userData->newsletter == "on") ? "1" : "0");
            }
            if ($_userData->publish) {
                $this->setPublish(($_userData->publish == "on") ? "1" : "0");
            }
            $this->setIs_facebook(0);
            $this->save();
            $this->_registerOnGeticket($_userData->email, $_userData->password, $_userData);
            if (!self::getUserId()) {
                $_mailer = new Thinkopen_Mailer();
                $_mailer->sendMail($_userData, Thinkopen_Mailer::REGISTER);
                return $this->_doLogin(true, $_userData->email, $_userData->password);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Configurazioni di sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    private static $_mySession = null;

    /**
     * Funzione di get per la sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return mixed
     */
    static public function getSession() {
        $_configs = Thinkopen_DbInitialize::getInstance();
        $_namespace = $_configs::$iniConfig->database->table->suffix;
        if (self::$_mySession === NULL) {
            self::$_mySession = new Zend_Session_Namespace('users' . $_namespace);
            if (!isset(self::$_mySession->initialized)) {
                Zend_Session::regenerateId();
                self::$_mySession->initialized = true;
            }
        }
        return self::$_mySession;
    }

    /**
     * Cancella la sessione dell'utente (logout)
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    static public function deleteSession() {
        self::getSession()->id = null;
        self::getSession()->geticketId = null;
        self::getSession()->geticketUserId = null;
        self::getSession()->phpSessid = null;
        Zend_Session::destroy();
        Zend_Session::regenerateId();
    }

    /**
     * Ritorna l'id dell'utente correntemente loggato se esiste,
     * altrimenti false
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @return int|boolean
     */
    public static function getUserId() {
        if (self::getSession()->id) {
            return self::getSession()->id;
        } else {
            return false;
        }
    }

    /**
     * Funzione privata per la login attraverso check nel database
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param string $_username
     * @param string $_password
     * @return boolean
     */
    private function executeLogin($_username, $_password) {
        $_hasBytes = mb_convert_encoding($_password, "UTF-16LE", mb_detect_encoding($_password));
        $_psswdCrypted = sha1($_hasBytes, true);
        $_passwordEnc = base64_encode($_psswdCrypted);
        $_collection = $this->getCollection(array("email" => array("=", $_username)));
        if ($_collection && count($_collection) == 1) {
            if ($_collection[0]->getPassword() == $_passwordEnc) {
                Thinkopen_Logger::getInstance()->log("login", Zend_Log::NOTICE, "USER:: " . $_username . " SUCCESSFULLY LOGIN ON " . date("Y-m-d H:i:s"));
                $this->load($_collection[0]->getUser_id());
                return $this;
            } else {
                Thinkopen_Logger::getInstance()->log("login", Zend_Log::WARN, "USER:: " . $_username . " FAILED LOGIN ON " . date("Y-m-d H:i:s"));
                return "wrongPassword";
            }
        } else {
            Thinkopen_Logger::getInstance()->log("login", Zend_Log::WARN, "USER:: " . $_username . " FAILED LOGIN ON " . date("Y-m-d H:i:s"));
            return "emailNotExist";
        }
    }

    /**
     * Funzione per la gestione dell'azione di logout
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @return boolean
     */
    public static function doLogout() {
        if (self::getUserId()) {
            self::logoutSuccess();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Funzione privata statica per il setting delle variabili 
     * all'interno del namespace di sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_id
     * @param int $_group
     * @param int $_fornitore
     */
    private static function loginSuccess($_user, $_geticketId, $_customerId = NULL, $_phpSessid = NULL) {
        Zend_Session::rememberMe(86400);
        self::getSession()->id = $_user->getUser_id();
        self::getSession()->geticketId = $_geticketId;
        self::getSession()->geticketUserId = $_customerId;
        self::getSession()->phpSessid = $_phpSessid;
    }

    /**
     * Funzione privata statica per il destroy delle variabili di sessione
     * dell'utente all'atto del logout effettuato con successo.
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    private static function logoutSuccess() {
        self::deleteSession();
    }
}
