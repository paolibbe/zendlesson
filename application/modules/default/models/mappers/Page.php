<?php

/**
 * Description of User
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @PrimaryKey: page_id
 * @Table: t_cmspage
 */
class Default_Model_Mapper_Page extends Thinkopen_Abstractmodel {

    public function load($_id, $_forIndex = false) {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_whereCondition = $this->select()->from("t_cmspage_block_sanCarlo")->where("`is_active` = 1")->where("`language` = ?", Top::getCurrentLanguage());
            $_whereFallbackCondition = $this->select()->from("t_cmspage_block_sanCarlo")->where("`is_active` = 1")->where("`language` = ?", "it");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pagecontent", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pagecontent", NULL, $_whereFallbackCondition);
            }
            if (count($_blocks) == 0 && !$_forIndex) {
                throw new Exception("No content specified for page", 404, NULL);
            }
            foreach ($_blocks as $_block) {
                $_objectData->content .= "<div class='blockeditable blocco-" . $_block->block_position . "'>";
                $_objectData->content .= $_block->content;
                $_objectData->content .= "</div>";
            }

            $this->_setData($_objectData);
            return $this;
        } catch (Exception $e) {
            $_logger = Thinkopen_Logger::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage()." (PK:: {$_id})");
            return false;
        }
    }

    private function _fallBackToAnotherLanguage(&$_objectData, $_attributeName, $_value) {
        $_objectData = $this->fetchRow("`is_active` = 1 AND `{$_attributeName}` = '{$_value}'");
        if(Top::getCurrentLanguage() == "it"){
            $_objectData_language = $this->fetchRow("`is_active` = 1 AND `page_id` = '{$_objectData->ref_lang_id}'");
        } else {
            $_objectData_language = $this->fetchRow("`is_active` = 1 AND `ref_lang_id` = '{$_objectData->page_id}' AND `language` = '" . Top::getCurrentLanguage() . "'");
        }
        if ($_objectData_language) {
            $_objectData = $_objectData_language;
        }
    }

    public function loadByAttribute($_attributeName, $_value = NULL) {
        if (!in_array($_attributeName, $this->_getCols())) {
            throw new Exception("Attribute \"" . $_attributeName . "\" is not part of object's attribute set", 500, NULL);
        }
        try {
            $_objectData = $this->fetchRow("`is_active` = 1 AND `{$_attributeName}` = '{$_value}' AND `language` = '" . Top::getCurrentLanguage() . "'");
            if (!$_objectData) {
                $this->_fallBackToAnotherLanguage($_objectData, $_attributeName, $_value);
            }
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_whereCondition = $this->select()->from("t_cmspage_block_sanCarlo")->where("`is_active` = 1")->where("`block_type` = ?", $_objectData->content_type)->where("`language` = ?", Top::getCurrentLanguage());
            $_whereFallbackCondition = $this->select()->from("t_cmspage_block_sanCarlo")->where("`is_active` = 1")->where("`block_type` = ?", $_objectData->content_type)->where("`language` = ?", "it");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pagecontent", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pagecontent", NULL, $_whereFallbackCondition);
            }
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for page", 404, NULL);
            }
            $i = 0;
            foreach ($_blocks as $_block) {
                if ($_block->background_image) {
                    $sfondo = "style='background:url(\"" . BASE_URL . $_block->background_image . "\")'";
                } else {
                    $sfondo = "";
                }
                $_objectData->content .= "<div class='pageblock " . (($i % 2 == 0) ? "even" : "odd") . " blocco-" . $_block->block_position . "' " . $sfondo . ">"
                        . "<div class='row'>
                                <div class='blockeditable col-xs-12 col-sm-12 col-md-12 col-lg-12' data-blockposition='" . $_block->block_position . "'>";
                $_objectData->content .= $_block->content;
                $_objectData->content .= "</div>"
                        . "</div>"
                        . "</div>";
                $i++;
            }
            $this->_setData($_objectData);
            return $this;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Preleva i blocchi relativa alla pagina
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param int $_id
     * @param string $_language
     * @param int $typeblock
     * @return boolean|array
     * @throws Exception
     */
    public function loadBlocks($_id, $_language = "it") {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_blockArray = array();
            $_whereCondition = $this->select()->from("t_cmspage_block_sanCarlo")->where("`language` = ?", $_language)->where("`block_type` = ?", $_objectData->content_type)->order("block_position ASC");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pagecontent", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for page", 404, NULL);
            }
            foreach ($_blocks as $_block) {
                array_push($_blockArray, Top::getModel("bo_dbTable/pagecontent")->setData($_block->toArray()));
            }
            return $_blockArray;
        } catch (Exception $e) {
            return false;
        }
    }

    public function loadBlocksShoulder($_id, $_language = "it") {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_blockArray = array();
            $_whereCondition = $this->select()->from("t_cmspage_shoulder_sanCarlo")->where("`language` = ?", $_language)->order("block_position ASC");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pageshoulder", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for page", 404, NULL);
            }
            foreach ($_blocks as $_block) {
                array_push($_blockArray, Top::getModel("bo_dbTable/pageshoulder")->setData($_block->toArray()));
            }
            return $_blockArray;
        } catch (Exception $e) {
            return false;
        }
    }

    public function loadBlocksShoulderForFe($_id, $_language = "it") {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_blockArray = array();
            $_whereCondition = $this->select()->from("t_cmspage_shoulder_sanCarlo")->where("`is_active` = ?", 1)->where("`language` = ?", $_language)->order("block_position ASC");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Pageshoulder", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for page", 404, NULL);
            }
            $to_return = "";
            foreach ($_blocks as $_block) {
                if (strpos($_block['content'], "{{w_news}}")) {
                    $to_return .= "<div class='col-sm-12 col-lg-12 news_widget_spalla'>";
                } else {
                    $to_return .= "<div class='col-sm-12 col-lg-12'>";
                }
                $to_return .= $_block['content'];
                $to_return .= "</div>";
            }
            return $to_return;
        } catch (Exception $e) {
            return false;
        }
    }

}
