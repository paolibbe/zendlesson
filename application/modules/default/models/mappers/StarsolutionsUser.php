<?php

/**
 * Class Default_Model_Mapper_StarsolutionsUser
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Default_Model_Mapper_StarsolutionsUser extends Zend_Db_Table_Abstract {

    const RESULT_ARRAY = 1;
    const RESULT_STDCLASS = 2;

    /**
     * @var /Zend_Session_Namespace
     */
    private static $_mySession = null;

    /**
     * @var string $_name Attributo protetto ereditato da Zend_Db_Table_Abstract che identifica la tabella sul DB
     */
    protected $_name;
    protected $_dbInitialize;

    /**
     * Default_Model_Mapper_StarsolutionsUser constructor.
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array|mixed $config
     */
    public function __construct($config) {
        $this->_dbInitialize = $_dbInitialize = Thinkopen_DbInitialize::getInstance();
        $_iniConfigs = $_dbInitialize::$iniConfig->database->table;
        $this->_name = $_iniConfigs->prefix . "utenti" . $_iniConfigs->suffix;
        parent::__construct($config);
    }

    /**
     * Metodo per la load di un
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param mixed $_value
     * @param string $_field
     * @param int $_format RESULT_ARRAY -> 1 (default), RESULT_STDCLASS -> 2
     * @return mixed
     */
    public function load($_value, $_field = "id", $_format = self::RESULT_ARRAY){
        $_userData = $this->select()->where("{$_field} = ?", $_value)->query();
        if($_userData) {
            return (($_format == self::RESULT_ARRAY) ? $_userData->fetch() : $_userData->fetchObject());
        } else {
            return false;
        }
    }

    /**
     * Metodo per recuperare i dati dell'utente per la form di EDIT
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param $_keysNotUpdatables
     * @return array
     */
    public function getCleanDataForPopulate($_keysNotUpdatables){
        $_userData = $this->load(self::getUsername(), "username", self::RESULT_ARRAY);
        if($_userData) {
            $_userDataFiltered = array_diff_key($_userData, array_flip($_keysNotUpdatables));
            $_dataOfBirth = new Zend_Date($_userDataFiltered['dob']);
            $_userDataFiltered['dob'] = $_dataOfBirth->toString("d/MM/y");
            return $_userDataFiltered;
        } else {
            return array();
        }
    }

    /**
     * Metodo per la gestione della login (validazione credenziali)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param String $_username
     * @param String $_password
     * @return Zend_Auth_Result
     */
    public function authenticate($_username, $_password){
        $_adapter = new Zend_Auth_Adapter_DbTable(
            Thinkopen_DbInitialize::getDbInstance(),
            $this->_name,
            'username',
            'password'
        );
        $_adapter->setIdentity($_username);
        $_adapter->setCredential(base64_encode(sha1($_password)));
        $_auth = Zend_Auth::getInstance();
        $_result = $_auth->authenticate($_adapter);
        $_isValid = $_result->isValid();
        if($_isValid){
            $this->loginSuccess($_result->getIdentity());
        }
        return $_isValid;
    }

    /**
     * Funzione di get per la sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return mixed
     */
    static public function getSession() {
        $_configs = Thinkopen_DbInitialize::getInstance();
        $_namespace = $_configs::$iniConfig->database->table->suffix;
        if (self::$_mySession === NULL) {
            self::$_mySession = new Zend_Session_Namespace('users' . $_namespace);
            if (!isset(self::$_mySession->initialized)) {
                Zend_Session::regenerateId();
                self::$_mySession->initialized = true;
            }
        }
        return self::$_mySession;
    }

    /**
     * Funzione privata statica per il setting delle variabili
     * all'interno del namespace di sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param \Zend_Auth_Result $_user
     */
    private function loginSuccess($_username) {
        Zend_Session::rememberMe(86400);
        self::getSession()->username = $_username;
        self::getSession()->user = $this->fetchRow("`username` = '{$_username}'")->toArray();
    }

    /**
     * Cancella la sessione dell'utente (logout)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    static public function deleteSession() {
        self::getSession()->username = null;
        self::getSession()->user = null;
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
        Zend_Session::regenerateId();
    }

    /**
     * Ritorna l'username dell'utente correntemente loggato se esiste,
     * altrimenti false
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return String|boolean
     */
    public static function getUsername() {
        if (self::getSession()->username) {
            return self::getSession()->username;
        } else {
            return false;
        }
    }

    /**
     * Metodo per il controllo se esista o meno un utente loggato all'applicazione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return bool
     */
    public static function isLoggedIn(){
        $_authenticator = Zend_Auth::getInstance();
        return ($_authenticator->hasIdentity()) ? true : false;
    }

    /**
     * Metodo per il logout dell'utente
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return bool
     */
    public static function logout(){
        if(!is_null(self::getSession()->username)){
            self::deleteSession();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo per l'update dell'utente
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_userId
     * @param array|string $_userData
     * @return stdClass
     */
    public function edit($_userId, array $_userData){
        $_response = new stdClass();
        try{
            $_dob = new Zend_Date($_userData['dob']);
            $_userData['dob'] = $_dob->toString("Y-MM-d");
            if(!is_null($_userData['password'])) {
                $_userData['password'] = base64_encode(sha1($_userData['password']));
            } else {
                unset($_userData['password']);
            }
            $_userData['updated_at'] = date("Y-m-d H:i:s");
            $_rowsUpdated = $this->update($_userData, "id = {$_userId}");
            self::getSession()->username = $_userData['username'];
            if($_rowsUpdated == 1){
                $_response->result = TRUE;
                $_response->message = "Utente updatato correttamente";
            } else {
                throw new Exception("No ID founded", 500);
            }
            return $_response;
        } catch (Zend_Db_Exception $e){
            $_response->result = FALSE;
            $_response->message = $e->getMessage();
            return $_response;
        } catch (Exception $f){
            $_response->result = FALSE;
            $_response->message = "Something bad happened";
            return $_response;
        }
    }

    /**
     * Metodo per la creazione di un utente
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $_userData
     * @return stdClass
     */
    public function create(array $_userData){
        $_response = new stdClass();
        try{
            $_dob = new Zend_Date($_userData['dob']);
            $_userData['dob'] = $_dob->toString("Y-MM-d");
            $_password = $_userData['password'];
            $_userData['password'] = base64_encode(sha1($_userData['password']));
            $_userData['created_at'] = $_userData['updated_at'] = date("Y-m-d H:i:s");
            $_newRow = $this->createRow($_userData);
            $_newRow->save();
            $this->authenticate($_userData['username'], $_password);
            $_response->result = TRUE;
            $_response->message = "Utente creato correttamente";
            return $_response;
        } catch (Zend_Db_Exception $e){
            $_response->result = FALSE;
            $_response->message = $e->getMessage();
            return $_response;
        } catch (Exception $f){
            $_response->result = FALSE;
            $_response->message = "Something bad happened";
            return $_response;
        }
    }

}