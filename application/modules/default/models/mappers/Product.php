<?php

/**
 * Class Default_Model_Mapper_StarsolutionsUser
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Default_Model_Mapper_Product extends Zend_Db_Table_Abstract {

    const RESULT_ARRAY = 1;
    const RESULT_STDCLASS = 2;

    /**
     * @var /Zend_Session_Namespace
     */
    private static $_mySession = null;

    /**
     * @var string $_name Attributo protetto ereditato da Zend_Db_Table_Abstract che identifica la tabella sul DB
     */
    protected $_name;
    protected $_dbInitialize;

    /**
     * Default_Model_Mapper_StarsolutionsUser constructor.
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array|mixed $config
     */
    public function __construct($config) {
        $this->_dbInitialize = $_dbInitialize = Thinkopen_DbInitialize::getInstance();
        $_iniConfigs = $_dbInitialize::$iniConfig->database->table;
        $this->_name = $_iniConfigs->prefix . "prodotti" . $_iniConfigs->suffix;
        parent::__construct($config);
    }

    /**
     * Metodo per la load di un
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param mixed $_value
     * @param string $_field
     * @param int $_format RESULT_ARRAY -> 1 (default), RESULT_STDCLASS -> 2
     * @return mixed
     */
    public function load($_value, $_field = "id", $_format = self::RESULT_ARRAY){
        $_userData = $this->select()->where("{$_field} = ?", $_value)->query();
        if($_userData) {
            return (($_format == self::RESULT_ARRAY) ? $_userData->fetch() : $_userData->fetchObject());
        } else {
            return false;
        }
    }

    /**
     * Metodo per recuperare i dati dell'utente per la form di EDIT
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param $_keysNotUpdatables
     * @return array
     */
//    public function getCleanDataForPopulate($_id){
//        $_productData = $this->load($_id, "id", self::RESULT_ARRAY);
//        if($_productData) {
//            $_productDataFiltered = array_diff_key($_productData, array_flip($_keysNotUpdatables));
//            $_fromDate = new Zend_Date($_productDataFiltered['from_date']);
//            $_productDataFiltered['from_date'] = $_fromDate->toString("d/MM/y");
//            $_toDate = new Zend_Date($_productDataFiltered['to_date']);
//            $_productDataFiltered['to_date'] = $_toDate->toString("d/MM/y");
//            return $_productDataFiltered;
//        } else {
//            return array();
//        }
//    }

    /**
     * Funzione di get per la sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return mixed
     */
    static public function getSession() {
        $_configs = Thinkopen_DbInitialize::getInstance();
        $_namespace = $_configs::$iniConfig->database->table->suffix;
        if (self::$_mySession === NULL) {
            self::$_mySession = new Zend_Session_Namespace('users' . $_namespace);
            if (!isset(self::$_mySession->initialized)) {
                Zend_Session::regenerateId();
                self::$_mySession->initialized = true;
            }
        }
        return self::$_mySession;
    }

    /**
     * Cancella la sessione dell'utente (logout)
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    static public function deleteSession() {
        self::getSession()->username = null;
        self::getSession()->user = null;
    }


    /**
     * Metodo per l'update dell'utente
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param int $_productId
     * @param array|string $_productData
     * @return stdClass
     */
    public function edit($_productId, array $_productData){
        $_response = new stdClass();
        try{
            $_fromDate = new Zend_Date($_productData['from_date']);
            $_toDate = new Zend_Date($_productData['to_date']);
            $_productData['from_date'] = $_fromDate->toString("Y-MM-d");
            $_productData['to_date'] = $_toDate->toString("Y-MM-d");
            $_rowsUpdated = $this->update($_productData, "id = {$_productId}");
            if($_rowsUpdated == 1){
                $_response->result = TRUE;
                $_response->message = "Prodotto updatato correttamente";
            } else {
                throw new Exception("No ID founded", 500);
            }
            return $_response;
        } catch (Zend_Db_Exception $e){
            $_response->result = FALSE;
            $_response->message = $e->getMessage();
            return $_response;
        } catch (Exception $f){
            $_response->result = FALSE;
            $_response->message = "Something bad happened";
            return $_response;
        }
    }

    /**
     * Metodo per la creazione di un prodotto
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param array $_productData
     * @return stdClass
     */
    public function create(array $_productData){
        $_response = new stdClass();
        try{
            $_fromDate = new Zend_Date($_productData['from_date']);
            $_toDate = new Zend_Date($_productData['to_date']);
            $_productData['from_date'] = $_fromDate->toString("Y-MM-d");
            $_productData['to_date'] = $_toDate->toString("Y-MM-d");
            $_newRow = $this->createRow($_productData);
            $_newRow->save();
            $_response->result = TRUE;
            $_response->message = "Utente creato correttamente";
            return $_response;
        } catch (Zend_Db_Exception $e){
            $_response->result = FALSE;
            $_response->message = $e->getMessage();
            return $_response;
        } catch (Exception $f){
            $_response->result = FALSE;
            $_response->message = $f->getMessage();
            return $_response;
        }
    }

    public function listAll(){
        return $this->select()->query()->fetchAll();
    }

    public function buy($_productId){
        $_product = $this->load($_productId, "id");
        if($_product && $_product['qty'] > 0){
            $_product['qty'] = $_product['qty'] - 1;
            $_resultEdit = $this->edit($_productId, $_product);
            $_userModel = new Default_Model_Mapper_StarsolutionsUser();
            $_userModel->getSession()->cartqty[$_product['id']]++;
            return json_decode(json_encode($_resultEdit), true);
        } else {
            return array("result" => false, "message" => "Il prodotto non è vendibile");
        }
    }

    public function remove($_productId){
        $_product = $this->load($_productId, "id");
        $_userModel = new Default_Model_Mapper_StarsolutionsUser();
        if($_product && isset($_userModel->getSession()->cartqty[$_product['id']])){
            $_product['qty'] = $_product['qty'] + 1;
            $_resultEdit = $this->edit($_productId, $_product);
            if($_userModel->getSession()->cartqty[$_product['id']] > 1) {
                $_userModel->getSession()->cartqty[$_product['id']]--;
            } else {
                unset($_userModel->getSession()->cartqty[$_product['id']]);
            }
            return json_decode(json_encode($_resultEdit), true);
        } else {
            return array("result" => false, "message" => "Prodotto non in carrello");
        }
    }

}