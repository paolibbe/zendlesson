<?php

/**
 * Description of User
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @PrimaryKey: news_id
 * @Table: t_cmsnews
 */
class Default_Model_Mapper_News extends Thinkopen_Abstractmodel {

    public function load($_id, $_isPreview = false, $_isForIndex = false) {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            if (!$_isPreview) {
                $_whereCondition = $this->select()->from("t_cmsnews_block_sanCarlo")->where("`is_active` = 1")->where("`language` = ?", Top::getCurrentLanguage());
                $_whereFallbackCondition = $this->select()->from("t_cmsnews_block_sanCarlo")->where("`is_active` = 1")->where("`language` = ?", "it");
                $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newscontent", NULL, $_whereCondition);
                if (count($_blocks) == 0) {
                    $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newscontent", NULL, $_whereFallbackCondition);
                }
                if (count($_blocks) == 0 && !$_isForIndex) {
                    throw new Exception("No content specified for news", 404, NULL);
                }
                foreach ($_blocks as $_block) {
                    $_objectData->content .= "<div class='blockeditable blocco-" . $_block->block_position . "'>";
                    $_objectData->content .= $_block->content;
                    $_objectData->content .= "</div>";
                }
            }
            $this->setData($_objectData);
            return $this;
        } catch (Exception $e) {
            $_logger = Thinkopen_Logger::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage()." (PK:: {$_id})");
            return false;
        }
    }
    
    private function _fallBackToAnotherLanguage(&$_objectData, $_attributeName, $_value) {
        $_objectDataFallback = $this->fetchRow("`is_active` = 1 AND `{$_attributeName}` = '{$_value}'");
        if(Top::getCurrentLanguage() == "it"){
            $_objectData_language = $this->fetchRow("`is_active` = 1 AND `news_id` = '{$_objectDataFallback->ref_lang_id}'");
        } else {
            $_objectData_language = $this->fetchRow("`is_active` = 1 AND `ref_lang_id` = '{$_objectDataFallback->news_id}' AND `language` = '" . Top::getCurrentLanguage() . "'");
        }
        if ($_objectData_language) {
            $_objectData = $_objectData_language;
        }
    }

    public function loadByAttribute($_attributeName, $_value = NULL) {
        if (!in_array($_attributeName, $this->_getCols())) {
            throw new Exception("Attribute \"" . $_attributeName . "\" is not part of object's attribute set", 500, NULL);
        }
        try {
            $_objectData = $this->fetchRow("`is_active` = 1 AND `{$_attributeName}` = '{$_value}' AND `language` = '" . Top::getCurrentLanguage() . "'");
            if (!$_objectData) {
                $this->_fallBackToAnotherLanguage($_objectData, $_attributeName, $_value);
            }
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_whereCondition = $this->select()->from("t_cmsnews_block_sanCarlo")->where("`is_active` = 1")->where("`language` = ?", Top::getCurrentLanguage());
            $_whereFallbackCondition = $this->select()->from("t_cmsnews_block_sanCarlo")->where("`is_active` = 1")->where("`language` = ?", "it");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newscontent", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newscontent", NULL, $_whereFallbackCondition);
            }
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for news", 404, NULL);
            }
            $i = 0;
            $_objectData->content = "";
            foreach ($_blocks as $_block) {
                $_objectData->content .= "<div class='pageblock " . (($i % 2 == 0) ? "even" : "odd") . " blocco-" . $_block->block_position . "'>"
                        . "<div class='row'>
                                <div class='blockeditable col-xs-12 col-sm-12 col-md-12 col-lg-12' data-blockposition='".$_block->block_position."'>";
                $_objectData->content .= $_block->content;
                $_objectData->content .= "</div>"
                        . "</div>"
                        . "</div>";
                $i++;
            }
            $this->setData($_objectData);
            return $this;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function getLatestNews($_newsNumber = 5){
        $lang = Top::getCurrentLanguage();
        $_arrayNews = array();
        foreach($this->fetchAll("`is_active` = 1 AND language = '".$lang."'", "created_at DESC", $_newsNumber, 0) as $_news){
            array_push($_arrayNews, Top::getModel("mapper/news")->setData($_news));
        }
        return $_arrayNews;
    }
    
    
    /**
     * Preleva i blocchi relativa alla news
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param int $_id
     * @param string $_language
     * @return boolean|array
     * @throws Exception
     */
    public function loadBlocks($_id, $_language = "it") {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_blockArray = array();
            $_whereCondition = $this->select()->from("t_cmsnews_block_sanCarlo")->where("`language` = ?", $_language)->where("`block_type` = ?", $_objectData->content_type)->order("block_position ASC");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newscontent", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for news", 404, NULL);
            }
            foreach ($_blocks as $_block) {
                array_push($_blockArray, Top::getModel("bo_dbTable/newscontent")->setData($_block->toArray()));
            }
            return $_blockArray;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    public function loadBlocksShoulder($_id, $_language = "it") {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_blockArray = array();
            $_whereCondition = $this->select()->from("t_cmsnews_shoulder_sanCarlo")->where("`language` = ?", $_language)->order("block_position ASC");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newsshoulder", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for news", 404, NULL);
            }
            foreach ($_blocks as $_block) {
                array_push($_blockArray, Top::getModel("bo_dbTable/newsshoulder")->setData($_block->toArray()));
            }
            return $_blockArray;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    public function loadBlocksShoulderForFe($_id, $_language = "it") {
        try {
            $_objectData = $this->fetchRow($this->_primaryKey . " = $_id");
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            }
            $_blockArray = array();
            $_whereCondition = $this->select()->from("t_cmsnews_shoulder_sanCarlo")->where("`is_active` = ?", 1)->where("`language` = ?", $_language)->order("block_position ASC");
            $_blocks = $_objectData->findDependentRowset("Bo_Model_DbTable_Newsshoulder", NULL, $_whereCondition);
            if (count($_blocks) == 0) {
                throw new Exception("No content specified for news", 404, NULL);
            }
            $to_return = "";
            foreach ($_blocks as $_block) {
                if (strpos($_block['content'],"{{w_news}}")) {
                    $to_return .= "<div class='col-sm-12 col-lg-12 news_widget_spalla'>";
                }else{
                    $to_return .= "<div class='col-sm-12 col-lg-12'>";
                }
                $to_return .= $_block['content'];
                $to_return .= "</div>";
            }
            return $to_return;
        } catch (Exception $e) {
            return false;
        }
    }

}
