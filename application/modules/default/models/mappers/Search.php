<?php

/**
 * Description of Search
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Default_Model_Mapper_Search {

    protected static $_instance;
    
    public static function getInstance() {
        if (self::$_instance === NULL) {
            $_className = get_called_class();
            self::$_instance = new $_className();
        }
        return self::$_instance;
    }

    /**
     * Configurazioni di sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    private static $_mySession = null;

    /**
     * Funzione di get per la sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @return mixed
     */
    static public function getSession() {
        $_configs = Thinkopen_DbInitialize::getInstance();
        $_namespace = $_configs::$iniConfig->database->table->suffix;
        if (self::$_mySession === NULL) {
            self::$_mySession = new Zend_Session_Namespace('search' . $_namespace);
            if (!isset(self::$_mySession->initialized)) {
                Zend_Session::regenerateId();
                self::$_mySession->initialized = true;
            }
        }
        return self::$_mySession;
    }

    /**
     * Cancella la sessione dell'utente (logout)
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public static function deleteSession() {
        self::getSession()->id = null;
        self::getSession()->querystring = null;
        Zend_Session::destroy();
        Zend_Session::regenerateId();
    }

    public static function getQuerystring() {
        if (self::getSession()->querystring) {
            return self::getSession()->querystring;
        } else {
            return false;
        }
    }

    /**
     * Funzione privata statica per il setting delle variabili 
     * all'interno del namespace di sessione
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     * @param string $_queryString Stringa di ricerca
     */
    public static function setQuerystring($_queryString) {
        Zend_Session::rememberMe(3600);
        self::getSession()->id = time();
        self::getSession()->querystring = $_queryString;
    }

}
