<?php

class ProductController extends Zend_Controller_Action {

    /**
     * @var /Default_Model_Mapper_StarsolutionsUser
     */
    protected $_productModel = NULL;

    public function preDispatch() {
        $this->_productModel = new Default_Model_Mapper_Product();
        $this->view->headTitle("Prodotti");
        parent::preDispatch();
    }

//    public function editAction() {
//        $_productModel = $this->_productModel;
//        if($this->getRequest()->getParam('id', false)) {
//            $_productEditForm = new Default_Form_Product_Edit();
//            $_userDataFiltered = $_productModel->getCleanDataForPopulate($this->getRequest()->getParam('id'));
//            $_productEditForm->populate($_userDataFiltered);
//            if($this->getRequest()->getParam("name", false) ||
//                $this->getRequest()->getParam("sku", false) ||
//                $this->getRequest()->getParam("price", false) ||
//                $this->getRequest()->getParam("qty", false)) {
//                $_isFormValid = $_productEditForm->isValid($this->getRequest()->getParams());
//                if ($_isFormValid) {
//                    $result = $_productModel->edit($_userDataFiltered['id'], $_productEditForm->getValues());
//                    if ($result->result) {
//                        $_productEditForm->clearErrorMessages();
//                        $this->redirect("default/product/index");
//                        return;
//                    } else {
//                        $this->view->createError = $result->message;
//                    }
//                }
//            } else {
//                $_productEditForm->clearErrorMessages();
//            }
//            $this->view->form = $_productEditForm;
//        } else {
//            $this->redirect(Top::getBaseUrl() . "default/product/create");
//        }
//    }

    public function indexAction() {
        $_message = $this->getRequest()->getParam("result", NULL);
        switch($_message){
            case NULL:
            default:
                $_messageColor = NULL;
                break;
            case true:
                $_messageColor = "green";
                break;
            case false:
                $_messageColor = "red";
                break;
        }
        $this->view->message = $this->getRequest()->getParam("message");
        $this->view->messageColor = $_messageColor;
        $this->view->productList = $this->_productModel->listAll();
    }

    public function detailAction(){
        $_productUrl = $this->getRequest()->getParam("product_url", false);
        if($_productUrl){
            if(stristr($_productUrl, ".html") === false){
                $_productUrl .= ".html";
            }
            $_product = $this->_productModel->load($_productUrl, "url");
            if($_product){
                $this->view->datiProdotto = $_product;
            } else {
                $this->redirect("default/product/index");
            }
        }
    }

    public function createAction()
    {
        $_productCreateForm = new Default_Form_Product_Create();
        if($this->getRequest()->getParam("name", false) ||
            $this->getRequest()->getParam("sku", false) ||
            $this->getRequest()->getParam("price", false) ||
            $this->getRequest()->getParam("qty", false)) {
            $_isFormValid = $_productCreateForm->isValid($this->getRequest()->getParams());
            if ($_isFormValid) {
                $result = $this->_productModel->create($_productCreateForm->getValues());
                if ($result->result) {
                    $_productCreateForm->clearErrorMessages();
                    $_productCreateForm->reset();
                    $this->redirect("default/product/index");
                    return;
                } else {
                    $this->view->createError = $result->message;
                }
            }
        } else {
            $_productCreateForm->clearErrorMessages();
            $_productCreateForm->reset();
        }
        $this->view->form = $_productCreateForm;
    }

    public function buyAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $_userModel = new Default_Model_Mapper_StarsolutionsUser();
        if(!$_userModel::isLoggedIn()) {
            $this->redirect("default/user/login");
        } else {
            if($this->getRequest()->getParam("product_id", false)) {
                $_result = $this->_productModel->buy($this->getRequest()->getParam("product_id"));
                if($_result['result'] === true){
                    $_result['message'] = "Prodotto acquistato con successo";
                    $this->redirect("default/product/index?" . http_build_query($_result));
                } else {
                    $this->redirect("default/product/index", $_result);
                }
            } else {
                $this->redirect("default/product/index", array("result" => false, "message" => "Please specify product"));
            }
        }
    }

    public function removeAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $_userModel = new Default_Model_Mapper_StarsolutionsUser();
        if(!$_userModel::isLoggedIn()) {
            $this->redirect("default/user/login");
        } else {
            if($this->getRequest()->getParam("product_id", false)) {
                $_result = $this->_productModel->remove($this->getRequest()->getParam("product_id"));
                if($_result['result'] === true){
                    $_result['message'] = "Prodotto rimosso con successo";
                    $this->redirect("default/user/account?" . http_build_query($_result));
                } else {
                    $this->redirect("default/user/account?"  .http_build_query($_result));
                }
            } else {
                $this->redirect("default/product/index", array("result" => false, "message" => "Please specify product"));
            }
        }
    }
}
