<?php

class ErrorController extends Zend_Controller_Action {

    public function errorAction() {
        $this->_helper->layout->setLayout('layout/layout');
        $error = $this->_getParam('error_handler');
        switch (get_class($error->exception)) {
            case 'PageNotFoundException':
            case 'Zend_Controller_Dispatcher_Exception':
                $this->forward('page-not-found', NULL, NULL, $this->getAllParams());
                break;

            case 'NotAuthorizedException':
                $this->forward('not-authorized', NULL, NULL, $this->getAllParams());
                break;

            default:
                $this->forward("generic-error", NULL, NULL, $this->getAllParams());
                break;
        }
    }

    public function pageNotFoundAction() {
        
    }

    public function notAuthorizedAction() {
        $this->view->content = "NOT AUTHORIZED";
        print_r($this->getAllParams());
    }

    public function genericErrorAction() {
        $this->view->content = "GENERIC";
        $logger = Thinkopen_Logger::getInstance();
        $logger->log("GENERIC_ERROR", Zend_Log::ERR, (string)$this->getParam("error_handler")->exception);
    }

}
