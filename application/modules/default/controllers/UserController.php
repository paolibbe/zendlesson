<?php

class UserController extends Zend_Controller_Action {

    /**
     * @var /Default_Model_Mapper_StarsolutionsUser
     */
    protected $_userModel = NULL;
    private $_keysNotUpdatable = NULL;

    public function preDispatch() {
        $this->_userModel = new Default_Model_Mapper_StarsolutionsUser();
        $this->_keysNotUpdatable = array("password", "created_at", "updated_at");
        parent::preDispatch();
    }

    public function editAction() {
        $_userModel = $this->_userModel;
        if($_userModel::isLoggedIn()) {
            $_loginForm = new Default_Form_User_Edit();
            $_userDataFiltered = $_userModel->getCleanDataForPopulate($this->_keysNotUpdatable);
            $_loginForm->populate($_userDataFiltered);
            if($this->getRequest()->getParam("password", false) ||
                $this->getRequest()->getParam("username", false) ||
                $this->getRequest()->getParam("email", false) ||
                $this->getRequest()->getParam("dob", false)) {
                $_isFormValid = $_loginForm->isValid($this->getRequest()->getParams());
                if ($_isFormValid) {
                    $result = $_userModel->edit($_userDataFiltered['id'], $_loginForm->getValues());
                    if ($result->result) {
                        $_loginForm->clearErrorMessages();
                        $this->redirect("default/user/account");
                        return;
                    } else {
                        $this->view->createError = $result->message;
                    }
                }
            } else {
                $_loginForm->clearErrorMessages();
            }
            $this->view->form = $_loginForm;
        } else {
            $this->redirect(Top::getBaseUrl() . "default/user/create");
        }
    }

    public function indexAction() {
        $_userModel = $this->_userModel;
        if($_userModel::isLoggedIn()){
            $this->redirect(Top::getBaseUrl() . "default/user/account");
        } else {
            $this->redirect(Top::getBaseUrl() . "default/user/login");
        }
    }

    public function loginAction()
    {
        $_userModel = $this->_userModel;
        if(!$_userModel::isLoggedIn()) {
            $loginForm = new Default_Form_User_Login();
            if($this->getRequest()->getParam("password", false) || $this->getRequest()->getParam("username", false)) {
                $_isFormValid = $loginForm->isValid($this->getRequest()->getParams());
                if ($_isFormValid) {
                    $result = $this->_userModel->authenticate($loginForm->getValue('username'), $loginForm->getValue('password'));
                    if ($result) {
                        $this->_helper->FlashMessenger('Successful Login');
                        $this->redirect("default/user/login");
                        return;
                    } else {
                        $this->view->loginError = "Errore di autenticazione";
                    }
                } else {
                    $this->view->loginError = "Errore di autenticazione";
                }
            }
            $this->view->form = $loginForm;
        } else {
            $this->redirect(Top::getBaseUrl() . "default/user/account");
        }

    }

    public function createAction()
    {
        $_userModel = $this->_userModel;
        if(!$_userModel::isLoggedIn()) {
            $_loginForm = new Default_Form_User_Create();
            if($this->getRequest()->getParam("password", false) ||
                $this->getRequest()->getParam("username", false) ||
                $this->getRequest()->getParam("email", false) ||
                $this->getRequest()->getParam("dob", false)) {
                $_isFormValid = $_loginForm->isValid($this->getRequest()->getParams());
                if ($_isFormValid) {
                    $result = $this->_userModel->create($_loginForm->getValues());
                    if ($result->result) {
                        $_loginForm->clearErrorMessages();
                        $_loginForm->reset();
                        $this->redirect("default/user/account");
                        return;
                    } else {
                        $this->view->createError = $result->message;
                    }
                }
            } else {
                $_loginForm->clearErrorMessages();
                $_loginForm->reset();
            }
            $this->view->form = $_loginForm;
        } else {
            $this->redirect(Top::getBaseUrl() . "default/user/edit");
        }

    }

    public function accountAction(){
        $_userModel = $this->_userModel;
        $this->view->username = $_userModel::getUsername();
        $_itemsInCart = array();
        if(count($_userModel->getSession()->cartqty) != 0){
            foreach($_userModel->getSession()->cartqty as $_itemId => $_qty){
                $_productModel = new Default_Model_Mapper_Product();
                $_product = $_productModel->load($_itemId, "id");
                if($_product){
                    array_push($_itemsInCart, array("qty" => $_qty, "name" => $_product['name'], "id" => $_product['id']));
                }
            }
        }
        $this->view->elementiCarrello = $_itemsInCart;
    }

    public function logoutAction(){
        $_userModel = $this->_userModel;
        if($_userModel::isLoggedIn()){
            $_userModel::logout();
            $this->redirect(Top::getBaseUrl());
        } else {
            $this->redirect(Top::getBaseUrl() . "default/user/login");
        }
    }

}
