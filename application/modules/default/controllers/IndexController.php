<?php

class IndexController extends Zend_Controller_Action {

    protected $_cache;
    protected $_sessionToken;

    public function indexAction() {
        $this->view->headTitle("Home");
        $this->view->titleSectionOne = "Primo blocco";
        $this->view->titleSectionTwo = "Secondo blocco";
        $this->view->titleSectionThree = "Terzo blocco";
        if($this->getParam("layout", false) == "alternative") {
            $this->_helper->viewRenderer('index/alternative', null, true);
        }
    }

    public function getjqueryAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $_controller = $this->_request->getParam("controller");
        try {
            $this->renderScript('layout/dynjs/jquery_' . trim($_controller) . '.phtml');
        } catch (Exception $e) {

        }
    }

}
