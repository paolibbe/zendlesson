<?php
/**
 * Description of Zend_View_Helper_Image
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Zend_View_Helper_Image extends Zend_View_Helper_Abstract {

    private $_imageResizer = NULL;

    public function image($_file, $_array = array(), $_base64 = false) {
        if ($this->_imageResizer === NULL) {
            $this->_imageResizer = Thinkopen_Resizeimage::getInstance();
        }
        $_check = ($this->_imageResizer->load($_file) !== false) ? true : false;
        if ($_check) {
            if ($_base64 === true) {
                return "data:image/png;base64," . $this->_imageResizer->output($_array['width'], $_array['height'], $_array['forceweh']);
            } else {
                return $this->_imageResizer->saveImage($_array['width'], $_array['height'], (isset($_array['forceweh']) ? $_array['forceweh'] : false));
            }
        } else {
            $logger = Thinkopen_Logger::getInstance();
            $logger->log("IMAGEHELPER", Zend_Log::WARN, "Image ".$_file." cannot be resized");
            return BASE_URL . $_file;
        }
    }

}
