<?php
/**
 * Description of Default_View_Helper_Url
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Zend_View_Helper_Url extends Zend_View_Helper_Abstract {

    private $router = NULL;

    private function _isStar($_string, $_fallback) {
        return ($_string === "*") ? $_fallback : $_string;
    }

    private function _declineFromPath($_pathExploded){
        switch (count($_pathExploded)) {
            case 1:
                $_urlOptions['action'] = $this->_isStar($_pathExploded[0], $this->router->getFrontController()->getRequest()->getActionName());
                $_urlOptions['controller'] = $this->router->getFrontController()->getRequest()->getControllerName();
                $_urlOptions['module'] = $this->router->getFrontController()->getRequest()->getModuleName();
                break;
            case 2:
                $_urlOptions['controller'] = $this->_isStar($_pathExploded[0], $this->router->getFrontController()->getRequest()->getControllerName());
                $_urlOptions['action'] = $this->_isStar($_pathExploded[1], $this->router->getFrontController()->getRequest()->getActionName());
                $_urlOptions['module'] = $this->router->getFrontController()->getRequest()->getModuleName();
                break;
            case 3:
                $_urlOptions['module'] = $this->_isStar($_pathExploded[0], $this->router->getFrontController()->getRequest()->getModuleName());
                $_urlOptions['controller'] = $this->_isStar($_pathExploded[1], $this->router->getFrontController()->getRequest()->getControllerName());
                $_urlOptions['action'] = $this->_isStar($_pathExploded[2], $this->router->getFrontController()->getRequest()->getActionName());
                break;
            default:
                $_urlOptions['action'] = $this->router->getFrontController()->getRequest()->getActionName();
                $_urlOptions['controller'] = $this->router->getFrontController()->getRequest()->getControllerName();
                $_urlOptions['module'] = $this->router->getFrontController()->getRequest()->getModuleName();
        }
        return $_urlOptions;
    }
    
    public function url($_path, $_params = array()) {
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $_pathExploded = explode("/", $_path);
        $_urlOptions = $this->_declineFromPath($_pathExploded);
        $_urlToReturn = $this->router->assemble(array_merge($_urlOptions, $_params), null, false, true);
        if (substr($_urlToReturn, -5) === ".html" && count($_params) != 0) {
            $_urlToReturn .= "?";
            $_counter = 0;
            foreach ($_params as $_key => $_value) {
                $_urlToReturn .= ($_counter == 0) ? ($_key . "=" . $_value) : ("&" . $_key . "=" . $_value);
                $_counter++;
            }
        }
        return $_urlToReturn;
    }

}
