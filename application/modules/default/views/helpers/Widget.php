<?php

include_once '../application/modules/default/views/helpers/Image.php';

class Zend_View_Helper_Widget extends Zend_View_Helper_Abstract {

    private static function _twitter($isHome = false) {
        try {
            $to_return = ($isHome) ? "" : "<div class='title_widget'><h3> Tweets </h3></div>";
            $to_return .= "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 '>";
            if (!$isHome) {
                $to_return .= "<script>$(document).ready(function ($) {  if ($(window).width() > 768) { $('#tweetsScroll').perfectScrollbar(); } })</script>";
            }
            $_twitter = Thinkopen_Twitter::getInstance();
            $_tweets = $_twitter->getLastTweets();
            $_mediasImage = $_twitter->getTweetsImage(12);
            if ($isHome) {
                $to_return .= "<div class='col-xs-12 col-sm-6 col-md-6 col-lg-7 ' style='margin-bottom: 15px;'>";
            } else {
                $to_return .= "<div class='col-xs-12 col-sm-6 col-md-12 col-lg-12 '>";
            }
            $to_return .= '<div class="news ps-container ps-active-y" id="tweetsScroll">';
            $to_return .= "<ul class='list-unstyled news_list tweets_list'>";
            $cnt = 0;
            foreach ($_tweets as $_tweet) {
                $_match = array();
                $regx = "/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:.,<>?«»“”‘’]))/";
                preg_match($regx, $_tweet->text, $_match);
                $text = str_replace($_match[0], "<a href='" . $_match[0] . "' target='_blank'>{$_match[0]}</a>", $_tweet->text);
                $to_return .= "<li class ='tweets_list_li'>";
                $to_return .= "<div class='row'>";
                $to_return .= "<a href='{$_tweet->userLink}' class='tweets_list_author'>" . $_tweet->userAtName . "</a>";
                $to_return .= "<div class=' tweets_list_content'>" . $text . "</div>";
                $to_return .= "<div class='tweets_list_date'>" . $_tweet->date . "</div>";
                $to_return .= "</div>";
                if ($cnt < ((count($_tweets)) - 1)) {
                    $to_return .= "<hr class='hr_tweet'>";
                }
                $to_return .= "</li>";
                $cnt++;
            }
            $to_return .= "</ul>";
            $to_return .= "</div>";
            $to_return .= "</div>";
            if ($isHome) {
                $to_return .= "<div class='col-xs-12 col-sm-6 col-md-6 col-lg-5 '>";
            } else {
                $to_return .= "<div class='col-xs-12 col-sm-6 col-md-12 col-lg-12 ' style='margin-bottom: 15px; margin-top: 10px;'>";
            }
            $_counter = 0;
            if ($isHome) {
                $ref = 3;
            } else {
                $ref = 4;
            }
            foreach ($_mediasImage as $_singleMedia) {
                if ($_counter % $ref == 0 || $_counter == (count($_mediasImage))) {
                    if ($_counter != 0) {
                        $to_return .= "</div>";
                    }
                    $to_return .= "<div class='row img_tweeter'>";
                }
                $to_return .= "<a href='{$_singleMedia['url']}' target='_blank'>";
                if ($isHome) {
                    $to_return .= "<img src='{$_singleMedia['image']}:thumb' >";
                } else {
                    $to_return .= "<img src='{$_singleMedia['image']}:thumb' class='tweet_img_spalla'>";
                }
                $_counter++;
            }
            $to_return .= "</a>";
            $to_return .= "</div>";
            $to_return .= "</div>";
            $to_return .= "</div>";
            return $to_return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function _youtube($videoId = false, $isHome = false) {
        $translateHelper = new Zend_View_Helper_Translate();
        $width = "100%";
        $height = ($isHome) ? "335px" : "235px";
        $to_return = ($isHome) ? "" : "<div class='title_widget'><h3> " . $translateHelper->translate("Canale Youtube") . " </h3></div>";
        $_vId = ($videoId) ? $videoId : "L1VguBvjWpE";
        $to_return .= '<iframe width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/' . $_vId . '?modestbranding=1&autohide=1&fs=0" frameborder="0" wmode="transparent" allowfullscreen></iframe>';
        return $to_return;
    }

    private static function _calendar() {
        try {
            $calendar = new Thinkopen_Calendar();
            $return = $calendar->getCalendarFrontend(date("n"), date("Y"));
            return $return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function _calendar_spalla() {
        try {
            $translateHelper = new Zend_View_Helper_Translate();
            $calendar = new Thinkopen_Calendar();
            $return = "<div class='title_widget'><h3> " . $translateHelper->translate("Calendario") . " </h3></div>";
            $return .= "<div class='scrollcalendario'></div>";
            $return .= "<div class='row calendar calendarBox' >";
            $return .= $calendar->getCalendarFrontend(date("n"), date("Y"));
            $return .= "</div>";
            $return .= "<div class='row calendarEventInfo calendar_spalla_info'>";
            $return .= "</div>";
            return $return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function _news() {
        try {
            $helperImage = new Zend_View_Helper_Image();
            $_lastNews = Top::getModel("mapper/news")->getLatestNews(10);
            $to_return = "<ul class='list-unstyled news_list'>";
            foreach ($_lastNews as $news) {
                $to_return .= "<li class ='news_list_li'>";
                $to_return .= "<a href='" . Top::getBaseUrl() . "news/" . $news->getIdentifier() . ".html'>";
                $to_return .= "<div class='row'>";
                $to_return .= "<div class='col-xs-1 col-sm-3 col-md-3 col-lg-3 '>";
                $to_return .= "<div class='news_list_image'><img class='hidden_img' src='" . BASE_URL . "img/freccia_news.png'/><img  class='img-responsive' src='" . $helperImage->image($news->getImage(), array("width" => 200)) . "'/></div>";
                $to_return .= "</div>";
                $to_return .= "<div class='col-xs-11 col-sm-9 col-md-9 col-lg-9 zeroPaddingLeft news_text' >";
                $to_return .= "<div class='news_list_cooming'>" . $news->getContent() . "</div>";
                $to_return .= "</div>";
                $to_return .= "</div>";
                $to_return .= "</a>";
                $to_return .= "</li>";
            }
            $to_return .= "</ul>";
            return $to_return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function _news_spalla() {
        try {
            $helperImage = new Zend_View_Helper_Image();
            $_lastNews = Top::getModel("mapper/news")->getLatestNews(5);
            $to_return = "<div class='title_widget'><h3> News </h3></div>";
            $to_return .= "<ul class='list-unstyled news_list'>";
            foreach ($_lastNews as $news) {
                $to_return .= "<li class ='news_list_li'>";
                $to_return .= "<a href='" . Top::getBaseUrl() . "news/" . $news->getIdentifier() . ".html'>";
                $to_return .= "<div class='row'>";
                $to_return .= "<div class='col-xs-1 col-sm-3 col-md-3 col-lg-3 zeroPaddingLeft'>";
                $to_return .= "<div class='news_list_image'><img class='hidden_img' src='" . BASE_URL . "img/freccia_news.png'/><img class='img-responsive' src='" . $helperImage->image($news->getImage(), array("width" => 200)) . "'/></div>";
                $to_return .= "</div>";
                $to_return .= "<div class='col-xs-11 col-sm-9 col-md-9 col-lg-9 zeroPaddingLeft' >";
                $to_return .= "<div class='news_list_cooming'>" . $news->getContent() . "</div>";
                $to_return .= "</div>";
                $to_return .= "</div>";
                $to_return .= "</a>";
                $to_return .= "</li>";
            }
            $to_return .= "</ul>";
            return $to_return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function _socialLargeIcon() {
        $to_return = '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/faceIcon.png"  class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="https://twitter.com/teatrosancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/twitIcon.png" class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="http://instagram.com/teatrosancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/instagramIcon.png" class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/tripIcon.png" class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/youtubeIcon.png" class="icon_social">';
        $to_return .= '</a>';
        return $to_return;
    }

    private static function _socialSmallIcon() {
        $to_return = '<a href="https://it-it.facebook.com/teatrodisancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/facePiccola.png"  class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="https://twitter.com/teatrosancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/twitPiccola.png" class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="http://instagram.com/teatrosancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/instagramPiccola.png" class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/tripPiccola.png" class="icon_social">';
        $to_return .= '</a>';
        $to_return .= '<a href="http://www.youtube.com/user/teatrodisancarlo" target="_blank" >';
        $to_return .= '<img src="' . BASE_URL . 'img/toutubePiccola.png" class="icon_social">';
        $to_return .= '</a>';
        return $to_return;
    }

    public function widget($_widget, $ishome = false) {
        if (in_array("_" . $_widget, get_class_methods(get_class($this)))) {
            $_string = "_" . $_widget;
            if ($_widget == "youtube") {
                return self::$_string(false, $ishome);
            }
            if ($_widget == "twitter") {
                return self::$_string($ishome);
            }
            return self::$_string();
        } else {
            return "NOT FOUND";
        }
    }

    /**
     * La funzione ogfacebook
     * @param string $titolo        Il titolo della pagina, della news o dello spettacolo
     * @param string $url           l'url della pagina, della news o dello spettacolo
     * @return string $to_return    il titolo, l'url, il type, l'immagine e la descrizione dell'oggetto da inserire in fb
     */
    public function ogfacebook($titolo, $url, $desc, $img) {
        try {
            $to_return = "<meta property='og:title' content='" . $titolo . "' />";
            $to_return .= "<meta property='og:type' content='article' />";
            $to_return .= "<meta property='og:url' content='" . $url . "'/>";
            $to_return .= "<meta property='og:image' content='" . BASE_URL . $img . "'/>";
            $to_return .= "<meta property='og:image:width' content='400' />";
            $to_return .= "<meta property='og:image:height' content='300' />";
            $to_return .= "<meta property='og:description' content=' " . $desc . " ' />";
            return $to_return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function searchwidget($content) {
        $_match = $_natch = array();
        preg_match_all('/({{([^}]+)}})/', $content, $_match, PREG_OFFSET_CAPTURE);
        foreach ($_match[2] as $_keymatched => $_widgetType) {
            $_widgetName = $_widgetType[0];
            $_widgetOffset = stripos($content, $_match[0][$_keymatched][0]);
            if (stripos($_widgetName, "youtube") !== FALSE) {
                preg_match('/link=\"([^}]+)\"/', $_widgetName, $_natch);
                $_linkYoutube = (count($_natch) > 0) ? $_natch[1] : FALSE;
                $content = substr_replace($content, self::_youtube(self::getYoutubeIdFromUrl($_linkYoutube)), $_widgetOffset, strlen($_match[0][$_keymatched][0]));
            } elseif (stripos($_widgetName, "twitter") !== FALSE) {
                $content = substr_replace($content, self::_twitter(), $_widgetOffset, strlen($_match[0][$_keymatched][0]));
            } elseif (stripos($_widgetName, "w_news") !== FALSE) {
                $content = substr_replace($content, self::_news_spalla(), $_widgetOffset, strlen($_match[0][$_keymatched][0]));
            } elseif (stripos($_widgetName, "calendar") !== FALSE) {
                $content = substr_replace($content, self::_calendar_spalla(), $_widgetOffset, strlen($_match[0][$_keymatched][0]));
            }
        }
        return $content;
    }

    private static function getYoutubeIdFromUrl($url) {
        if ($url === FALSE) {
            return FALSE;
        }
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $qs);
            if (isset($qs['v'])) {
                return $qs['v'];
            } else if ($qs['vi']) {
                return $qs['vi'];
            }
        }
        if (isset($parts['path'])) {
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path) - 1];
        }
        return false;
    }

    public function shareSocial() {
        try {
            $to_return = "<div class='row'>";
            $to_return .="<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 backgroundSocialShare'>";
            $to_return .="<iframe src='//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.teatrosancarlo.it&amp;width=100&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=605888966088469' scrolling='no' frameborder='0' style='border:none; overflow:hidden; width:110px; height:20px;' allowTransparency='true'></iframe>";
            $to_return .="<iframe src='//www.facebook.com/plugins/share_button.php?href=http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']."&amp;layout=button_count&amp;appId=605888966088469' scrolling='no' frameborder='0' style='border:none; overflow:hidden;width:115px; height:20px;' allowTransparency='true'></iframe>";
            $to_return .=" <a href='https://twitter.com/share' class='twitter-share-button' >Tweet</a>";
            $to_return .="<a href='http://instagram.com/teatrosancarlo' target='_blank' >";
            $to_return .="<img src='". BASE_URL ."img/instagramlink.png' class='buttonLinkSocial '   > </a>";
            $to_return .="<a href='http://www.youtube.com/user/teatrodisancarlo' target='_blank' >";    
            $to_return .="<img src='". BASE_URL ."img/yuotubelink.png'class='buttonLinkSocial buttonLinkSocialMargin1'   ></a>";   
            $to_return .=" <a href='http://www.tripadvisor.it/Attraction_Review-g187785-d195418-Reviews-Teatro_di_San_Carlo-Naples_Province_of_Naples_Campania.html' target='_blank' >";           
            $to_return .=" <img src='". BASE_URL ."img/tripadvisorlink.png'class='buttonLinkSocial buttonLinkSocialMargin2'   ></a>";                 
            $to_return .="</div>";
            $to_return .="</div>";
            return $to_return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
