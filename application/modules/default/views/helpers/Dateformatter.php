<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dateformatter
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Zend_View_Helper_Dateformatter extends Zend_View_Helper_Abstract {
    public function dateformatter($_date, $_isFromSearch = false){
        $dateString = new Zend_Date(strtotime($_date), false, Top::getCurrentLanguage());
        if($_isFromSearch){
            return $dateString->addDay(1)->toString("dd MMMM yyyy");
        } else {
            return $dateString->toString("dd MMMM yyyy");
        }
    }
}
