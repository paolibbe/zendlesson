<?php

/*
 * classe form, seleziona i criteri di ricerca
 */

class Default_Form_Search extends Zend_Form
{
    public function __construct($options = null) {
        parent::__construct($options);
        
        $this->setName("ricerca_per_username_utenti");
        $user=new Zend_Form_Element_Text("username");
        $user->setLabel("ricerca per username");
        $this->addElement($user);
        $find=new Zend_Form_Element_Submit("find");
        $find->setLabel("trova");
        $this->addElement($find);
        
        
    }
}


?>