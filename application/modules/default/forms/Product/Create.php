<?php

/**
 * Classe per la creazione della form di creazione prodotto
 * 
 * 
 * @author Walter Fato <walter.fato@thinkopen.it>
 * 
 */
class Default_Form_Product_Create extends Zend_Form {

    public function init()
    {
        $this->setMethod('POST');
        $this->setAction(Top::getBaseUrl() . "default/product/create");
        $this->setName("createproduct");

        $this->addElement(
            'text', 'name', array(
            'label' => 'Nome:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'sku', array(
            'label' => 'SKU:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'url', array(
            'label' => 'Url prodotto:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'qty', array(
            'label' => 'Quantità:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'price', array(
            'label' => 'Price:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'from_date', array(
            'label' => 'Data da:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'to_date', array(
            'label' => 'Data a:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Create',
        ));
    }
}

?>