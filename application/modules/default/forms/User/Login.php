<?php

/**
 * Classe per la creazione della form di login
 * 
 * 
 * @author Walter Fato <walter.fato@thinkopen.it>
 * 
 */
class Default_Form_User_Login extends Zend_Form {

    public function init()
    {
        $this->setMethod('POST');
        $this->setAction(Top::getBaseUrl() . "default/user/login");

        $this->addElement(
            'text', 'username', array(
            'label' => 'Username:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Password:',
            'required' => true,
        ));

        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Login',
        ));

        $this->clearErrorMessages();
        $this->reset();

    }
}

?>