<?php

/**
 * Classe per la creazione della form di login
 * 
 * 
 * @author Walter Fato <walter.fato@thinkopen.it>
 * 
 */
class Default_Form_User_Create extends Zend_Form {

    public function init()
    {
        $this->setMethod('POST');
        $this->setAction(Top::getBaseUrl() . "default/user/create");
        $this->setName("createuser");

        $this->addElement(
            'text', 'username', array(
            'label' => 'Username:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'email', array(
            'label' => 'Email:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement(
            'text', 'dob', array(
            'label' => 'Data di nascita:',
            'required' => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Password:',
            'required' => true,
        ));

        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Create',
        ));
    }
}

?>