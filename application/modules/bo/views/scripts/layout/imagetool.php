<script src="<?= BASE_URL ?>jquery/jquery-1.9.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= BASE_URL ?>jquery/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="<?= BASE_URL ?>jquery/ui-themes/smoothness/jquery-ui-1.10.1.custom.css" type="text/css" media="screen" title="no title" charset="utf-8">
<link rel="stylesheet" href="<?= BASE_URL ?>css/common.css"      type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/dialog.css"      type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/toolbar.css"     type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/navbar.css"      type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/statusbar.css"   type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/contextmenu.css" type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/cwd.css"         type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/quicklook.css"   type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/commands.css"    type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/fonts.css"       type="text/css">
<link rel="stylesheet" href="<?= BASE_URL ?>css/theme.css"       type="text/css">

<!-- elfinder core -->
<script src="<?= BASE_URL ?>js/elFinder.js"></script>
<script src="<?= BASE_URL ?>js/elFinder.version.js"></script>
<script src="<?= BASE_URL ?>js/jquery.elfinder.js"></script>
<script src="<?= BASE_URL ?>js/elFinder.resources.js"></script>
<script src="<?= BASE_URL ?>js/elFinder.options.js"></script>
<script src="<?= BASE_URL ?>js/elFinder.history.js"></script>
<script src="<?= BASE_URL ?>js/elFinder.command.js"></script>

<!-- elfinder ui -->
<script src="<?= BASE_URL ?>js/ui/overlay.js"></script>
<script src="<?= BASE_URL ?>js/ui/workzone.js"></script>
<script src="<?= BASE_URL ?>js/ui/navbar.js"></script>
<script src="<?= BASE_URL ?>js/ui/dialog.js"></script>
<script src="<?= BASE_URL ?>js/ui/tree.js"></script>
<script src="<?= BASE_URL ?>js/ui/cwd.js"></script>
<script src="<?= BASE_URL ?>js/ui/toolbar.js"></script>
<script src="<?= BASE_URL ?>js/ui/button.js"></script>
<script src="<?= BASE_URL ?>js/ui/uploadButton.js"></script>
<script src="<?= BASE_URL ?>js/ui/viewbutton.js"></script>
<script src="<?= BASE_URL ?>js/ui/searchbutton.js"></script>
<script src="<?= BASE_URL ?>js/ui/sortbutton.js"></script>
<script src="<?= BASE_URL ?>js/ui/panel.js"></script>
<script src="<?= BASE_URL ?>js/ui/contextmenu.js"></script>
<script src="<?= BASE_URL ?>js/ui/path.js"></script>
<script src="<?= BASE_URL ?>js/ui/stat.js"></script>
<script src="<?= BASE_URL ?>js/ui/places.js"></script>

<!-- elfinder commands -->
<script src="<?= BASE_URL ?>js/commands/back.js"></script>
<script src="<?= BASE_URL ?>js/commands/forward.js"></script>
<script src="<?= BASE_URL ?>js/commands/reload.js"></script>
<script src="<?= BASE_URL ?>js/commands/up.js"></script>
<script src="<?= BASE_URL ?>js/commands/home.js"></script>
<script src="<?= BASE_URL ?>js/commands/copy.js"></script>
<script src="<?= BASE_URL ?>js/commands/cut.js"></script>
<script src="<?= BASE_URL ?>js/commands/paste.js"></script>
<script src="<?= BASE_URL ?>js/commands/open.js"></script>
<script src="<?= BASE_URL ?>js/commands/rm.js"></script>
<script src="<?= BASE_URL ?>js/commands/info.js"></script>
<script src="<?= BASE_URL ?>js/commands/duplicate.js"></script>
<script src="<?= BASE_URL ?>js/commands/rename.js"></script>
<script src="<?= BASE_URL ?>js/commands/help.js"></script>
<script src="<?= BASE_URL ?>js/commands/getfile.js"></script>
<script src="<?= BASE_URL ?>js/commands/mkdir.js"></script>
<script src="<?= BASE_URL ?>js/commands/mkfile.js"></script>
<script src="<?= BASE_URL ?>js/commands/upload.js"></script>
<script src="<?= BASE_URL ?>js/commands/download.js"></script>
<script src="<?= BASE_URL ?>js/commands/edit.js"></script>
<script src="<?= BASE_URL ?>js/commands/quicklook.js"></script>
<script src="<?= BASE_URL ?>js/commands/quicklook.plugins.js"></script>
<script src="<?= BASE_URL ?>js/commands/extract.js"></script>
<script src="<?= BASE_URL ?>js/commands/archive.js"></script>
<script src="<?= BASE_URL ?>js/commands/search.js"></script>
<script src="<?= BASE_URL ?>js/commands/view.js"></script>
<script src="<?= BASE_URL ?>js/commands/resize.js"></script>
<script src="<?= BASE_URL ?>js/commands/sort.js"></script>	
<script src="<?= BASE_URL ?>js/commands/netmount.js"></script>	

<!-- elfinder languages -->
<script src="<?= BASE_URL ?>js/i18n/elfinder.it.js"></script>

<!-- elfinder 1.x connector API support -->
<script src="<?= BASE_URL ?>js/proxy/elFinderSupportVer1.js"></script>




<script>
    $(document).ready(function () {
        $('.imageputter').click(function (event) {
            event.preventDefault();
            var idTarget = $(this).data("idtarget");
            $('#elfinder').elfinder({
                url: "<?= Top::getBaseUrl() . "page/connector" ?>",
                sync: 20000,
                lang: 'it',
                width: 700,
                requestType: 'POST',
                commandsOptions: {
                    getfile: {
                        oncomplete: 'destroy'
                    }
                },
                getFileCallback: function (url) {
                    $("#" + idTarget).val(url.path);
                }
            }).elfinder('instance');
        });
    });
</script>

<div id="contenitor_elfinder">
<div  style = "position: fixed; z-index: 10000;" id = 'elfinder' > </div>
</div>