<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Default_View_Helper_Url
 *
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 */
class Zend_View_Helper_Url extends Zend_View_Helper_Abstract {

    private function _isStar($_string) {
        return ($_string === "*") ? NULL : $_string;
    }

    public function url($_path) {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $_pathExploded = explode("/", $_path);
        switch (count($_pathExploded)) {
            case 1:
                $urlOptions['action'] = $this->_isStar($_pathExploded[0]);
                break;
            case 2:
                $urlOptions['controller'] = $this->_isStar($_pathExploded[0]);
                $urlOptions['action'] = $this->_isStar($_pathExploded[1]);
                break;
            case 3:
                $urlOptions['module'] = $this->_isStar($_pathExploded[0]);
                $urlOptions['controller'] = $this->_isStar($_pathExploded[1]);
                $urlOptions['action'] = $this->_isStar($_pathExploded[2]);
                break;
            default:
                return $router->assemble(array(), NULL, false, true);
        }
        return $router->assemble($urlOptions, null, false, true);
    }

}
