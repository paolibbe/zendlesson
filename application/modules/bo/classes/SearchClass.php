<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SearchClass
 *
 * @author utente
 */
class SearchClass {

    private $dbVideo;

    public function __construct() {
        $this->dbVideo = new DbVideos();
    }

    public function reindexVideoData() {
        $_test = $this->dbVideo
                ->select()
                ->query()
                ->fetchAll();
        $index = Zend_Search_Lucene::create("../video");
        foreach ($_test as $_video) {
            $document = $this->createVideoDocument($_video);
            $index->addDocument($document);
        }
        $index->commit();
    }

    /**
     * Funzione che restituisce il timestamp di una data
     * @param type $timeZone Timezone locale
     * @param type $data se non settatto restituisce la data attuale in versione timestamp
     * @return type valore numerico timestamp
     */
    private function getTimestamp($timeZone, $data = false) {
        $_tempData = NULL;
        if ($data !== false) {
            $_tempData = new DateTime($data, new DateTimeZone($timeZone));
        } else {
            $_tempData = new DateTime("Now", new DateTimeZone($timeZone));
        }
        return $_tempData->getTimestamp();
    }

    /**
     * Metodo che restituisce il timestamp in base a un valore passato al parametro value
     * @param type $_value riceve un valore compreso tra 1 e 5
     * @param type $_TimeZone timezone utilizzato es. ('Berlin/Rome')
     * @return type integer (timestamp)
     */
    private function getTimeToLuceneSearch($_value, $_TimeZone) {
        $_risTimeStamp = null;
        $_risTempDate = null;
        switch ($_value) {
            case 1:
                // Video del giorno
                $_risTempDate = new DateTime("Now", new DateTimeZone($_TimeZone));
                $_risTimeStamp = $_risTempDate->getTimestamp();
                break;
            case 2:
                // Video dell'ultima settimana
                $_risTempDate = new DateTime(date('Y-m-d H:i:s', strtotime('-1 week')), new DateTimeZone($_TimeZone));
                $_risTimeStamp = $_risTempDate->getTimestamp();
                break;
            case 3:
                // Video dell'ultimo mese
                $_risTempDate = new DateTime(date('Y-m-d H:i:s', strtotime('-1 months')), new DateTimeZone($_TimeZone));
                $_risTimeStamp = $_risTempDate->getTimestamp();
                break;
            case 4:
                // Video degli ultimi tre mesi
                $_risTempDate = new DateTime(date('Y-m-d H:i:s', strtotime('-3 months')), new DateTimeZone($_TimeZone));
                $_risTimeStamp = $_risTempDate->getTimestamp();
                break;
            case 5:
                // Video degli ultimi sei mesi
                $_risTempDate = new DateTime(date('Y-m-d H:i:s', strtotime('-6 months')), new DateTimeZone($_TimeZone));
                $_risTimeStamp = $_risTempDate->getTimestamp();
                break;
            default:
                $_risTempDate = 0;
                break;
        }
        return $_risTimeStamp;
    }

    private function createVideoDocument($video) {
        $titolo = $video['titolo'];
        $descrizione = $video['description'];
        $tag = $video['tag'];
        $id_video = $video['id_video'];
        $immagine = $video['link_img_video'];
        $id_youtube = $video['id_video_link'];
        $nLike = $video['tot_like'];
        $nShare = $video['tot_share'];
        $nVisualizzazioni = $video['num_visualizzazioni'];
        $length = gmdate("i:s", $video['length']);
        $source_date = $video['source_video_id'];
        $autore = $video['autore'];
        $temp_time = new DateTime($video['update_time'], new DateTimeZone('Europe/Rome'));
        $updated_time = $temp_time->getTimestamp();

        $document = new Zend_Search_Lucene_Document();
        $document->addField(Zend_Search_Lucene_Field::text("titolo", $titolo));
        $document->addField(Zend_Search_Lucene_Field::unStored("descrizione", $descrizione));
        $document->addField(Zend_Search_Lucene_Field::text("tag", $tag));
//        $document->addField(Zend_Search_Lucene_Field::text("categoria", $this->setCategoriaByVideo($id_video)));
        $document->addField(Zend_Search_Lucene_Field::text("autore", $autore));
        $document->addField(Zend_Search_Lucene_Field::keyword("time", strtolower($updated_time)));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("id_video", $id_video));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("immagine", $immagine));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("id_youtube", $id_youtube));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("like", number_format($nLike, 0, '', '.')));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("visualizzazioni", number_format($nVisualizzazioni, 0, '', '.')));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("share", number_format($nShare, 0, '', '.')));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("durata", $length));
        $document->addField(Zend_Search_Lucene_Field::unIndexed("fonte", $source_date));
        return $document;
    }

    public function searchForAutocomplete($_parameter) {
        $index = Zend_Search_Lucene::open("../video");
        $hits = $index->find($_parameter);
        $_arrayDaTornare = array();
        foreach ($hits as $i => $hit) {
            array_push($_arrayDaTornare, $hit->titolo);
        }
        return $_arrayDaTornare;
    }

    private function sonoMostro($str) {
        $str = preg_replace("/[^a-zA-Z0-9]/", ' ', $str);
        $str2 = explode(" ", $str);
        $str_completa = "";
        foreach ($str2 as $value) {
            if (!empty($value))
                $str_completa .= trim($value) . " ";
        }
        return strtolower($str_completa);
//        echo "Stringa completa: " . $str_completa;
    }

    public function readVideoIndex($_parameter, $_autoComplete = false, $_categoria = false, $_tempo = false, $_hiddenField = false, $order = false) {
        try {
//            print_r(array($_categoria, $_tempo));
//            die();
            echo "<h1> autocomplete: ".$_autoComplete."</h1>";
            $_parameter = strtolower($_parameter);
            $index = Zend_Search_Lucene::open("../video");
//            if ($_autoComplete === true){
//                $_parameter = $this->sonoMostro($_parameter);
//                
//            }
//            if ($_categoria === false || $_tempo === false) {
////            if ($_categoria !== false || $_tempo !== false) {
////                $_parameter = $_parameter . "~";
//                $_parameter = "".$_parameter . "";
//            }
//            if ($_categoria !== false) {
//                $_categoryParameter = " +categoria: " . $_categoria." ";
//            }

            $queryFinale = new Zend_Search_Lucene_Search_Query_MultiTerm();
            if ($_hiddenField === "OK")
                $queryGrossa = new Zend_Search_Lucene_Search_Query_Boolean();

            /**
             * Primo blocco ricerca solo testuale senza menu avanzato, else controlli con campi avanzati
             */
            if ($_hiddenField === "KO" || $_autoComplete === true) {
                if (strlen($_parameter) > 3) {
                    $_terms = explode(" ", $_parameter);
                    foreach ($_terms as $_termine) {
                        $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term($_termine), TRUE);
                    }
                }
//                echo "<h2>NON VUOTI: STRINGA RICERCA - RICERCA AVANZATA DISABILITATA</h2>";
            } else {
                /**
                 * Ricerca con stringa con ricerca avanzata abilitata
                 */
                if (strlen($_parameter) > 3 && empty($_categoria) && empty($_tempo)) {
                    $_terms = explode(" ", $_parameter);
                    foreach ($_terms as $_termine) {
                        $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term($_termine), true);
                    }
//                    echo "<h2>NON VUOTI: STRINGA RICERCA - VUOTI CATEGORIA E TEMPO</h2>";
                }
                /**
                 * Ricerca con stringa e categoria
                 */
                if (strlen($_parameter) > 3 && !empty($_categoria) && empty($_tempo)) {
                    $_terms = explode(" ", $_parameter);
                    foreach ($_terms as $_termine) {
                        $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term($_termine), true);
                    }
                    $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($_categoria), TRUE));
//                    echo "<h2>NON VUOTI: TESTO RICERCA E CATEGORIA, VUOTO TEMPO</h2>";
                }
                /**
                 * Se spuntata solo la categoria
                 */
                if (!empty($_categoria) && empty($_tempo) && strlen($_parameter) < 3) {
                    $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($_categoria), TRUE));
//                    echo "<h2>NON VUOTI: CATEGORIA</h2>";
                }
                /**
                 * Se spuntata solo il range di tempo
                 */
                if (!empty($_tempo) && empty($_categoria) && strlen($_parameter) < 3) {
                    $timeTo = $this->getTimeToLuceneSearch(1, "Europe/Rome");
                    $timeFrom = $this->getTimeToLuceneSearch($_tempo, "Europe/Rome");
                    $to = new Zend_Search_Lucene_Index_Term(date("Ymd", $timeTo), 'time');
                    $from = new Zend_Search_Lucene_Index_Term(date("Ymd", $timeFrom), 'time');
                    $tendenza = new Zend_Search_Lucene_Search_Query_Range($from, $to, true);
//                    $queryGrossa = new Zend_Search_Lucene_Search_Query_Boolean();
//                    $queryGrossa->addSubquery($queryFinale, true);
                    $queryGrossa->addSubquery($tendenza, true);
//                    echo "<h2>NON VUOTI: TEMPO</h2>";
                }
                /**
                 * Ricerca per categoria e tempo
                 */
                if (!empty($_categoria) && !empty($_tempo) && strlen($_parameter) === 0) {
                    $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($_categoria), TRUE));
                    $timeTo = $this->getTimeToLuceneSearch(1, "Europe/Rome");
                    $timeFrom = $this->getTimeToLuceneSearch($_tempo, "Europe/Rome");
                    $to = new Zend_Search_Lucene_Index_Term(date("Ymd", $timeTo), 'time');
                    $from = new Zend_Search_Lucene_Index_Term(date("Ymd", $timeFrom), 'time');
                    $tendenza = new Zend_Search_Lucene_Search_Query_Range($from, $to, true);
//                    $queryGrossa = new Zend_Search_Lqene_Search_Query_Boolean();
                    $queryGrossa->addSubquery($queryFinale, true);
                    $queryGrossa->addSubquery($tendenza, true);
//                    echo "<h2>NON VUOTI: CATEGORIA, TEMPO - VUOTA: STRINGA RICERCA</h2>";
                }
                /**
                 * Ricerca su tutti e tre i campi TEMPO, CATEGORIA e TESTO
                 */
                if (!empty($_categoria) && !empty($_tempo) && strlen($_parameter) > 3) {
                    $_terms = explode(" ", $_parameter);
                    foreach ($_terms as $_termine) {
                        $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term($_termine), TRUE);
                    }
                    $queryFinale->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($_categoria), TRUE));
                    $timeTo = $this->getTimeToLuceneSearch(1, "Europe/Rome");
                    $timeFrom = $this->getTimeToLuceneSearch($_tempo, "Europe/Rome");
                    $to = new Zend_Search_Lucene_Index_Term(date("Ymd", $timeTo), 'time');
                    $from = new Zend_Search_Lucene_Index_Term(date("Ymd", $timeFrom), 'time');
                    $tendenza = new Zend_Search_Lucene_Search_Query_Range($from, $to, TRUE);
//                    $queryGrossa = new Zend_Search_Lqene_Search_Query_Boolean();
                    $queryGrossa->addSubquery($queryFinale, TRUE);
                    $queryGrossa->addSubquery($tendenza, TRUE);
                }
            }

            $index->setResultSetLimit(5000);
            if (!empty($_tempo) && $_hiddenField === "OK") {
//                echo "<pre>";
//                var_dump($queryGrossa);
//                echo "</pre>";
                //WALTER
                if ($order == false)
                    $hits = $index->find($queryGrossa);
                else
                    $hits = $index->find($queryGrossa, $order, SORT_NUMERIC, SORT_DESC);
//                echo "<h2>Query Grossa</h2>";
            } else {
//                echo "<pre>";
//                var_dump($queryFinale);
//                echo "</pre>";
                //WALTER
                if ($order == false)
                    $hits = $index->find($queryFinale);
                else
                    $hits = $index->find($queryFinale, $order, SORT_NUMERIC, SORT_DESC);
//                echo "<h2>Query Finale</h2>";
            }

            // Convert to stdClass to allow caching
            $resultsArray = array();
            foreach ($hits as $i => $hit) {
                $resultsArray[$i] = new stdClass();
                $doc = $hit->getDocument();
                foreach ($doc->getFieldNames() as $field) {
                    $resultsArray[$i]->{$field} = $hit->{$field};
                }
                $resultsArray[$i]->score = $hit->score;
            }

//            var_dump($hits);
            $_arrayDaTornare['resultSet'] = $resultsArray;
            $_arrayDaTornare['totalHits'] = count($hits);
//            var_dump($resultsArray);
//            echo "<br>" . $_parameter;
//            print_r($_arrayDaTornare);
//            
//            die();
            return $_arrayDaTornare;
        } catch (Exception $e) {
            echo $e->getTraceAsString();
            die($_parameter);
        }
    }

    public function getSelectObject() {
        return $this->dbVideo
                        ->select()
                        ->where("id_video < ?", 33);
    }

}

?>
