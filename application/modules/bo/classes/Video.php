<?php

/**
 * Description of Video
 * @author Pasquale Carucci <pasquale.carucci@thinkopen.it>
 */
class Video {

    private $_dbVideo;
    private $_video;
    private $_bloccoHP = array();
    public $_object = true;

    public function __construct($_idVideo = false) {
        $this->_dbVideo = new DbVideos();
        if ($_idVideo !== false) {
            $this->retrieveVideoData($_idVideo);
        }
    }

    public function __get($_name) {
        return $this->_video->$_name;
    }

    /**
     * Funzione per la load del Video
     * @param type $_idVideo
     * @return boolean|\Video
     */
    public function load($_idVideo) {
        if (!isset($_idVideo)) {
            return false;
        } else {
            $this->retrieveVideoData($_idVideo);
            return $this;
        }
    }

    private function retrieveVideoData($_idVideo) {
        $this->_video = new VideoSkeleton($_idVideo);
        if ($this->_video === false) {
            $this->_object = false;
        }
    }

    public function getAllInfoVideo() {
        return $this->_video->exposeVars();
    }

    public static function switchSource($src_video) {
        $toResult = '';
        switch ($src_video) {
            case 0:
                $toResult = 'Youtube';
                break;
            case 1:
                $toResult = 'Facebook';
                break;
        }
        return $toResult;
    }

    public static function getSourceVideoStatic($src_video) {
        if (is_array($src_video)) {
            $src_video['source_video_id'] = self::switchSource($src_video['source_video_id']);
        } else {
            $src_video = self::switchSource($src_video);
        }
        return $src_video;
    }

    public function getMostViewed($_pageNumber = 1, $_limit = 30, $_numPerPage = 15) {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('controlsInfinityMV.phtml');
        $_selectObject = VideoSkeleton::mostMethod("n_views");
        $paginator = Zend_Paginator::factory($_selectObject);
        $paginator->setItemCountPerPage($_numPerPage);
        $paginator->setCurrentPageNumber($_pageNumber);
        return $paginator;
    }

    public function getMostLiked($_pageNumber = 1, $_limit = 30, $_numPerPage = 15) {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('controlsInfinityMV.phtml');
        $_selectObject = VideoSkeleton::mostMethod("n_likes");
        $paginator = Zend_Paginator::factory($_selectObject);
        $paginator->setItemCountPerPage($_numPerPage);
        $paginator->setCurrentPageNumber($_pageNumber);
        return $paginator;
    }

    public function getMostShare($_pageNumber = 1, $_limit = 30, $_numPerPage = 15) {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('controlsInfinityMV.phtml');
        $_selectObject = VideoSkeleton::mostMethod("n_shares");
        $paginator = Zend_Paginator::factory($_selectObject);
        $paginator->setItemCountPerPage($_numPerPage);
        $paginator->setCurrentPageNumber($_pageNumber);
        return $paginator;
    }

    public function getAccessVideoSlider($type = 'n_likes') {
        $_skeletonModel = new VideoSkeleton();
        return $_skeletonModel->getResults(15, $type, "DESC");
    }

    /**
     * Restituisce una array contenente i video correlati a quello che stai guardando
     * scegliendone 16 dai tags e 15 dalle categorie.
     * @author walter fato <walter.fato@thinkopen.it>
     * @param string $tags
     * @param int $category
     * @return array
     */
    public function getVideoCorrelati($tags, $category, $idvideo) {
        $db = new DbUserViewVideo();
        $lenght = strlen($tags);
        $tags = explode(",", $tags);
        $arraySupport = array();

        for ($i = 0; $i < 4; $i++) {
            $query = "SELECT distinct entity_id FROM vlk_video_entity_text WHERE (attribute_id = 6 AND value like '%" . $tags[$i] . ",%' AND entity_id != " . $idvideo . ") ORDER BY RAND() LIMIT 4";
            $asd = $db->getAdapter()->query($query)->fetchAll();
            $arraySupport = array_merge($arraySupport, $asd);
        }

        $query = "SELECT distinct entity_id FROM vlk_video_entity_int WHERE (attribute_id = 13 AND value = " . $category . " AND entity_id != " . $idvideo . ") ORDER BY RAND() LIMIT 15";
        $asd = $db->getAdapter()->query($query)->fetchAll();
        $arraySupport = array_merge($arraySupport, $asd);

        return $arraySupport;
    }

    /**
     * Gestisce l'aggiunta di un interesse nella tabella vlk_user_influence.
     * Dopo aver controllato l'esistenza dell'interesse o meno:
     *       A)Inserisce il nuovo interesse dell'utente
     *       B)Esegue l'update per incrementare il peso dell'interesse.
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param int $category
     * @param string $tags
     * @return boolean
     */
    public function gestisciInterresse($tags, $category) {
        $db = new DbVideoSuggeriti();
        $taggo = explode(",", $tags);
        if (NormalUser::getUserId()) {

            $query = "SELECT id_category FROM  vlk_user_influences WHERE (id_user=" . NormalUser::getUserId() . ") AND id_category=" . $category;
            $res = $db->getAdapter()->query($query)->rowCount();
            if ($res < 1) {
                $qInsertdue = array("id_user" => NormalUser::getUserId(), "id_category" => $category, "peso" => 1);
                $db->insert($qInsertdue);
                $return = true;
            } else {
                $query = "UPDATE vlk_user_influences SET peso = peso + 1 WHERE (id_user=" . NormalUser::getUserId() . " AND id_category=" . $category . "  ) ";
                $db->getAdapter()->query($query);
                $return = true;
            }


            $querydue = "SELECT tag FROM  vlk_user_influences WHERE (id_user=" . NormalUser::getUserId() . ") AND tag='" . $taggo[0] . "'";
            $resdue = $db->getAdapter()->query($querydue)->rowCount();
            if ($resdue < 1) {
                if (!empty($taggo[0])) {
                    $qInsert = array("id_user" => NormalUser::getUserId(), "tag" => $taggo[0], "peso" => 1);
                    $db->insert($qInsert);
                    $return = true;
                }
            } else {
                $querydue = "UPDATE vlk_user_influences SET peso = peso + 1 WHERE (id_user=" . NormalUser::getUserId() . " AND  tag='" . $taggo[0] . "' ) ";
                $db->getAdapter()->query($querydue);
                $return = true;
            }
        }
        else
            $return = false;

        return $return;
    }

    /**
     * Restituisce gli interessi dell'utente se è loggato altrimenti diversamente. I due casi sono:
     *      A)Restituisce gli interessi più pesati di un utente se loggato
     *      B) Restituisce i 30 video piu popolari
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @return array entity_id di video
     */
    public function getInteressi() {
        $db = new DbVideoSuggeriti();
        $arraySupport = array();
        $_categorieInteressanti = array();
        $_tagsInteressanti = array();
        $_stringSearch = "";
        if (NormalUser::getUserId()) {
            $interessi = $db->select()->from("vlk_user_influences", array("id_category", "tag"))->where("id_user = ?", NormalUser::getUserId())->order("peso DESC")->limit(5)->query()->fetchAll();
            if (count($interessi) > 0) {
                foreach ($interessi as $inter) {
                    if ($inter['id_category'] != null) {
                        array_push($_categorieInteressanti, $inter['id_category']);
                    } else {
                        array_push($_tagsInteressanti, $inter['tag']);
                    }
                }
                $_notEmptyCategorie = empty($_categorieInteressanti);
                $_notEmptyTags = empty($_tagsInteressanti);
                if (!$_notEmptyCategorie) {
                    $_stringSearch = "categoria: (\"" . implode("\" \"", $_categorieInteressanti) . "\")";
                    if (!$_notEmptyTags) {
                        $_stringSearch .= " OR ";
                    }
                }
                if(!$_notEmptyTags) {
                    $_stringSearch = "tag: (\"" . implode("\" \"", $_tagsInteressanti) . "\")";
                }
                $_resultSet = $this->doSearchRequest($_stringSearch);
                print_r($_resultSet['resultSet']);
                die();
//                    if ($inter['id_category'] != null) {
//                        $query = "SELECT entity_id FROM vlk_video_entity_int WHERE (attribute_id = 13 AND value = " . $inter['id_category'] . ") ORDER BY RAND() LIMIT 6";
//                        $asddue = $db->getAdapter()->query($query)->fetchAll();
//                        $arraySupport = array_merge($arraySupport, $asddue);
//                    }
//                    if ($inter['tag'] != null) {
//                        $querydue = "SELECT entity_id FROM vlk_video_entity_text WHERE (attribute_id = 6 AND value like '%" . $inter['tag'] . ",%') ORDER BY RAND() LIMIT 10";
//                        $asd = $db->getAdapter()->query($querydue)->fetchAll();
//                        $arraySupport = array_merge($arraySupport, $asd);
//                    }
//                }
            } else {
                $arraySupport = $this->randomSuggeirti();
            }
        } else {
            $arraySupport = $this->randomSuggeirti();
        }
        return $arraySupport;
    }
    
    private function doSearchRequest($_stringSearch){
        $_parametri = array(
            'text' => $_stringSearch,
            'maxResults' => 40
        );
        $options = array(
            'soap_version' => SOAP_1_1,
            'cache_wsdl' => false
        );
        $client = new Zend_Soap_Client("http://localhost:8080/VideolikedWSSearch/services/Search?wsdl", $options);
        $_resultSet = $client->searchForDocument($_parametri);

        $_arrayDaTornare['resultSet'] = json_decode($_resultSet->searchForDocumentReturn);
        $_arrayDaTornare['totalHits'] = count($_arrayDaTornare['resultSet']);
        return $_arrayDaTornare;
    }

    /**
     * Restituisce i 30 video piu popolari
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @return array entity_id di video
     */
    private function randomSuggeirti() {
        $db = new DbVideoSuggeriti();
        $arraySupport = array();
        $interessi = $db->select()->from("vlk_user_influences", array("id_category", "tag"))->order("peso DESC")->limit(10)->query()->fetchAll();
        foreach ($interessi as $inter) {
            if ($inter['id_category'] != null) {
                $query = "SELECT entity_id FROM vlk_video_entity_int WHERE (attribute_id = 13 AND value = " . $inter['id_category'] . ") ORDER BY RAND() LIMIT 10";
                $asddue = $db->getAdapter()->query($query)->fetchAll();
                $arraySupport = array_merge($arraySupport, $asddue);
            }
            if ($inter['tag'] != null) {
                $querydue = "SELECT entity_id FROM vlk_video_entity_text WHERE (attribute_id = 6 AND value like '%" . $inter['tag'] . ",%') ORDER BY RAND() LIMIT 10";
                $asd = $db->getAdapter()->query($querydue)->fetchAll();
                $arraySupport = array_merge($arraySupport, $asd);
            }
        }
        return $arraySupport;
    }

    /**
     * Funzioni estrazione 12 record liked,share,mosted
     */
    private function getLiked($_arrayNotIn = false) {
        $_skeletonModel = new VideoSkeleton();
        $_resultSet = $_skeletonModel->getResults(12, "n_likes", "DESC", $_arrayNotIn);
        $this->extractIdsFromSkeletonResultSet($_resultSet);
        return $_resultSet;
    }

    private function getShared($_arrayNotIn = false) {
        $_skeletonModel = new VideoSkeleton();
        $_resultSet = $_skeletonModel->getResults(12, "n_shares", "ASC", $_arrayNotIn);
        $this->extractIdsFromSkeletonResultSet($_resultSet);
        return $_resultSet;
    }

    private function getViewed($_arrayNotIn = false) {
        $_skeletonModel = new VideoSkeleton();
        $_resultSet = $_skeletonModel->getResults(12, "n_views", "DESC", $_arrayNotIn);
        $this->extractIdsFromSkeletonResultSet($_resultSet);
        return $_resultSet;
    }

    private function extractIdsFromSkeletonResultSet($_resultSet) {
        foreach ($_resultSet as $_video) {
            array_push($this->_bloccoHP, $_video->id_video);
        }
    }

    public function getVideoIndexPage() {
        $_mostLiked = $this->getLiked();
        $_mostShared = $this->getShared($this->_bloccoHP);
        $_mostViewed = $this->getViewed($this->_bloccoHP);
        $resTotal = array_merge($_mostLiked, $_mostShared, $_mostViewed);
        shuffle($resTotal);
        return $resTotal;
    }

    /**
     * @todo Migrazione verso il nuovo database
     */
    public function getTagCloud() {
        $_resTag = preg_split("/[,]/", $this->_video->tags);
        return $_resTag;
    }

    /**
     * @todo Migrazione verso il nuovo database
     * @param type $idVideo
     */
    public function getTagCloudType($typeOrder) {

        $_skeletonModel = new VideoSkeleton();
        $_tagsArray = array();
        $_resultSet = $_skeletonModel->getResults(3, $typeOrder, "DESC");
        foreach ($_resultSet as $_video) {
            $tags = $_video->tags;
//            echo "uhahuaahuuha" . $tags . "<br>";
//            if (preg_match("/[^a-zA-Z0-9\s\p{P}]/", $tags) === false) {
            $_tagsArray = array_merge($_tagsArray, explode(", ", $tags));
//            }
        }
        return $_tagsArray;
    }

}