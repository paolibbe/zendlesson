<?php

/**
 * Classe per la creazione della form di ricerca nell'head
 * 
 * 
 * @author Walter Fato <walter.fato@thinkopen.it>
 * 
 */
class SearchFormMini extends Zend_Form {

    public function __construct($options = null, $_datiInput = null) {
        parent::__construct($options);
        $this->setName("searchFormMini");
        $this->setMethod("POST");
        $this->setAction(URL . 'search/search');
        $decorator = array(
            'ViewHelper',
            array('Errors', array('style' => 'font-style: italic; font-weight:bold; color: red; ', 'escape' => false, 'placement' => 'append')),
            array(array('containerDiv' => 'HtmlTag'), array('tag' => 'div')),
            array('Label', array('tag' => 'div', 'escape' => false, 'style' => array('float:left;'))),
        );
        $decoratorFieldRadio = array();
        $test = array(
            'ViewHelper', 
            array('Errors', array('escape' => false, 'placement' => 'append')),
            array(array('containerP' => 'HtmlTag'), array('tag' => 'p')),
            array('Label', array('tag' => 'p', 'escape' => false)),            
        );

        $Category = new Category();
        $arrayCategory = $Category->getLabalNomeCategoria(true);
        $arrayDateTime = array(1 => "Oggi", 2 => "Ultima settimana", 3 => "Ultimo mese", 4 => "Ultimi tre mesi", 5 => "Ultimi sei mesi");
        $_fromAutocomplete = new Zend_Form_Element_Hidden("fromAutocomplete");
        $_fromAutocomplete->setValue("NO");

        $_text = new Zend_Form_Element_Text("searchMiniInput");
//        $_text->setLabel("Cerca")->setRequired(true);
        $_find = new Zend_Form_Element_Image("searchMiniSubmit");
        $_find->setImage(IMG_URL."icon_lente20x20.png");        
        
        $_fieldCategory = new Zend_Form_Element_Select("raffina_category");
        $_fieldDateTime = new Zend_Form_Element_Radio("raffina_datetime");
        $_fieldCategory->addMultiOptions($arrayCategory);
        $_fieldDateTime->addMultiOptions($arrayDateTime);
//        $_fieldDateTime->setDecorators($test);
        $_fieldHiddenSearch = new Zend_Form_Element_Hidden("checkRicAvanz");
        $_fieldHiddenSearch->setName("checkRicAvanz");
        $_fieldHiddenSearch->setValue("KO");
        
        //WALTER
        $_hiddenFilter = new Zend_Form_Element_Hidden("orderFilter");
        $_hiddenFilter->setName("orderFilter");
        $_hiddenFilter->setValue("");
        

        $_elements = array(
            $_fromAutocomplete,
            $_text,
            $_find,
            $_fieldCategory,
            $_fieldDateTime,
            $_fieldHiddenSearch,
                $_hiddenFilter
        );

        $this->addElements($_elements);


        if (!is_null($_datiInput)) {
            $this->populate($_datiInput);
        }
    }

}

?>