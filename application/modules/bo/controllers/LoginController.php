<?php

class Bo_LoginController extends Thinkopen_Bocontroller {

    public function indexAction() {
        
    }

    public function loginAction() {
        if ($this->getParam("login", false) && $this->getParam("password", false)) {
            $this->forward("dologin", NULL, NULL, array("login" => $this->getParam("login"), "password" => $this->getParam("password")));
        } else {
            $this->forward("index");
        }
    }

    public function dologinAction() {
        $_staticUser = Top::getSingleton("bo_mapper/user")->doLogin($this->getParam("login"), $this->getParam("password"));
        if ($_staticUser) {
            $this->redirect(Top::getBaseUrl(true), array("code" => 302));
        } else {
            $this->redirect(Top::getBaseUrl(true), array("code" => 302));
        }
    }

    public function logoutAction() {
        Top::getSingleton("bo_mapper/user")->doLogout();
        $this->redirect(Top::getBaseUrl(true), array("code" => 302));
    }

}
