<?php

class Bo_MenuController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_page = Top::getModel("bo_mapper/menu");
        $_fetchedAll = $_page->fetchAll(NULL, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_page->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_pageModel = Top::getModel("mapper/menu");
        $this->view->assign("attributes", $_pageModel->getAttributes());
        $this->view->codeResponse = $this->getParam("codeResponse");
        $this->view->messageResponse = $this->getParam("messageResponse");
    }

    public function updateAction() {
        switch ($this->getParam("menu_id", NULL)) {
            case false:
                $this->view->newMenu = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/menu")->load($this->getParam("menu_id"));
                $this->view->newMenu = false;
                $this->view->assign("data", $model);
                $this->view->xmlparsato = Thinkopen_Elementparser::parseToUpdate($model->getStructure_xml());
                $this->view->language = $this->getParam("language");
                $this->view->codeResponse = $this->getParam("codeResponse");
                $this->view->messageResponse = $this->getParam("messageResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("page_id" => false));
    }

    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getParam("menu_id")) {
            $_modelMenu = Top::getModel("bo_mapper/menu")->load($this->getParam("menu_id"));
        } else {
            $_modelMenu = Top::getModel("bo_mapper/menu");
        }
        try {
            $name = ($this->getParam("name")) ? $this->getParam("name") : $this->getParam("name_clone");
            $lang = ($this->getParam("language")) ? $this->getParam("language") : $this->getParam("language_clone");
            if (is_array($this->getParam("element"))) {
                $_structureXml = Thinkopen_Elementparser::assemble($this->getParam("element"));
            } else {
                $_structureXml = "";
            }
            if ($this->getParam("menu_id")) {
                $isact = ($this->getParam("is_active", NULL) == "on") ? "1" : "0";
            } else {
                $isact = "0";
            }
            $_modelMenu->setLanguage($lang)
                    ->setName($name)
                    ->setIs_active($isact)
                    ->setBg_color($this->getParam("bg_color"))
                    ->setStructure_xml($_structureXml)
                    ->save();
            $this->_helper->redirector("list", "menu", "admin", array("codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("index", "menu", "admin", array("codeResponse" => 100, "messageResponse" => $e->getMessage()));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelMenu = Top::getModel("bo_mapper/menu")->load($this->getParam("menu_id"));
            $_modelMenu->delete();
            $this->_helper->redirector("index", "menu", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "menu", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

}
