<?php

/**
 * @author Paolo Libertini <paolo.libertini@thinkopen.it>
 * @acl: BO_ADMINISTRATOR
 */
class Bo_ShowattributesController extends Thinkopen_Bocontroller {

    public function indexAction() {
        try {
            $this->view->content = $this->getRequest()->getParam("error", NULL);
            $this->forward("list");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_page = Top::getModel("bo_mapper/showattributes");
        $_fetchedAll = $_page->fetchAll(NULL, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_page->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        try {
            $_pageModel = Top::getModel("bo_mapper/showattributes");
            $this->view->assign("attributes", $_pageModel->getAttributes());
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Salva le informazioni della pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (!$this->getParam("attribute_id", false)) {
            $_attribute = Top::getModel("bo_mapper/showattributes");
        } else {
            $_attribute = Top::getModel("bo_mapper/showattributes")->load($this->getParam("attribute_id"));
        }
        try {
            $_attribute->setAttribute_type($this->getParam("attribute_type"))
                    ->setAttribute_name($this->getParam("attribute_name"))
                    ->setAttribute_label($this->getParam("attribute_label"))
                    ->setIs_visible(($this->getParam("is_visible", 0) == "on") ? 1 : 0)
                    ->setIs_searchable(($this->getParam("is_searchable", 0) == "on") ? 1 : 0)
                    ->setIs_required(($this->getParam("is_required", 0) == "on") ? 1 : 0)
                    ->save();
            $this->_helper->redirector("update", "showattributes", "admin", array("attribute_id" => $_attribute->getAttribute_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "showattributes", "admin", array("attribute_id" => $_attribute->getAttribute_id(), "codeResponse" => 100));
        }
    }

    /**
     * Gestisce la creazione e la modifica di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function updateAction() {
        switch ($this->getParam("attribute_id", NULL)) {
            case false:
                $this->view->newPage = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/showattributes")->load($this->getParam("attribute_id"));
                $this->view->newPage = false;
                $this->view->assign("data", $model);
                $this->view->language = $this->getParam("language");
                $this->view->codeResponse = $this->getParam("codeResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("attribute_id" => false));
    }

}
