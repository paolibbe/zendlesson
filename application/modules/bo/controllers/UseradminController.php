<?php

class Bo_UseradminController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_userBoSingleton = Top::getSingleton("bo_mapper/user");
        $_userBo = $_userBoSingleton->load($_userBoSingleton::getUserId());
        $_page = Top::getModel("bo_mapper/user");
        $_userArray = array();
        $_fetchedAll = $_page->fetchAll(array("role >= ?" => $_userBo->getRole()), (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        foreach ($_fetchedAll as $_fetched) {
            array_push($_userArray, Top::getModel("bo_mapper/user")->load($_fetched['entity_id'])->getData());
        }
        echo json_encode(array("total" => $_page->countRows(), "rows" => (array) $_userArray));
    }

    public function listAction() {
        $_pageModel = Top::getModel("bo_mapper/user");
        $_attributesArray = $_pageModel->getAttributes(true);
        $this->view->assign("attributes", $_attributesArray);
        $this->view->assign("codeResponse", $this->getParam("codeResponse", NULL));
        $this->view->assign("messageResponse", $this->getParam("messageResponse", NULL));
    }

    /**
     * Salva le informazioni della pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $all_param = $this->getParam('dati');
        if (!$this->getParam("user_id", false)) {
            $_user = Top::getModel("bo_mapper/user");
            $all_param['password'] = base64_encode(sha1($all_param['password']));
        } else {
            $_user = Top::getModel("bo_mapper/user")->load($this->getParam("user_id"));
            $_user->setUpdated_at(date("Y-m-d H:i:s"));
            $all_param['password'] = ("password" == $all_param['password']) ? $_user->getPassword() : base64_encode(sha1($all_param['password']));
        }
        try {
            foreach ($all_param as $_attribute => $_value) {
                $_setString = "set" . ucfirst($_attribute);
                $_user->{$_setString}($_value);
            }
            $_user->save();
            $this->_helper->redirector("update", "useradmin", "admin", array("user_id" => $_user->getEntity_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "useradmin", "admin", array("user_id" => $_user->getEntity_id(), "messageResponse" => $e->getMessage(), "codeResponse" => 100));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelBoUserSingleton = Top::getSingleton("bo_mapper/user");
            if($this->getParam("user_id") == $_modelBoUserSingleton::getUserId()){
                throw new Exception("Non si puo` cancellare l'utente correntemente loggato", 500, NULL);
            }
            $_modelUser = Top::getModel("bo_mapper/user")->load($this->getParam("user_id"));
            $_modelUser->delete();
            $this->_helper->redirector("index", "useradmin", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "useradmin", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

    /**
     * Gestisce la creazione e la modifica di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function updateAction() {
        try {
            $_userSingleton = Top::getSingleton("bo_mapper/user");
            $_userSingleton = $_userSingleton->load($_userSingleton->getUserId());
            $_userId = $this->getParam("user_id", NULL);
            $lang = $this->getParam("lang", false);
            $_userModel = Top::getModel("bo_mapper/user");
            $_attributesEntityArray = $_userModel->getAttributes(true);
            $this->view->assign("fixedattribues", $_attributesEntityArray);
            switch ($_userId) {
                case false:
                    $this->view->newUser = true;
                    break;
                case NULL:
                    $this->forward("index");
                    break;
                default:
                        $model = $_userModel->load($this->getParam("user_id"));
                    if($model->getRole() < $_userSingleton->getRole()){
                        $this->_helper->redirector("index", "useradmin", "admin", array("codeResponse" => 100, "messageResponse" => "Non si hanno sufficienti privilegi per visualizzare questo utente"));
                    }
                    $this->view->newUser = false;
                    $this->view->assign("data", $model);
                    $this->view->language = $lang;
                    $this->view->codeResponse = $this->getParam("codeResponse");
                    break;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("user_id" => false));
    }

    private function responsiveTable($content) {
        $content = str_replace("<table", "<div class='responsive_table'><table", str_replace("<div class='responsive_table'><table", "<table", $content));
        $content = str_replace("</table>", "</table><p></p></div>", str_replace("</table><p></p></div>", "</table>", $content));
        return $content;
    }

}
