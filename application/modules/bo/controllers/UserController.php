<?php

class Bo_UserController extends Thinkopen_Bocontroller {

    private $_searchableAttributes = array("firstname", "lastname", "email", "created_at");
    private $_totalResultPaginatedCollection = 0;

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_userArray = array();
        $_whereCondition = $this->getParam("search", false);
        $_collection = $this->_getFiltres($_whereCondition);
        foreach ($_collection as $_fetched) {
            array_push($_userArray, Top::getModel("mapper/user")->load($_fetched->getUser_id())->getData());
        }
        echo json_encode(array("total" => $this->_totalResultPaginatedCollection, "rows" => (array) $_userArray));
    }

    private function _getFiltres($_whereCondition) {
        try {
            $modelShow = Top::getModel("mapper/user");
            $_realConditions = array();
            $_explodedConditions = explode("|", $_whereCondition);
            $_offset = $this->getParam("offset", 0) / $this->getParam("limit", 10) + 1;
            if (count($_explodedConditions) == 1) {
                if ($_whereCondition != "" && $_whereCondition) {
                    foreach ($this->_searchableAttributes as $_attributeName) {
                        $_realConditions[$_attributeName] = array("LIKE", "%" . $_whereCondition . "%");
                    }
                } else {
                    $_realConditions = array();
                }
                //getPaginatedCollection($_filters, $_pageNumber, $_language = NULL, $_itemsPerPage = 10, $_order = NULL, $_orWhere = false)
                $showcollection = $modelShow->getPaginatedCollection($_realConditions, $_offset, NULL, $this->getParam("limit", 10), (($this->getParam("sort", false)) ? array($this->getParam("sort"), strtoupper($this->getParam("order"))) : NULL), true);
            } else {
                array_pop($_explodedConditions);
                foreach ($_explodedConditions as $_singleCondition) {
                    $_elementsCondition = explode(">", $_singleCondition);
                    if (is_int($_elementsCondition[1])) {
                        $_realConditions[$_elementsCondition[0]] = array("=", $_elementsCondition[1]);
                    } else {
                        $_realConditions[$_elementsCondition[0]] = array("LIKE", "%" . $_elementsCondition[1] . "%");
                    }
                }
                $showcollection = $modelShow->getPaginatedCollection($_realConditions, $_offset, NULL, $this->getParam("limit", 10), (($this->getParam("sort", false)) ? array($this->getParam("sort"), strtoupper($this->getParam("order"))) : NULL), false);
            }
            $this->_totalResultPaginatedCollection = $modelShow->totalResultPaginatedCollection;
            return $showcollection;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            echo $ex->getTraceAsString();
        }
    }

// FUNZIONI PER IMPORT ----------------------------------------------------------------------------

    public function importAction() {
        $model = Top::getModel("mapper/user");
        $model->executeImport();
    }

//FINE FUNZIONI PER IMPORT ----------------------------------------------------------------------------


    public function exportAction() {
        $_page = Top::getModel("mapper/user");
        $_userArray = array();
        $_fetchedAll = $_page->fetchAll(NULL, null, null, null)->toArray();
        foreach ($_fetchedAll as $_fetched) {
            array_push($_userArray, Top::getModel("mapper/user")->load($_fetched['user_id'])->getData());
        }
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->Csv($_userArray, "users");
    }

    public function listAction() {
        $_pageModel = Top::getModel("mapper/user");
        $_attributesArray = $_pageModel->getAttributes(true);
        $_attributesModel = Top::getModel("bo_mapper/userattributes");
        $_requiredAttributesArray = $_attributesModel->fetchAll("is_required = 1")->toArray();
        foreach ($_requiredAttributesArray as $_simpleAttribute) {
            array_push($_attributesArray, $_simpleAttribute['attribute_name']);
        }
        $this->view->assign("searchableattributes", implode(", ", $this->_searchableAttributes));
        $this->view->assign("attributes", $_attributesArray);
    }

    /**
     * Salva le informazioni della pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $all_param = $this->getParam('dati');
        if (!$this->getParam("user_id", false)) {
            $_user = Top::getModel("mapper/user");
            $_user->setIs_facebook("0");
            $_hasBytes = mb_convert_encoding($all_param['password'], "UTF-16LE", mb_detect_encoding($all_param['password']));
            $_psswdCrypted = sha1($_hasBytes, true);
            $all_param['password'] = base64_encode($_psswdCrypted);
        } else {
            $_user = Top::getModel("mapper/user")->load($this->getParam("user_id"), $this->getParam("lang", NULL), false);
            $_user->setUpdated_at(date("Y-m-d H:i:s"));
            if ($_user->getPassword() == $all_param['password']) {
                unset($all_param['password']);
            } else {
                $_hasBytes = mb_convert_encoding($all_param['password'], "UTF-16LE", mb_detect_encoding($all_param['password']));
                $_psswdCrypted = sha1($_hasBytes, true);
                $all_param['password'] = base64_encode($_psswdCrypted);
            }
        }
        try {
            if (!in_array('newsletter', array_keys($all_param))) {
                $all_param['newsletter'] = "off";
            }
            if (!in_array('publish', array_keys($all_param))) {
                $all_param['publish'] = "off";
            }
            foreach ($all_param as $_attribute => $_value) {
                $_setString = "set" . ucfirst($_attribute);
                if ($_attribute == "newsletter" || $_attribute == "publish") {
                    $_value = ($_value == "on") ? "1" : "0";
                }
                $_user->{$_setString}($_value);
            }
            $_user->save();
            $this->_helper->redirector("update", "user", "admin", array("user_id" => $_user->getUser_id(), "lang" => $this->getParam("lang"), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "user", "admin", array("user_id" => $_user->getUser_id(), "lang" => $this->getParam("lang"), "codeResponse" => 100));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelUser = Top::getModel("mapper/user")->load($this->getParam("user_id"));
            $_modelUser->delete();
            $this->_helper->redirector("index", "user", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "user", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

    /**
     * Gestisce la creazione e la modifica di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function updateAction() {
        try {
            $lang = $this->getParam("lang", false);
            $_userModel = Top::getModel("mapper/user");
            $_attributesEntityArray = $_userModel->getAttributes(true);
            $this->view->assign("fixedattribues", $_attributesEntityArray);
            $_attributesModel = Top::getModel("bo_mapper/userattributes");
            $_attributesArray = $_attributesModel->fetchAll()->toArray();
            $this->view->assign("variableattributes", $_attributesArray);
            switch ($this->getParam("user_id", NULL)) {
                case false:
                    $this->view->language = "it";
                    $this->view->newPage = true;
                    break;
                case NULL:
                    $this->forward("index");
                    break;
                default:
                    if ($lang) {
                        $model = $_userModel->load($this->getParam("user_id"), $lang);
                    } else {
                        $model = $_userModel->load($this->getParam("user_id"));
                    }
                    $this->view->assign("attributesRealLanguage", $model->getRealLangAttributes());
                    $this->view->newPage = false;
                    $this->view->assign("data", $model);
                    $this->view->language = $lang;
                    $this->view->codeResponse = $this->getParam("codeResponse");
                    break;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("user_id" => false));
    }

    private function responsiveTable($content) {
        $content = str_replace("<table", "<div class='responsive_table'><table", str_replace("<div class='responsive_table'><table", "<table", $content));
        $content = str_replace("</table>", "</table><p></p></div>", str_replace("</table><p></p></div>", "</table>", $content));
        return $content;
    }

}
