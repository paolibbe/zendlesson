<?php

class Bo_HomeController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_home = Top::getModel("bo_mapper/home");
        $_fetchedAll = $_home->fetchAll(NULL, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_home->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_homeModel = Top::getModel("bo_mapper/home");
        $this->view->assign("attributes", $_homeModel->getAttributes());
        $this->view->codeResponse = $this->getParam("codeResponse");
        $this->view->messageResponse = $this->getParam("messageResponse");
    }

    public function updateAction() {
        switch ($this->getParam("home_id", NULL)) {
            case false:
                $this->view->newMenu = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/home")->load($this->getParam("home_id"));
                $this->view->newMenu = false;
                $this->view->modelhome = $model;
                $this->view->slide = Thinkopen_Elementparser::buildDivHomeBo(json_decode($model->getSlide(),true));
                $this->view->language = $this->getParam("language");
                $this->view->codeResponse = $this->getParam("codeResponse");
                $this->view->messageResponse = $this->getParam("messageResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("home_id" => false));
    }

    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getParam("home_id")) {
            $_modelHome = Top::getModel("bo_mapper/home")->load($this->getParam("home_id"));
        } else {
            $_modelHome = Top::getModel("bo_mapper/home");
        }
        try {
            $_slide = json_encode($this->getParam("element"));
            $_modelHome->setLanguage($this->getParam("language"))
                    ->setBg_image_uno($this->getParam("bg_image_uno"))
                    ->setPhrase_uno($this->getParam("phrase_uno"))
                    ->setBg_image_due($this->getParam("bg_image_due"))
                    ->setPhrase_due($this->getParam("phrase_due"))
                    ->setBg_image_tre($this->getParam("bg_image_tre"))
                    ->setPhrase_tre($this->getParam("phrase_tre"))
                    ->setBg_image_quattro($this->getParam("bg_image_quattro"))
                    ->setPhrase_quattro($this->getParam("phrase_quattro"))
                    ->setSlide($_slide)
                    ->save();
            $this->_helper->redirector("update", "home", "admin", array("home_id" => $_modelHome->getHome_id(), "language" => $this->getParam("language"), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("index", "home", "admin", array("codeResponse" => 100, "messageResponse" => $e->getMessage()));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelHome = Top::getModel("bo_mapper/home")->load($this->getParam("home_id"));
            $_modelHome->delete();
            $this->_helper->redirector("index", "home", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "home", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

}
