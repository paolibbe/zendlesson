<?php

class Bo_NewsController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_news = Top::getModel("bo_mapper/news");
        $_whereCondition = $this->getParam("search", false);
        if ($_whereCondition) {
            $_where = array('identifier LIKE ?' => "%" . $_whereCondition . "%",
                'title LIKE ?' => "%" . $_whereCondition . "%");
        } else {
            $_where = NULL;
        }
        $_fetchedAll = $_news->fetchAll($_where, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_news->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_newsModel = Top::getModel("bo_mapper/news");
        $this->view->assign("attributes", $_newsModel->getAttributes());
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelNews = Top::getModel("bo_mapper/news")->load($this->getParam("news_id"));
            $news_id_to_ref = ($_modelNews->getLanguage() == "it") ? $_modelNews->getNews_id() : false;
            if ($news_id_to_ref) {
                $_modelNews->deleteAllRelatedLanguageNews($news_id_to_ref);
            }
            $_modelNews->delete();
            $_searcher = new Thinkopen_Searcher();
            foreach (Top::getLanguages() as $_language) {
                $_searcher->delete($this->getParam("news_id"), Thinkopen_Searcher::NEWS, $_language);
            }
            $this->_helper->redirector("index", "news", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "news", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

    /**
     * Salva le informazioni della news
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_searcher = new Thinkopen_Searcher();
        if ($this->getParam("clona", false)) {
            $this->clonanews($this->getAllParams());
        }
        if (!$this->getParam("news_id", false)) {
            $_news = Top::getModel("bo_mapper/news");
        } else {
            $_news = Top::getModel("bo_mapper/news")->load($this->getParam("news_id"));
        }
        try {
            $_news->setTitle($this->getParam("title"))
                    ->setIdentifier(trim($this->getParam("identifier")))
                    ->setMeta_keywords($this->getParam("meta_keywords"))
                    ->setMeta_description($this->getParam("meta_description"))
                    ->setUpdated_at(date("Y-m-d H:i:s"))
                    ->setContent_type($this->getParam("content_type"))
                    ->setLanguage($this->getParam("language"))
                    ->setFb_share_desc($this->getParam("fb_share_desc"))
                    ->setFb_share_img($this->getParam("fb_share_img"))
                    ->setImg_wide($this->getParam("img_wide"))
                    ->setLayout($this->getParam("layout"))
                    ->setContent($this->getParam("content"))
                    ->setImage($this->getParam("image"));
            $iscative = ($this->getParam("is_active") == "on") ? 1 : 0;
            $_news->setIs_active($iscative);
            $_newNews = $_news->save();
            $_searcher->addNewsToIndex($_newNews->getNews_id());
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $_news->getNews_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $_news->getNews_id(), "codeResponse" => 100, "message" => $e->getMessage()));
        }
    }

    /**
     * Cloan la news per la traduzione
     * @param array $_post
     */
    private function clonanews($_post) {
        $_news = Top::getModel("bo_mapper/news");
        try {
            $_news->setTitle($_post["name_clone"])
                    ->setIdentifier(trim($_post["link_clone"]))
                    ->setRef_lang_id($_post["news_id"])
                    ->setMeta_keywords($this->getParam("meta_keywords"))
                    ->setMeta_description($this->getParam("meta_description"))
                    ->setUpdated_at(date("Y-m-d H:i:s"))
                    ->setFb_share_desc($this->getParam("fb_share_desc"))
                    ->setFb_share_img($this->getParam("fb_share_img"))
                    ->setImg_wide($this->getParam("img_wide"))
                    ->setContent_type($this->getParam("content_type"))
                    ->setLanguage($this->getParam("language_clone"))
                    ->setLayout($this->getParam("layout"))
                    ->setContent($this->getParam("content"))
                    ->setImage($this->getParam("image"));
            $iscative = ( $_post["is_active"] == "on") ? 1 : 0;
            $_news->setIs_active($iscative)
                    ->save();
            $modelBlocchiShoulder = Top::getModel("mapper/news")->loadBlocksShoulder($_post["news_id"], $_post['language']);
            foreach ($modelBlocchiShoulder as $postuno) {
                $_primoPost = $postuno->getData();
                unset($_primoPost['block_id']);
                if ($_primoPost['is_active'] == "0") {
                    unset($_primoPost['is_active']);
                }
                $this->setParameteresForBlockNewsShoulder($_primoPost, $_news->getNews_id(), $_post['language_clone']);
            }
            $modelBlocchi = Top::getModel("mapper/news")->loadBlocks($_post["news_id"], $_post['language']);
            if ($_post["content_type"] == 0) {
                $_secondoPost = $modelBlocchi[0]->getData();
                if ($_secondoPost ['is_active'] == "0") {
                    unset($_secondoPost ['is_active']);
                }
                unset($_secondoPost['block_id']);
                $this->setParameteresForBlockNews($_secondoPost, $_news->getNews_id(), $_post['language_clone'], false);
            } else {
                foreach ($modelBlocchi as $postdue) {
                    $_secondoPost = $postdue->getData();
                    if ($_secondoPost ['is_active'] == "0") {
                        unset($_secondoPost ['is_active']);
                    }
                    unset($_secondoPost['block_id']);
                    $this->setParameteresForBlockNews($_secondoPost, $_news->getNews_id(), $_post['language_clone'], true);
                }
            }
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $_news->getNews_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $_news->getNews_id(), "codeResponse" => 100));
        }
    }

    /**
     * Gestisce la creazione e la modifica di una news
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function updateAction() {
        switch ($this->getParam("news_id", NULL)) {
            case false:
                $this->view->newNews = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/news")->load($this->getParam("news_id"));
                $this->view->newNews = false;
                $this->view->assign("data", $model);
//                $this->view->language = $this->getParam("language");
                $this->view->language = $model->getLanguage();
                $this->view->codeResponse = $this->getParam("codeResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("news_id" => false));
    }

    /**
     * Gestisce la creazione dei blocchi di una news
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function createblocksAction() {
        try {
            $language = ($this->getParam("language")) ? $this->getParam("language") : "it";
            $newsModel = Top::getModel("bo_mapper/news")->load($this->getParam("news_id"));
            $modelBlocchi = Top::getModel("mapper/news")->loadBlocks($this->getParam("news_id"), $language);
            $this->view->assign("data", $newsModel);
            $this->view->modelblocchi = $modelBlocchi;
            $this->view->language = $language;
        } catch (Exception $ex) {
            echo"errore";
            echo $ex->getTraceAsString();
        }
    }

    /**
     * Salva le informazioni dei blocchi interni alla news
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveblockAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $postData = $this->getAllParams();
        try {
            if ($postData['content_type'] == 0) {
                $this->setParameteresForBlockNews($postData['form_fluid'], $postData['news_id'], $postData['language'], false);
            } else {
                foreach ($postData['form_dati'] as $_post) {
                    $this->setParameteresForBlockNews($_post, $postData['news_id'], $postData['language'], true);
                }
            }
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $postData['news_id'], "language" => $postData['language'], "codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $postData['news_id'], "language" => $postData['language'], "codeResponse" => 100));
        }
    }

    /**
     * Effettua il save degli attributi per il model bo_dbtable/newscontent
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param array $_post
     * @param string $_newsId
     * @param string $language
     * @param boolean $isBlock
     */
    private function setParameteresForBlockNews($_post, $_newsId, $language, $isBlock) {
        if (array_key_exists('block_id', $_post)) {
            $modelBlock = Top::getModel("bo_dbTable/newscontent")->load($_post['block_id']);
        } else {
            $modelBlock = Top::getModel("bo_dbTable/newscontent");
        }
        $modelBlock->setNews_id($_newsId)
                ->setLanguage($language)
                ->setBackground_image($_post['background_image'])
                ->setContent($_post['content']);
        if (array_key_exists('is_active', $_post)) {
            $modelBlock->setIs_active(1);
        } else {
            $modelBlock->setIs_active(0);
        }
        if ($isBlock) {
            $modelBlock->setBlock_position($_post['block_position']);
            $modelBlock->setBlock_type(1);
        } else {
            $modelBlock->setBlock_position(0);
            $modelBlock->setBlock_type(0);
        }
        $modelBlock->save();
    }

    /** SPALLA * */

    /**
     * Gestisce la creazione dei blocchi di una news
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function createblocksshoulderAction() {
        try {
            $language = ($this->getParam("language")) ? $this->getParam("language") : "it";
            $newsModel = Top::getModel("bo_mapper/news")->load($this->getParam("news_id"));
            $modelBlocchi = Top::getModel("mapper/news")->loadBlocksShoulder($this->getParam("news_id"), $language);
            $this->view->assign("data", $newsModel);
            $this->view->modelblocchishould = $modelBlocchi;
            $this->view->language = $language;
        } catch (Exception $ex) {
            echo"errore";
            echo $ex->getTraceAsString();
        }
    }

    /**
     * Salva le informazioni dei blocchi interni alla news
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveblockshoulderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $postData = $this->getAllParams();
        try {
            foreach ($postData['form_spalla'] as $_post) {
                $this->setParameteresForBlockNewsShoulder($_post, $postData['news_id'], $postData['language']);
            }
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $postData['news_id'], "codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $postData['news_id'], "codeResponse" => 100));
        }
    }

    /**
     * Effettua il save degli attributi per il model bo_dbtable/newsshoulder
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param array $_post
     * @param string $_newsId
     * @param string $language
     * @param boolean $isBlock
     */
    private function setParameteresForBlockNewsShoulder($_post, $_newsId, $language) {
        try {
            if (array_key_exists('block_id', $_post)) {
                $modelBlock = Top::getModel("bo_dbTable/newsshoulder")->load($_post['block_id']);
            } else {
                $modelBlock = Top::getModel("bo_dbTable/newsshoulder");
            }
            $modelBlock->setNews_id($_newsId)
                    ->setLanguage($language)
                    ->setBackground_image($_post['background_image'])
                    ->setContent($this->pulisciSpalla($_post['content']));
            if (array_key_exists('is_active', $_post)) {
                $modelBlock->setIs_active(1);
            } else {
                $modelBlock->setIs_active(0);
            }
            $modelBlock->setBlock_position($_post['block_position']);
            $modelBlock->setBlock_type(1);
            $modelBlock->save();
        } catch (Exception $e) {
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $_newsId, "language" => $language, "codeResponse" => 100));
        }
    }

    private function pulisciSpalla($content) {
        $content = str_replace("<p>", "<div>", $content);
        $content = str_replace("</p>", "</div>", $content);
        $content = str_replace("<p></p>", "", $content);
        $content = str_replace("<p> </p>", "", $content);
        for ($i = 1; $i < 7; $i++) {
            $content = str_replace("<h" . $i . "></h" . $i . ">", "", $content);
            $content = str_replace("<h" . $i . "> </h" . $i . ">", "", $content);
        }
        return $content;
    }

}
