<?php

class Bo_ProgrammController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_programm = Top::getModel("bo_mapper/programm");
        $_fetchedAll = $_programm->fetchAll(NULL, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_programm->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_programmModel = Top::getModel("bo_mapper/programm");
        $this->view->assign("attributes", $_programmModel->getAttributes());
        $this->view->codeResponse = $this->getParam("codeResponse");
        $this->view->messageResponse = $this->getParam("messageResponse");
    }

    public function updateAction() {
        switch ($this->getParam("prog_id", NULL)) {
            case false:
                $this->view->newProgramm = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/programm")->load($this->getParam("prog_id"));
                $this->view->newProgramm = false;
                $this->view->assign("data", $model);
                $this->view->language = $this->getParam("language");
                $this->view->codeResponse = $this->getParam("codeResponse");
                $this->view->messageResponse = $this->getParam("messageResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("prog_id" => false));
    }

    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (!$this->getParam("prog_id", false)) {
            $_programm = Top::getModel("bo_mapper/programm");
        } else {
            $_programm = Top::getModel("bo_mapper/programm")->load($this->getParam("prog_id"));
        }
        try {
            $_programm->setTitle($this->getParam("title"))
                    ->setIdentifier(trim($this->getParam("identifier")))
                    ->setMeta_keywords($this->getParam("meta_keywords"))
                    ->setMeta_description($this->getParam("meta_description"))
                    ->setUpdated_at(date("Y-m-d H:i:s"))
                    ->setFb_share_desc($this->getParam("fb_share_desc"))
                    ->setFb_share_img($this->getParam("fb_share_img"))
                    ->setImg_wide($this->getParam("img_wide"));
            if ($this->getParam("isspecial") == "on") {
                $_programm->setIsspecial(1);
                if ($this->getParam("special_dal") != "") {
                    $_programm->setSpecial_dal(date("Y-m-d", strtotime(str_replace('/', '-', $this->getParam("special_dal")))));
                }
                if ($this->getParam("special_al") != "") {
                    $_programm->setSpecial_al(date("Y-m-d", strtotime(str_replace('/', '-', $this->getParam("special_al")))));
                }
            } else {
                $_programm->setIsspecial(0);
                $_programm->setCategory_id($this->getParam("category_id"));
            }
            $iscative = ($this->getParam("is_active") == "on") ? 1 : 0;
            $_programm->setIs_active($iscative)
                    ->save();
            $this->_helper->redirector("update", "programm", "admin", array("prog_id" => $_programm->getProg_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "programm", "admin", array("prog_id" => $_programm->getProg_id(), "codeResponse" => 100));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelProgramm = Top::getModel("bo_mapper/programm")->load($this->getParam("prog_id"));
            $_modelProgramm->delete();
            $this->_helper->redirector("list", "programm", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("list", "programm", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

}
