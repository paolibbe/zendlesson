<?php

class Bo_ShowController extends Thinkopen_Bocontroller {

    private $_searchableAttributes = array("titolo", "category_id", "data_da", "data_a");
    private $_totalResultPaginatedCollection = 0;

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function archivedAction() {
        $_pageModel = Top::getModel("bo_mapper/archived/show");
        $_attributesArray = $_pageModel->getAttributes(true);
        $_attributesModel = Top::getModel("bo_mapper/archived/showattributes");
        $_requiredAttributesArray = $_attributesModel->fetchAll()->toArray();
        foreach ($_requiredAttributesArray as $_simpleAttribute) {
            $_attributesArray[$_simpleAttribute['attribute_name']] = $_simpleAttribute['attribute_label'];
        }
        $this->view->assign("attributes", $_attributesArray);
    }

    public function jsonrenderarchivedAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_page = Top::getModel("bo_mapper/archived/show");
        $_showArray = array();
        $_whereCondition = $this->getParam("search", false);
        if ($_whereCondition) {
            $_where = array('identifier LIKE ?' => "%" . $_whereCondition . "%");
        } else {
            $_where = NULL;
        }
        $_fetchedAll = $_page->fetchAll($_where, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        foreach ($_fetchedAll as $_fetched) {
            array_push($_showArray, Top::getModel("bo_mapper/archived/show")->load($_fetched['show_id'])->getData());
        }
        echo json_encode(array("total" => $_page->countRows(), "rows" => (array) $_showArray));
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_showArray = array();
        $_whereCondition = $this->getParam("search", false);
        $_collection = $this->_getFiltres($_whereCondition);
        foreach ($_collection as $_fetched) {
            array_push($_showArray, Top::getModel("mapper/show")->load($_fetched->getShow_id())->getData());
        }

        echo json_encode(array("total" => $this->_totalResultPaginatedCollection, "rows" => (array) $_showArray));
    }

    private function _getFiltres($_whereCondition) {
        try {
            $modelShow = Top::getModel("mapper/show");
            $_realConditions = array();
            $_explodedConditions = explode("|", $_whereCondition);
            $_offset = $this->getParam("offset", 0) / $this->getParam("limit", 10) + 1;
            if (count($_explodedConditions) == 0) {
                if ($_whereCondition != "" && $_whereCondition) {
                    foreach ($this->_searchableAttributes as $_attributeName) {
                        $_realConditions[$_attributeName] = array("LIKE", "%" . $_whereCondition . "%");
                    }
                } else {
                    $_realConditions = array();
                }
                //getPaginatedCollection($_filters, $_pageNumber, $_language = NULL, $_itemsPerPage = 10, $_order = NULL, $_orWhere = false)
                $showcollection = $modelShow->getPaginatedCollection($_realConditions, $_offset, NULL, $this->getParam("limit", 10), (($this->getParam("sort", false)) ? array($this->getParam("sort"), strtoupper($this->getParam("order"))) : NULL), true);
            } else {
                array_pop($_explodedConditions);
                foreach ($_explodedConditions as $_singleCondition) {
                    $_elementsCondition = explode(">", $_singleCondition);
                    if (is_int($_elementsCondition[1]) || ($_elementsCondition[0] == "category_id" && $_elementsCondition[1] != "")) {
                        $_realConditions[$_elementsCondition[0]] = array(" = ", $_elementsCondition[1]);
                    } else {
                        $_realConditions[$_elementsCondition[0]] = array("LIKE", "%" . $_elementsCondition[1] . "%");
                    }
                }
                $showcollection = $modelShow->getPaginatedCollection($_realConditions, $_offset, NULL, $this->getParam("limit", 10), (($this->getParam("sort", false)) ? array($this->getParam("sort"), strtoupper($this->getParam("order"))) : NULL), false);
            }
            $this->_totalResultPaginatedCollection = $modelShow->totalResultPaginatedCollection;
            return $showcollection;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            echo $ex->getTraceAsString();
        }
    }

    public function listAction() {
        $_pageModel = Top::getModel("mapper/show");
        $_attributesArray = $_pageModel->getAttributes(true);
        $_attributesModel = Top::getModel("bo_mapper/showattributes");
        $_requiredAttributesArray = $_attributesModel->fetchAll("is_required = 1")->toArray();
        foreach ($_requiredAttributesArray as $_simpleAttribute) {
            $_attributesArray[$_simpleAttribute['attribute_name']] = $_simpleAttribute['attribute_label'];
        }
        $categoryArray = Top::getModel("bo_mapper/category")->getArrayCategories();
        $this->view->assign("categoryarray", json_encode($categoryArray));
        $this->view->assign("searchableattributes", implode(", ", $this->_searchableAttributes));
        $this->view->assign("attributes", $_attributesArray);
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            if ($this->getParam("show_id", false)) {
                $_modelUser = Top::getModel("mapper/show")->load($this->getParam("show_id"));
                $_modelUser->delete();
                $_searcher = new Thinkopen_Searcher();
                foreach (Top::getLanguages() as $_language) {
                    $_searcher->delete($this->getParam("show_id"), Thinkopen_Searcher::SHOWS, $_language);
                }
                $this->_helper->redirector("index", "show", "admin", array("codeResponse" => 200));
            } else {
                throw new Exception("Specificare un id valido", 500, NULL);
            }
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "show", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

    /**
     * Salva le informazioni della pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_searcher = new Thinkopen_Searcher();
        if (!$this->getParam("show_id", false)) {
            $_show = Top::getModel("mapper/show");
        } else {
            $_show = Top::getModel("mapper/show")->load($this->getParam("show_id"), $this->getParam("lang", NULL), false);
            $_show->setUpdated_at(date("Y-m-d H:i:s"));
        }
        try {
            $all_param = $this->getParam('dati');
            if (!in_array('is_in_hp', array_keys($all_param))) {
                $all_param['is_in_hp'] = "off";
            }
            foreach ($all_param as $_attribute => $_value) {
                $_setString = "set" . ucfirst($_attribute);
                if ($_attribute == "tabella_prezzi" || $_attribute == "description") {
                    $_value = $this->responsiveTable($_value, true);
                }
                if ($_attribute == "is_in_hp") {
                    $_value = ($_value == "on") ? "1" : "0";
                }
                $_show->{$_setString}($_value);
            }
            $_show->save();
            $_searcher->addShowToIndex($_show->getShow_id(), $this->getParam("lang", "it"));
            $this->_helper->redirector("update", "show", "admin", array("show_id" => $_show->getShow_id(), "lang" => $this->getParam("lang"), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "show", "admin", array("show_id" => $_show->getShow_id(), "lang" => $this->getParam("lang"), "codeResponse" => 100));
        }
    }

    /**
     * Salva le informazioni della pagina, proveniente dalla duplicazione dell'elemento
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function duplicateAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_searcher = new Thinkopen_Searcher();
        $_show = Top::getModel("mapper/show");
        try {
            $all_param = $this->getParam('dati');
            if (!in_array('is_in_hp', array_keys($all_param))) {
                $all_param['is_in_hp'] = "off";
            }
            foreach ($all_param as $_attribute => $_value) {
                $_setString = "set" . ucfirst($_attribute);
                if ($_attribute == "tabella_prezzi" || $_attribute == "description") {
                    $_value = $this->responsiveTable($_value, true);
                }
                if ($_attribute == "is_in_hp") {
                    $_value = ($_value == "on") ? "1" : "0";
                }
                $_show->{$_setString}($_value);
            }
            $_show->save();
            $_searcher->addShowToIndex($_show->getShow_id(), $this->getParam("lang", "it"));
            $this->_helper->redirector("update", "show", "admin", array("show_id" => $_show->getShow_id(), "lang" => $this->getParam("lang"), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("archiveddetail", "show", "admin", array("show_id" => $this->getParam("show_id"), "lang" => $this->getParam("lang"), "codeResponse" => 100));
        }
    }

    /**
     * Gestisce l'archiviazione degli show all'interno delle tabelle di appoggio
     * @author Paolo Libertini <paolo.libertini@thinkopen.it>
     */
    public function archiveAction() {
        try {
            if ($this->getParam("show_id", false)) {
                $showModel = Top::getModel("mapper/show");
                $show = $showModel->load($this->getParam("show_id"));
                if ($show) {
                    $_resultArchive = $show->archiveShow();
                    if ($_resultArchive) {
                        $this->_helper->redirector("index", "show", "admin", array("codeResponse" => 200));
                    } else {
                        throw new Exception("Cannot archive show", 502, NULL);
                    }
                } else {
                    throw new Exception("Cannot load show", 502, NULL);
                }
            } else {
                throw new Exception("Cannot load show", 502, NULL);
            }
        } catch (Exception $exc) {
            $this->_helper->redirector("index", "show", "admin", array("codeResponse" => 100, "message" => $exc->getMessage()));
        }
    }

    /**
     * Gestisce la creazione e la modifica di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function archiveddetailAction() {
        try {
            $this->_helper->viewRenderer("update");
            $_showModel = Top::getModel("bo_mapper/archived/show");
            $_attributesEntityArray = $_showModel->getAttributes(true);
            $this->view->assign("fixedattribues", $_attributesEntityArray);
            $_attributesModel = Top::getModel("bo_mapper/archived/showattributes");
            $_attributesArray = $_attributesModel->fetchAll()->toArray();
            $this->view->assign("variableattributes", $_attributesArray);
            $this->view->assign("archived", true);
            switch ($this->getParam("show_id", NULL)) {
                case false:
                    $this->view->language = "it";
                    $this->view->newPage = true;
                    break;
                case NULL:
                    $this->forward("index");
                    break;
                default:
                    $model = $_showModel->load($this->getParam("show_id"));
                    $model->setTabella_prezzi($this->responsiveTable($model->getTabella_prezzi()));
                    $model->setDescription($this->responsiveTable($model->getDescription()));
                    $this->view->assign("attributesRealLanguage", $model->getRealLangAttributes());
                    $this->view->newPage = false;
                    $this->view->assign("data", $model);
                    $this->view->language = NULL;
                    $this->view->codeResponse = $this->getParam("codeResponse");
                    break;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Gestisce la creazione e la modifica di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function updateAction() {
        try {
            $lang = $this->getParam("lang", false);
            $_showModel = Top::getModel("mapper/show");
            $_attributesEntityArray = $_showModel->getAttributes(true);
            $this->view->assign("fixedattribues", $_attributesEntityArray);
            $_attributesModel = Top::getModel("bo_mapper/showattributes");
            $_attributesArray = $_attributesModel->fetchAll()->toArray();
            $this->view->assign("variableattributes", $_attributesArray);
            switch ($this->getParam("show_id", NULL)) {
                case false:
                    $this->view->language = "it";
                    $this->view->newPage = true;
                    break;
                case NULL:
                    $this->forward("index");
                    break;
                default:
                    if ($lang) {
                        $model = $_showModel->load($this->getParam("show_id"), $lang);
                    } else {
                        $model = $_showModel->load($this->getParam("show_id"));
                    }
                    $model->setTabella_prezzi($this->responsiveTable($model->getTabella_prezzi()));
                    $model->setDescription($this->responsiveTable($model->getDescription()));
                    $this->view->assign("attributesRealLanguage", $model->getRealLangAttributes());
                    $this->view->newPage = false;
                    $this->view->assign("data", $model);
                    $this->view->language = $lang;
                    $this->view->codeResponse = $this->getParam("codeResponse");
                    break;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("show_id" => false));
    }

    private function responsiveTable($content, $isSave = false) {
        if ($isSave) {
            $content = str_replace("<table", "<div class='responsive_table'><table", $content);
            $content = str_replace("</table>", "</table><div class='spazio_table_responsive'></div></div>", $content);
        } else {
            $content = str_replace("<div class='responsive_table'>", "", $content);
            $content = str_replace("<div class='spazio_table_responsive'></div></div>", "", $content);
        }
        return $content;
    }

}
