<?php

class Bo_UploadController extends Thinkopen_Bocontroller {

    public function indexAction() {
        
    }

    public function connectorAction() {
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        error_reporting(0); // Set E_ALL for debuging
        function access($attr, $path, $data, $volume) {
            return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
                    ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
                    : null;                                    // else elFinder decide it itself
        }
        $opts = array(
            'roots' => array(
                array(
                    'driver' => 'LocalFileSystem', // driver for accessing file system (REQUIRED)
                    'path' => APPLICATION_PATH . '/../public/files/', // path to files (REQUIRED)
                    'URL' => BASE_URL . 'files/', // URL to files (REQUIRED)
                    'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
                )
            )
        );
        $connector = new Elfinder_Connector(new Elfinder_Elfinder($opts));
        $connector->run();
    }

}
