<?php

class Bo_FooterController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_footer = Top::getModel("bo_mapper/footer");
        $_fetchedAll = $_footer->fetchAll(NULL, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_footer->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_footerModel = Top::getModel("bo_mapper/footer");
        $this->view->assign("attributes", $_footerModel->getAttributes());
        $this->view->codeResponse = $this->getParam("codeResponse");
        $this->view->messageResponse = $this->getParam("messageResponse");
    }

    public function updateAction() {
        switch ($this->getParam("footer_id", NULL)) {
            case false:
                $this->view->newMenu = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/footer")->load($this->getParam("footer_id"));
                $this->view->newMenu = false;
                $this->view->modelfooter = $model;
                $this->view->xmlparsato = Thinkopen_Elementparser::parseToUpdate($model->getContent());
                $this->view->language = $this->getParam("language");
                $this->view->codeResponse = $this->getParam("codeResponse");
                $this->view->messageResponse = $this->getParam("messageResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("footer_id" => false));
    }

    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getParam("footer_id")) {
            $_modelFooter = Top::getModel("bo_mapper/footer")->load($this->getParam("footer_id"));
        } else {
            $_modelFooter = Top::getModel("bo_mapper/footer");
        }
        try {
            $name = ($this->getParam("name")) ? $this->getParam("name") : $this->getParam("name_clone");
            $lang = ($this->getParam("language")) ? $this->getParam("language") : $this->getParam("language_clone");
            if (is_array($this->getParam("element"))) {
                $_structureXml = Thinkopen_Elementparser::assemble($this->getParam("element"));
            } else {
                $_structureXml = "";
            }
            if ($this->getParam("footer_id")) {
                $isact = ($this->getParam("is_active", NULL) == "on") ? "1" : "0";
            } else {
                $isact = "0";
            }
            $_modelFooter->setLanguage($lang)
                    ->setName($name)
                    ->setIs_active($isact)
                    ->setBg_color($this->getParam("bg_color"))
                    ->setInfo_biglietteria($this->getParam("info_biglietteria"))
                    ->setContent($_structureXml)
                    ->save();
            $this->_helper->redirector("list", "footer", "admin", array("codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("index", "footer", "admin", array("codeResponse" => 100, "messageResponse" => $e->getMessage()));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelFooter = Top::getModel("bo_mapper/footer")->load($this->getParam("footer_id"));
            $_modelFooter->delete();
            $this->_helper->redirector("index", "footer", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "footer", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

}
