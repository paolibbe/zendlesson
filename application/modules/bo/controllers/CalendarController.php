<?php

class Bo_CalendarController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $calendar = new Thinkopen_Calendar();
        $month = ($this->getParam('month')) ? $this->getParam("month") : date("m");
        $year = ($this->getParam('year')) ? $this->getParam("year") : date("Y");
        $this->view->content = $calendar->getCalendarBackoffice($month, $year);
    }

    public function eventAction() {
        try {
            $month = ($this->getParam('om')) ? $this->getParam("om") : date("m");
            $year = ($this->getParam('oy')) ? $this->getParam("oy") : date("Y");
            $day = ($this->getParam('od')) ? $this->getParam("od") : date("d");
            $full_content_date = date("d.m.Y", strtotime($day . '.' . $month . '.' . $year));
            $pos = $this->getParam('pos');
            $pgGo = $this->getParam("pgGo", false);

            $calendar = new Thinkopen_Calendar();
            $this->view->arrayLang = $calendar->getArrayLang();
            $this->view->caLang = $calendar->getCaLang();
            if (($this->getParam("addNote", false))) {
                $this->saveEvent($this->getAllParams());
            }
            if (($this->getParam("editNote", false))) {
                $this->saveEvent($this->getAllParams());
            }
            $showCollection = Top::getModel("mapper/show")->getCollection(array("data_a" => array(">=", ($year."-".$month."-".$day))), "it");
            switch ($pos) {
                case 0:
                    $this->view->content = $calendar->listEvent($full_content_date, $pgGo, $day, $month, $year);
                    break;
                case 1:
                    $this->view->content = $calendar->addEvent($full_content_date, $showCollection);
                    break;
                case 2:
                    $model = Top::getModel("bo_mapper/calendar")->load($this->getParam("calendar_id"));
                    $this->view->content = $calendar->editEvent($model, $showCollection);
                    break;
                case 3:
                    die("ERRORE GUARDA CLASSE BO CONTROLLER");
                    break;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function savedAction() {
        $this->view->check = $this->getParam("codeResponse", false);
        $this->view->message = $this->getParam("messageResponse", false);
    }

    public function deleteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $model = Top::getModel("bo_mapper/calendar")->load($this->getParam("calendar_id"));
            $model->delete();
            $this->_helper->redirector("index", "calendar", "admin", array("codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("index", "calendar", "admin", array("messageResponse" => $e->getMessage()));
        }
    }

    private function saveEvent($post) {
        try {
            if (array_key_exists("calendar_id",$post)) {
                $modelCal = Top::getModel("bo_mapper/calendar")->load($post["calendar_id"]);
            }else {
                $modelCal = Top::getModel("bo_mapper/calendar");
            }
            $show = Top::getModel("mapper/show");
            $show->load($post['show_id']);
            if ($show) {
                $modelCal->setTitle($show->getTitolo())
                        ->setLink($show->getIdentifier())
                        ->setLocation($show->getLocation())
                        ->setDate($this->_rotateDate($post['orgDate']))
                        ->setLink_geticket($post['geticket_url'])
                        ->setHour(date("H:i:s", strtotime($post['orgHour'] . ":00")))
                        ->save();
                $this->_helper->redirector("index", "calendar", "admin", array("codeResponse" => 200));
            } else {
                throw new Exception("Spettacolo non valido", 501);
            }
        } catch (Exception $e) {
            $this->_helper->redirector("index", "calendar", "admin", array("messageResponse" => $e->getMessage()));
        }
    }

    private function _rotateDate($originalDate, $_american = false) {
        $_americanDate = date("Y-m-d", strtotime($date = str_replace('/', '-', $originalDate)));
        return ($_american) ? $_americanDate : date("Y-m-d", strtotime($originalDate));
    }

}
