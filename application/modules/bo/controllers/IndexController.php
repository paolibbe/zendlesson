<?php

class Bo_IndexController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $_user = Top::getSingleton("bo_mapper/user");
        $_userId = $_user::getUserId();
        if ($_userId) {
            $_user->load($_userId);
            $ga_email = 'sancarlo.thinkopen@gmail.com';
            $ga_password = 'th1nk0p3n';
            $ga_profile_id = '61070135';
//            $ga = new Thinkopen_Gapi($ga_email, $ga_password);
//            $ga->requestReportData($ga_profile_id, array('date'), array('pageviews', 'visits'), 'date');
//            $this->view->firstResult = $ga->getResults();
//            $ga->requestReportData($ga_profile_id, array('pagePath'), array('pageviews', 'visits', 'exitRate', 'avgTimeOnPage', 'entranceBounceRate'));
//            $this->view->secondResult = $ga->getResults();
//            $this->view->pageviews = number_format($ga->getPageviews(), 0, ",", ".");
//            $this->view->visits = number_format($ga->getVisits(), 0, ",", ".");
//            $this->view->avgTime = round($ga->getAvgTimeOnPage(), 2);
//            $this->view->bounceRate = round($ga->getEntranceBounceRate(), 2);
            $client = new Zend_Http_Client("http://local.health.it/");
            $client->setParameterGet("out", "json");
            $_response = $client->request("GET")->getBody();
            $_jsonResponse = Zend_Json::decode($_response);
            $_percentRam = round($_jsonResponse['RAM']['free'] / $_jsonResponse['RAM']['total'] * 100, 2);
            $_freeRam = round($_jsonResponse['RAM']['free'] / 1000000000, 2);
            $this->view->freeRam = "<span class=' col-lg-4 col-md-4 col-sm-12 col-xs-12'>RAM Libera:</span><span class='col-lg-8 col-md-8 col-sm-12 col-xs-12'><b>" . $_percentRam . '% (' . $_freeRam . 'GB)</b></span>';
            $_serverConfig = "<span class=' col-lg-4 col-md-4 col-sm-12 col-xs-12'>O.S.:</span><span class='col-lg-8 col-md-8 col-sm-12 col-xs-12'><b> " . implode(" v. ", $_jsonResponse['Distro']) . "</b></span>";
            $_serverConfig .= "<span class=' col-lg-4 col-md-4 col-sm-12 col-xs-12'>RAM:</span><span class='col-lg-8 col-md-8 col-sm-12 col-xs-12'><b> " . round($_jsonResponse['RAM']['total'] / 1000000000, 2) . "GB</b></span>";
            $_serverConfig .= "<span class=' col-lg-4 col-md-4 col-sm-12 col-xs-12'>CPUs:</span><span class='col-lg-8 col-md-8 col-sm-12 col-xs-12'><b> n. " . count($_jsonResponse['CPU']) . " cpus</b></span>";
             $_serverConfig .= "<span class=' col-lg-4 col-md-4 col-sm-12 col-xs-12'>Model:</span><span class='col-lg-8 col-md-8 col-sm-12 col-xs-12'><b> " . $_jsonResponse['CPU'][0]['Model'] . "</b></span>";
            $_serverConfig .= "<span class=' col-lg-4 col-md-4 col-sm-12 col-xs-12'>Server Time:</span><span class='col-lg-8 col-md-8 col-sm-12 col-xs-12'> <b>" . str_replace("T", " ", $_jsonResponse['timestamp']) . "</b></span>";
            $this->view->serverConfig = $_serverConfig;
            $_pageModel = Top::getSingleton("mapper/user");
            $_attributesArray = $_pageModel->getAttributes(true);
            $_attributesModel = Top::getSingleton("bo_mapper/userattributes");
            $_requiredAttributesArray = $_attributesModel->fetchAll("is_required = 1")->toArray();
            foreach ($_requiredAttributesArray as $_simpleAttribute) {
                array_push($_attributesArray, $_simpleAttribute['attribute_name']);
            }
            $this->view->assign("userattributes", $_attributesArray);
        }
    }

    public function userrenderAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_user = Top::getSingleton("mapper/user");
        $_collection = $_user->getCollection(array(), NULL, 5, "created_at DESC");
        $_userArray = array();
        foreach ($_collection as $_fetched) {
            array_push($_userArray, $_fetched->getData());
        }
        echo json_encode(array("total" => 5, "rows" => (array) $_userArray));
    }

    private function _isAuthorizedCleaner($_username, $_password) {
        $iniConfig = Thinkopen_DbInitialize::getInstance();
        $_iniConfig = $iniConfig::$iniConfig;
        if ($_iniConfig->cachecleaner->username == $_username && $_iniConfig->cachecleaner->password == md5($_password)) {
            return true;
        } else {
            return false;
        }
    }

    public function cleancacheAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_username = $this->getParam("username");
        $_password = $this->getParam("password");
        $_database = $this->getParam("database", FALSE);
        $_twitter = $this->getParam("twitter", FALSE);
        if ($this->_isAuthorizedCleaner($_username, $_password)) {
            Thinkopen_Cacher::cleanAll((bool) $_database, (bool) $_twitter);
        } else {
            $this->forward("index");
        }
    }

    public function generatesitemapAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $sitemap = new Thinkopen_Sitemap();
            $sitemap->createSitemap();
            echo "<span style='color:green; font-size: 20px;'>Sitemap generata con successo</span>";
        } catch (Exception $e) {
            echo "<span style='color:red; font-size: 20px;'>Errore nella generazione della sitemap: " . $e->getMessage() . "</span>";
        }
    }

    public function reindexAction() {
        
    }

    public function completereindexAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $_searcher = new Thinkopen_Searcher();
        $_searcher->performCompleteReindex();
        $this->redirect(Top::getBaseUrl(true));
    }

    public function sitemapAction() {
        $sitemap = new Thinkopen_Sitemap();
        $this->view->lastupdate = $sitemap->getLastUpdate();
    }

    public function downloadsitemapAction() {
        $this->view->layout()->disableLayout();
        $sitemap = new Thinkopen_Sitemap();
        $this->view->sitemapXml = $sitemap->getSitemapXml();
    }

}
