<?php

class Bo_CategoryController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_category = Top::getModel("bo_mapper/category");
        $_fetchedAll = $_category->fetchAll(NULL, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_category->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_categoryModel = Top::getModel("bo_mapper/category");
        $this->view->assign("attributes", $_categoryModel->getAttributes());
        $this->view->codeResponse = $this->getParam("codeResponse");
        $this->view->messageResponse = $this->getParam("messageResponse");
    }

    public function updateAction() {
        switch ($this->getParam("category_id", NULL)) {
            case false:
                $this->view->newCategory = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/category")->load($this->getParam("category_id"));
                $this->view->newCategory = false;
                $this->view->assign("data", $model);
                $this->view->language = $this->getParam("language");
                $this->view->codeResponse = $this->getParam("codeResponse");
                $this->view->messageResponse = $this->getParam("messageResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("category_id" => false));
    }

    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (!$this->getParam("category_id", false)) {
            $_category = Top::getModel("bo_mapper/category");
        } else {
            $_category = Top::getModel("bo_mapper/category")->load($this->getParam("category_id"));
        }
        try {
            $_category->setTitle($this->getParam("title"))
                    ->setMeta_keywords($this->getParam("meta_keywords"))
                    ->setUpdated_at(date("Y-m-d H:i:s"));
            $iscative = ($this->getParam("is_active") == "on") ? 1 : 0;
            $_category->setIs_active($iscative)
                    ->save();
            $this->_helper->redirector("list", "category", "admin", array("category_id" => $_category->getCategory_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("list", "category", "admin", array("category_id" => $_category->getCategory_id(), "codeResponse" => 100));
        }
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelCategory = Top::getModel("bo_mapper/category")->load($this->getParam("category_id"));
            $_modelCategory->delete();
            $this->_helper->redirector("list", "category", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("list", "category", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

}
