<?php

class Bo_PageController extends Thinkopen_Bocontroller {

    public function indexAction() {
        $this->view->content = $this->getRequest()->getParam("error", NULL);
        $this->forward("list");
    }

    public function jsonrenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_page = Top::getModel("mapper/page");
        $_whereCondition = $this->getParam("search", false);
        if ($_whereCondition) {
            $_where = array('identifier LIKE ?' => "%" . $_whereCondition . "%",
                'title LIKE ?' => "%" . $_whereCondition . "%");
        } else {
            $_where = NULL;
        }
        $_fetchedAll = $_page->fetchAll($_where, (($this->getParam("sort", false)) ? $this->getParam("sort") . " " . $this->getParam("order") : NULL), $this->getParam("limit", NULL), $this->getParam("offset", NULL))->toArray();
        echo json_encode(array("total" => $_page->countRows(), "rows" => json_decode(json_encode((array) $_fetchedAll))));
    }

    public function listAction() {
        $_pageModel = Top::getModel("mapper/page");
        $this->view->assign("attributes", $_pageModel->getAttributes());
    }

    public function savefromfeAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $_pageContent = Top::getModel("bo_dbTable/pagecontent")->loadByAttribute(array(
                "page_id" => $this->getParam("page_id"),
                "language" => $this->getParam("language", "it"),
                "block_position" => $this->getParam("block_position"),
                "is_active" => 1
            ));
            $_pageContent->setContent($this->responsiveTable($this->getParam("content"), true))->save();
            $cacher = Thinkopen_Pagecacher::getInstance();
            $cacher->clean(Zend_Cache::CLEANING_MODE_NOT_MATCHING_TAG, array("metadatas"));
            echo "Il contenuto della pagina è stato correttamente salvato.";
        } catch (Exception $e) {
            echo "Errore durante il salvataggio della pagina : " . $e->getMessage();
        }
    }

    /**
     * Salva le informazioni della pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_searcher = new Thinkopen_Searcher();
        if ($this->getParam("clona", false)) {
            $this->clonapagina($this->getAllParams());
        }
        if (!$this->getParam("page_id", false)) {
            $_page = Top::getModel("bo_mapper/page");
        } else {
            $_page = Top::getModel("bo_mapper/page")->load($this->getParam("page_id"));
        }
        try {
            $_page->setTitle($this->getParam("title"))
                    ->setIdentifier(trim($this->getParam("identifier")))
                    ->setMeta_keywords($this->getParam("meta_keywords"))
                    ->setMeta_description($this->getParam("meta_description"))
                    ->setUpdated_at(date("Y-m-d H:i:s"))
                    ->setFb_share_desc($this->getParam("fb_share_desc"))
                    ->setFb_share_img($this->getParam("fb_share_img"))
                    ->setImg_wide($this->getParam("img_wide"))
                    ->setLanguage($this->getParam("language"))
                    ->setContent_type($this->getParam("content_type"))
                    ->setLayout($this->getParam("layout"));
            $iscative = ($this->getParam("is_active") == "on") ? 1 : 0;
            $_page->setIs_active($iscative);
            $_newPage = $_page->save();
            $_searcher->addPageToIndex($_newPage->getPage_id());
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $_page->getPage_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $_logger = Thinkopen_Logger::getInstance();
            $_logger->log("PAGE_ERROR", Zend_Log::ERR, $e->getMessage());
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $_page->getPage_id(), "codeResponse" => 100));
        }
    }

    /**
     * Cloan la pagina per la traduzione
     * @param array $_post
     */
    private function clonapagina($_post) {
        $_page = Top::getModel("bo_mapper/page");
        try {
            $_page->setTitle($_post["name_clone"])
                    ->setIdentifier(trim($_post["link_clone"]))
                    ->setRef_lang_id($_post["page_id"])
                    ->setMeta_keywords($_post["meta_keywords"])
                    ->setMeta_description($_post["meta_description"])
                    ->setFb_share_desc($this->getParam("fb_share_desc"))
                    ->setFb_share_img($this->getParam("fb_share_img"))
                    ->setImg_wide($this->getParam("img_wide"))
                    ->setLanguage($_post["language_clone"])
                    ->setContent_type($_post["content_type"])
                    ->setLayout($_post["layout"]);
            $iscative = ( $_post["is_active"] == "on") ? 1 : 0;
            $_page->setIs_active($iscative)
                    ->save();
            $modelBlocchiShoulder = Top::getModel("mapper/page")->loadBlocksShoulder($_post["page_id"], $_post['language']);
            foreach ($modelBlocchiShoulder as $postuno) {
                $_primoPost = $postuno->getData();
                unset($_primoPost['block_id']);
                if ($_primoPost['is_active'] == "0") {
                    unset($_primoPost['is_active']);
                }
                $this->setParameteresForBlockPageShoulder($_primoPost, $_page->getPage_id(), $_post['language_clone']);
            }
            $modelBlocchi = Top::getModel("mapper/page")->loadBlocks($_post["page_id"], $_post['language']);
            if ($_post["content_type"] == 0) {
                $_secondoPost = $modelBlocchi[0]->getData();
                if ($_secondoPost ['is_active'] == "0") {
                    unset($_secondoPost ['is_active']);
                }
                unset($_secondoPost['block_id']);
                $this->setParameteresForBlockPage($_secondoPost, $_page->getPage_id(), $_post['language_clone'], false);
            } else {
                foreach ($modelBlocchi as $postdue) {
                    $_secondoPost = $postdue->getData();
                    if ($_secondoPost ['is_active'] == "0") {
                        unset($_secondoPost ['is_active']);
                    }
                    unset($_secondoPost['block_id']);
                    $this->setParameteresForBlockPage($_secondoPost, $_page->getPage_id(), $_post['language_clone'], true);
                }
            }
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $_page->getPage_id(), "codeResponse" => 200));
        } catch (Exception $e) {
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $_page->getPage_id(), "codeResponse" => 100));
        }
    }

    /**
     * Gestisce la creazione e la modifica di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function updateAction() {
        switch ($this->getParam("page_id", NULL)) {
            case false:
                $this->view->newPage = true;
                break;
            case NULL:
                $this->forward("index");
                break;
            default:
                $model = Top::getModel("bo_mapper/page")->load($this->getParam("page_id"));
                $this->view->newPage = false;
                $this->view->assign("data", $model);
//                $this->view->language = $this->getParam("language");
                $this->view->language = $model->getLanguage();
                $this->view->codeResponse = $this->getParam("codeResponse");
                break;
        }
    }

    public function createAction() {
        $this->forward("update", NULL, NULL, array("page_id" => false));
    }

    public function deleteAction() {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $_modelPage = Top::getModel("bo_mapper/page")->load($this->getParam("page_id"));
            $page_id_to_ref = ($_modelPage->getLanguage() == "it") ? $_modelPage->getPage_id() : false;
            if ($page_id_to_ref) {
                $_modelPage->deleteAllRelatedLanguagePage($page_id_to_ref);
            }
            $_modelPage->delete();
            $_searcher = new Thinkopen_Searcher();
            foreach (Top::getLanguages() as $_language) {
                $_searcher->delete($this->getParam("page_id"), Thinkopen_Searcher::PAGES, $_language);
            }
            $this->_helper->redirector("index", "page", "admin", array("codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("index", "page", "admin", array("codeResponse" => 100, "messageResponse" => $ex->getMessage()));
        }
    }

    /** BLOCCHI CONTENT * */

    /**
     * Gestisce la creazione dei blocchi di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function createblocksAction() {
        try {
            $language = ($this->getParam("language")) ? $this->getParam("language") : "it";
            $pageModel = Top::getModel("bo_mapper/page")->load($this->getParam("page_id"));
            $modelBlocchi = Top::getModel("mapper/page")->loadBlocks($this->getParam("page_id"), $language);
            foreach ($modelBlocchi as $key => $block) {
                $arrayBlocchi[$key] = $block->setContent($this->responsiveTable($block->getContent()));
            }
            $this->view->assign("data", $pageModel);
            $this->view->modelblocchi = $arrayBlocchi;
            $this->view->language = $language;
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }

    /**
     * Salva le informazioni dei blocchi interni alla pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveblockAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $postData = $this->getAllParams();
        try {
            if ($postData['content_type'] == 0) {
                $this->setParameteresForBlockPage($postData['form_fluid'], $postData['page_id'], $postData['language'], false);
            } else {
                foreach ($postData['form_dati'] as $_post) {
                    $this->setParameteresForBlockPage($_post, $postData['page_id'], $postData['language'], true);
                }
            }
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $postData['page_id'], "codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $postData['page_id'], "codeResponse" => 100));
        }
    }

    /**
     * Effettua il save degli attributi per il model bo_dbtable/pagecontent
     * @author Walter Fato <walter.fato@thinkopen.it>
     * @param array $_post
     * @param string $_pageId
     * @param string $language
     * @param boolean $isBlock
     */
    private function setParameteresForBlockPage($_post, $_pageId, $language, $isBlock) {
        if (array_key_exists('block_id', $_post)) {
            $modelBlock = Top::getModel("bo_dbTable/pagecontent")->load($_post['block_id']);
        } else {
            $modelBlock = Top::getModel("bo_dbTable/pagecontent");
        }
        $modelBlock->setPage_id($_pageId)
                ->setLanguage($language)
                ->setBackground_image($_post['background_image'])
                ->setContent($this->responsiveTable($_post['content'], true));
        if (array_key_exists('is_active', $_post)) {
            $modelBlock->setIs_active(1);
        } else {
            $modelBlock->setIs_active(0);
        }
        if ($isBlock) {
            $modelBlock->setBlock_position($_post['block_position']);
            $modelBlock->setBlock_type(1);
        } else {
            $modelBlock->setBlock_position(0);
            $modelBlock->setBlock_type(0);
        }
        $modelBlock->save();
    }

    /** SPALLA * */

    /**
     * Gestisce la creazione dei blocchi della spalla di una pagina
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function createblocksshoulderAction() {
        try {
            $language = ($this->getParam("language")) ? $this->getParam("language") : "it";
            $pageModel = Top::getModel("bo_mapper/page")->load($this->getParam("page_id"));
            $modelBlocchi = Top::getModel("mapper/page")->loadBlocksShoulder($this->getParam("page_id"), $language);
            $this->view->assign("data", $pageModel);
            $this->view->modelblocchishould = $modelBlocchi;
            $this->view->language = $language;
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }

    /**
     * Salva le informazioni dei blocchi interni alla spalla
     * @author Walter Fato <walter.fato@thinkopen.it>
     */
    public function saveblockshoulderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $postData = $this->getAllParams();
        try {
            foreach ($postData['form_spalla'] as $_post) {
                $this->setParameteresForBlockPageShoulder($_post, $postData['page_id'], $postData['language']);
            }
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $postData['page_id'], "codeResponse" => 200));
        } catch (Exception $ex) {
            $this->_helper->redirector("update", "page", "admin", array("page_id" => $postData['page_id'], "codeResponse" => 100));
        }
    }

    private function setParameteresForBlockPageShoulder($_post, $_pageId, $language) {
        try {
            if (array_key_exists('block_id', $_post)) {
                $modelBlock = Top::getModel("bo_dbTable/pageshoulder")->load($_post['block_id']);
            } else {
                $modelBlock = Top::getModel("bo_dbTable/pageshoulder");
            }
            $modelBlock->setPage_id($_pageId)
                    ->setLanguage($language)
                    ->setBackground_image($_post['background_image'])
                    ->setContent($this->pulisciSpalla($_post['content']));
            if (array_key_exists('is_active', $_post)) {
                $modelBlock->setIs_active(1);
            } else {
                $modelBlock->setIs_active(0);
            }
            $modelBlock->setBlock_position($_post['block_position']);
            $modelBlock->setBlock_type(1);
            $modelBlock->save();
        } catch (Exception $e) {
            $this->_helper->redirector("update", "news", "admin", array("news_id" => $_pageId, "language" => $language, "codeResponse" => 100));
        }
    }

    private function responsiveTable($content, $isSave = false) {
        if ($isSave) {
            $content = str_replace("<table", "<div class='responsive_table'><table", $content);
            $content = str_replace("</table>", "</table><div class='spazio_table_responsive'></div></div>", $content);
        } else {
            $content = str_replace("<div class='responsive_table'>", "", $content);
            $content = str_replace("<div class='spazio_table_responsive'></div></div>", "", $content);
        }
        return $content;
    }

    private function pulisciSpalla($content) {
        $content = str_replace("<p>", "<div>", $content);
        $content = str_replace("</p>", "</div>", $content);
        $content = str_replace("<p></p>", "", $content);
        $content = str_replace("<p> </p>", "", $content);
        for ($i = 1; $i < 7; $i++) {
            $content = str_replace("<h" . $i . "></h" . $i . ">", "", $content);
            $content = str_replace("<h" . $i . "> </h" . $i . ">", "", $content);
        }
        return $content;
    }

}
