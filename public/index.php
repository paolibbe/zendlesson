<?php 
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH',
              realpath(dirname(__FILE__) . '/../application'));

defined('LANGUAGE_PATH')
    || define('LANGUAGE_PATH',
              realpath(dirname(__FILE__) . '/../languages'));

set_include_path('.'
        . PATH_SEPARATOR . get_include_path()
        . PATH_SEPARATOR . '../library/:'
);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'production'));
 
/** Zend_Application */
require_once 'Zend/Application.php';
 
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap()
            ->run();